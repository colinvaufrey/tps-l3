/*
Programme passager à compléter : les zones à compléter sont indiquées et il n'est pas nécessaire d'ajouter de nouvelles traces d'exécution. 

Vous devez expliquer en commentaires : le sens donné au messages echangés et aux étiquettes.

*/

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include "simulation.h"

// Mettre le même message pour les deux directions cause des conflits : il faut différencier aller et retour via des étiquettes.
#define ALLER_EMBARQUEMENT_PRET 1
#define RETOUR_EMBARQUEMENT_PRET 6

// Les autres messages sont les mêmes pour les deux directions.
// Les passagers retours ne reçoivent pas les messages avant d'embarquer.
#define EMBARQUEMENT_PASSAGER 2
#define DECOLLAGE 3
#define DEBARQUEMENT_PRET 4
#define DEBARQUEMENT_PASSAGER 5

#define ERROR -1

void checkError(int value, const char *message) {
	if (value == ERROR) {
		perror(message);
		exit(1);
	}
}

struct message {
	long msgType;
};

size_t messageSize = 0;

int main(int argc, char * argv[]){
	initDefault(10); // ne pas modifier ni déplacer cette ligne

	if (argc != 5) {
		printf("Nbre d'args invalide, utilisation :\n");
		printf("%s destination aller_ou_retour fichier-pour-cle-ipc entier-pour-cle-ipc\n", argv[0]);
		exit(0);
	}

	int cle = ftok(argv[3], atoi(argv[4]));
	int idFile = msgget(cle, 0666 | IPC_CREAT);

	// j'utilise msgget de sorte à s'assurer que la file existe avant de poursuivre
	if (idFile == -1) {
		perror("erreur msgget");
		exit(-1);
	}

	char * destination = argv[1];
	int aller_ou_retour = atoi(argv[2]); // 0 si je suis un passager du voyage aller, sinon 1 (pour un vol retour)  

	char * couleurTrace = "\x1b[38;5;226m";  // ne pas déplacer ni modifier
	if (aller_ou_retour) couleurTrace="\x1b[38;5;46m"; // ces deux lignes.

	// ... vous pouvez ajouter du code ici si nécessaire

	afficher('p', "Yoopi, je vais prendre l'avion !", destination, couleurTrace);
	afficher('p', "je patiente en salle d'embarquement", destination, couleurTrace);

	struct message embarquementPret;
	int tagToCheck = aller_ou_retour ? RETOUR_EMBARQUEMENT_PRET : ALLER_EMBARQUEMENT_PRET;
	checkError(msgrcv(idFile, &embarquementPret, messageSize, tagToCheck, 0), "erreur msgrcv");	

	afficher('p', "j'embarque ...", destination, couleurTrace);
	action(); // simulation de l'action embarquer.

	struct message confirmationEmbarquement;
	confirmationEmbarquement.msgType = EMBARQUEMENT_PASSAGER;
	checkError(msgsnd(idFile, &confirmationEmbarquement, messageSize, 0), "erreur msgsnd");

	struct message decollage;
	checkError(msgrcv(idFile, &decollage, messageSize, DECOLLAGE, 0), "erreur msgrcv");

	// ici se produit la simulaion du vol. je n'ai rien à faire

	afficher('p', "maintenant, je patiente jusqu'à destination", destination, couleurTrace);

	/* ... à compléter pour attendre l'arrivée à destination et donc attendre de pouvoir débarquer.*/

	struct message debarquementPret;
	checkError(msgrcv(idFile, &debarquementPret, messageSize, DEBARQUEMENT_PRET, 0), "erreur msgrcv");

	afficher('p', "je descends de l'avion", destination, couleurTrace);

	action(); // simulation de l'action débarquer.

	struct message confirmationDebarquement;
	confirmationDebarquement.msgType = DEBARQUEMENT_PASSAGER;
	checkError(msgsnd(idFile, &confirmationDebarquement, messageSize, 0), "erreur msgsnd");

	afficher('p', "je quitte l'aéroport !", destination, couleurTrace); 

	return 0;
}

