/*
Programme avion à compléter. Les zones à compléter sont indiquées et il n'est pas nécessaire d'ajouter de nouvelles traces d'exécution.

Vous devez expliquer en commentaires : le sens donné au messages echangés et aux étiquettes.
*/


#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include "simulation.h"

// Mettre le même message pour les deux directions cause des conflits : il faut différencier aller et retour via des étiquettes.
#define ALLER_EMBARQUEMENT_PRET 1
#define RETOUR_EMBARQUEMENT_PRET 6

// Les autres messages sont les mêmes pour les deux directions.
// Les passagers retours ne reçoivent pas les messages avant d'embarquer.
#define EMBARQUEMENT_PASSAGER 2
#define DECOLLAGE 3
#define DEBARQUEMENT_PRET 4
#define DEBARQUEMENT_PASSAGER 5

#define ERROR -1

void checkError(int value, const char *message) {
	if (value == ERROR) {
		perror(message);
		exit(1);
	}
}

struct message {
	long msgType;
};

size_t messageSize = 0;

int main(int argc, char * argv[]){

	initDefault(10); // ne pas modifier ni déplacer 
	char * couleurTrace = "\x1b[38;5;39m"; //ces deux ligne.

	if (argc != 7) {
		printf("Nbre d'args invalide, utilisation :\n");
		printf("%s destination_aller nombre_passagers_aller destination_retour nombre_passagers_retour fichier-pour-cle-ipc entier-pour-cle-ipc\n", argv[0]);
		exit(0);
	}

	char * dest_aller = argv[1];
	int nbPassagers_Aller = atoi(argv[2]);

	char * dest_retour = argv[3];
	int nbPassagers_Retour = atoi(argv[4]);

	int cle = ftok(argv[5], atoi(argv[6]));

	int idFile = msgget(cle, 0666 | IPC_CREAT);

	// j'utilise msgget de sorte à s'assurer que la file existe avant de poursuivre l'exécution.
	if (idFile == -1) {
		perror("erreur msgget");
		exit(-1);
	}

	// ... vous pouvez ajouter du code ici si nécessaire

	// dans la suite, les traces d'exécution données sont à garder inchangées.

	// vol aller 
	afficher('a', "embarquement immediat !", dest_aller, couleurTrace);

	/* ... à compléter pour :

	1) annoncer le début de l'embarquement aux passagers de l'aller
	2) attendre que tous les passagers de l'aller aient embarqué. Cette partie doit inclure la ligne suivante au bon moment :

	afficher('a', "attente fin embarquement", dest_aller, couleurTrace);   

	*/

	struct message msgEmbarquement;
	msgEmbarquement.msgType = ALLER_EMBARQUEMENT_PRET;

	for (int i = 0; i < nbPassagers_Aller; i++) {
		checkError(msgsnd(idFile, &msgEmbarquement, messageSize, 0), "erreur msgsnd");
	}

	int passagersEmbarques = 0;

	while (passagersEmbarques < nbPassagers_Aller) {
		afficher('a', "attente fin embarquement", dest_aller, couleurTrace);  
		struct message msg;
		checkError(msgrcv(idFile, &msg, messageSize, EMBARQUEMENT_PASSAGER, 0), "erreur msgrcv");
		passagersEmbarques++;
	}

	struct message msgDecollage;
	msgDecollage.msgType = DECOLLAGE;

	for (int i = 0; i < nbPassagers_Aller; i++) {
		checkError(msgsnd(idFile, &msgDecollage, messageSize, 0), "erreur msgsnd");
	}

	afficher('a',"décolage imminent! Durée du vol estimée à quelques secondes", dest_aller, couleurTrace); 

	vol(4);  // simulation d'une durée de vol (vous pouvez tester avec d'autres valeurs. Le code rendu doit toutefois être vol(4); 

	afficher('a',"arrivé à destination", dest_aller, couleurTrace);

	/* ... à compléter pour :

	1) annoncer l'arrivée à destination et donc le début du débarquement
	2) attendre que tous les passagers soient descendus de l'avion. Cette partie doit inclure la ligne suivante au bon moment :

	afficher('a', "attente que tout le monde soit descendu", dest_aller, couleurTrace);

	*/

	struct message msgDebarquement;
	msgDebarquement.msgType = DEBARQUEMENT_PRET;

	for (int i = 0; i < nbPassagers_Aller; i++) {
		checkError(msgsnd(idFile, &msgDebarquement, messageSize, 0), "erreur msgsnd");
	}

	int passagersDescendus = 0;

	while (passagersDescendus < nbPassagers_Aller) {
		afficher('a', "attente que tout le monde soit descendu", dest_aller, couleurTrace);
		struct message msg;
		checkError(msgrcv(idFile, &msg, messageSize, DEBARQUEMENT_PASSAGER, 0), "erreur msgrcv");
		passagersDescendus++;
	}

	afficher('a', "vol aller terminé", dest_aller, couleurTrace);

	// vol retour 
	afficher('a', "embarquement immediat !", dest_retour, couleurTrace);

	/* ... à compléter pour :

	1) annoncer le début de l'embarquement aux passagers de l'aller
	2) attendre que tous les passagers de l'aller aient embarqué. Cette partie doit inclure la ligne suivante au bon moment :

	afficher('a', "attente fin embarquement", dest_retour, couleurTrace);   

	*/

	msgEmbarquement.msgType = RETOUR_EMBARQUEMENT_PRET;

	for (int i = 0; i < nbPassagers_Retour; i++) {
		checkError(msgsnd(idFile, &msgEmbarquement, messageSize, 0), "erreur msgsnd");
	}

	passagersEmbarques = 0;

	while (passagersEmbarques < nbPassagers_Retour) {
		afficher('a', "attente fin embarquement", dest_retour, couleurTrace);  
		struct message msg;
		checkError(msgrcv(idFile, &msg, messageSize, EMBARQUEMENT_PASSAGER, 0), "erreur msgrcv");
		passagersEmbarques++;
	}

	msgDecollage.msgType = DECOLLAGE;

	for (int i = 0; i < nbPassagers_Retour; i++) {
		checkError(msgsnd(idFile, &msgDecollage, messageSize, 0), "erreur msgsnd");
	}

	afficher('a',"décolage imminent! Durée du vol estimée à quelques secondes", dest_retour, couleurTrace); 

	vol(4);  // simulation d'une durée de vol (vous pouvez tester avec d'autres valeurs. Le code rendu doit toutefois être vol(4); 

	afficher('a',"arrivé à destination", dest_retour, couleurTrace);

	/* ... à compléter pour :

	1) annoncer l'arrivée à destination et donc le début du débarquement
	2) attendre que tous les passagers soient descendus de l'avion. Cette partie doit inclure la ligne suivante au bon moment :

	afficher('a', "attente que tout le monde soit descendu", dest_retour, couleurTrace);

	*/

	msgDebarquement.msgType = DEBARQUEMENT_PRET;

	for (int i = 0; i < nbPassagers_Retour; i++) {
		checkError(msgsnd(idFile, &msgDebarquement, messageSize, 0), "erreur msgsnd");
	}

	passagersDescendus = 0;

	while (passagersDescendus < nbPassagers_Retour) {
		afficher('a', "attente que tout le monde soit descendu", dest_retour, couleurTrace);
		struct message msg;
		checkError(msgrcv(idFile, &msg, messageSize, DEBARQUEMENT_PASSAGER, 0), "erreur msgrcv");
		passagersDescendus++;
	}

	/*
	Remarques :
	- ne faire cette partie qu'une fois l'implémentation du vol aller terminée et testée !
	- les processus passager du vol retour ne doivent pas être réveillés inutilement tout au long de l'étape implémentant l'aller. 
	*/

	afficher('a', "vol retour terminé", dest_retour, couleurTrace); 
	return 0;
}

