#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Ce programme est à compléter et à déposer sur Moodle, sans changer le nom du fichier. N'ajouter aucun autre fichier sauf ceux qui peuvent être demandés pendant l'épreuve.

	Lire attentivement les instructions en commentaires pour compléter correctement ce programme.

	Les principales étapes de ce programme sont :

	1) contacter un serveur UDP

	2) recevoir une instruction à suivre en UDP et l'afficher. L'expéditeur de cette instruction a une adresse différente de celle du serveur que vous avez contacté en 1). Cela sera expliqué dans l'instruction.

	3) mettre en place un serveur TCP et échanger avec un client distant.

	4) faire une copie du programme et le modifier pour qu'il soit capable de traiter plusieurs clients de manière itérative.

	5) faire une copie du programme et le modifier pour qu'il soit capable de traiter plusieurs clients simultanément (en utilisant la fonction fork()).

	Attention : vous devez déposer un code qui compile. Exemple : si vous êtes à l'étape 3 qui n'est pas fonctionnelle, mettre cette dernière étape en commentaire pour que toutes les étapes d'avant soient validées.

*/

socklen_t adressLength = sizeof(struct sockaddr_in);

int main(int argc, char *argv[]) {

	if (argc != 4){
		printf("Utilisation : %s ip_serveurUDP  port_serveurUDP  port_votre_serveur_TCP\n - ip_serveurUDP  et port_serveurUDP : donnés par l'intervenant(e). Il s'agit de l'adresse d'un serveur UDP qui est en attente d'un message que vous allez envoyer.\n - port_votre_serveur_TCP : un numero de votre choix pour un serveur TCP qui sera a implementer\n", argv[0]);
		exit(0);
	}

	/* Etape 1 : envoyer un message au serveur UDP (adresse passée en parametres) et recevoir une réponse 

		Début :

		- envoyer un message de type chaine de caractères. Cette chaine est à saisir au clavier (dites bonjour au serveur UDP).
	*/


	printf("Client : Création de la socket en cours...\n");

	int socketDescription = socket(PF_INET, SOCK_DGRAM, 0);

	if (socketDescription == -1) {
		perror("Client : Problème création socket ");
		exit(1);
	}

	printf("Client : Création de la socket réussie\n");

	printf("Client : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketClient;
	adresseSocketClient.sin_family = AF_INET;
	adresseSocketClient.sin_addr.s_addr = INADDR_ANY;
	adresseSocketClient.sin_port = htons((short) atoi(argv[3]));
	int resultBind = bind(socketDescription, (struct sockaddr*)
	&adresseSocketClient, sizeof(adresseSocketClient));

	if (resultBind == -1) {
	   perror("Client : Problème dans le lien de la socket (bind) ");
	   exit(1);
	}

	printf("Client : Lien de la socket réussie\n");

	printf("Client : Description de la socket du serveur...\n");

	struct sockaddr_in adresseSocketDistante;
	adresseSocketDistante.sin_family = AF_INET;
	adresseSocketDistante.sin_addr.s_addr = inet_addr(argv[1]);
	adresseSocketDistante.sin_port = htons((short)atoi(argv[2]));

	printf("Client : Description de la socket du serveur terminée\n");

	printf("Client : Envoi d'un message au serveur...\n");

	char premierMsg[200];
	printf("CC : Saisir un message à envoyer (moins de 200 caractères) :\n");
	fgets(premierMsg, sizeof(premierMsg), stdin); 
	premierMsg[strlen(premierMsg)-1] = '\0';

	int messageLength = strlen(premierMsg);

	int resultSending =
		sendto(socketDescription, &premierMsg, messageLength, 0,
			   (struct sockaddr *)&adresseSocketDistante, adressLength);

	if (resultSending == -1) {
		perror("Client : Problème dans l'envoi du message ");
		exit(1);
	}

	printf("Client : Message envoyé avec succès\n");

	// Recevoir un message de type chaîne de caractères et l'afficher. Il s'agira de la prochaine étape / instruction à réaliser dans votre programme. Une instruction se terminera par la mention Fin instruction dans le message.

	char nouvelleInstruction[2024];

	struct sockaddr_in adresseSocketContactee;

	int resultReceiving =
		recvfrom(socketDescription, &nouvelleInstruction, sizeof(nouvelleInstruction), 0,
				 (struct sockaddr *)&adresseSocketContactee, &adressLength);
	
	if (resultReceiving == -1) {
		perror("Client : Problème dans la réception du message ");
		exit(1);
	}

	printf("Client : Message reçu avec succès\n");
	printf("Client : Message reçu : %s\n", nouvelleInstruction);

	// Fin étape 1    

	/* Etape 2 : répondre à la question reçue 

	Début */

	// Socket écoute TCP

	int socketEcoute = socket(PF_INET, SOCK_STREAM, 0);

	if (socketEcoute == -1) {
		perror("Client : Problème création socket d'écoute TCP");
		exit(1);
	}

	printf("Client : Création de la socket d'écoute TCP réussie\n");

	// Nommage de la socket

	printf("Client : Lien de la socket d'écoute TCP en cours...\n");

	struct sockaddr_in adresseSocketEcoute;
	adresseSocketEcoute.sin_family = AF_INET;
	adresseSocketEcoute.sin_addr.s_addr = INADDR_ANY;
	adresseSocketEcoute.sin_port = htons((short) atoi(argv[3]) + 1);
	resultBind = bind(socketEcoute, (struct sockaddr*)&adresseSocketEcoute, sizeof(adresseSocketEcoute));

	if (resultBind == -1) {
	   perror("Client : Problème dans le lien de la socket d'écoute TCP (bind) ");
	   exit(1);
	}

	printf("Client : Lien de la socket d'écoute TCP réussie\n");

	// int resultGetSocketName = getsockname(socketEcoute, (struct sockaddr*)&adresseSocketEcoute, &adressLength);

	// if (resultGetSocketName == -1) {
	// 	perror("Serveur : Problème getsockname");
	// 	exit(1);
	// }

	// Mise en écoute

	printf("Client : Mise en écoute de la socket d'écoute TCP...\n");

	int resListen = listen(socketEcoute, 5);

	if (resListen == -1) {
	   perror("Client : Problème dans la mise en écoute de la socket d'écoute TCP ");
	   exit(1);
	}

	printf("Client : Mise en écoute de la socket d'écoute TCP réussie\n");

	// Envoi de la socket d'écoute au serveur UDP

	printf("Client : Envoi de l'adresse de la socket d'écoute au serveur UDP...\n");

	resultSending =
		sendto(socketDescription, &adresseSocketEcoute, sizeof(adresseSocketEcoute), 0,
			   (struct sockaddr *)&adresseSocketContactee, adressLength);

	if (resultSending == -1) {
		perror("Client : Problème dans l'envoi de l'adresse de la socket d'écoute au serveur UDP ");
		exit(1);
	}
	
	printf("Client : Envoi de l'adresse de la socket d'écoute au serveur UDP réussi\n");

	printf("Client : Fermeture de la socket UDP...\n");

	int resClose = close(socketDescription);

	if (resClose == -1) {
	   perror("Client : Problème dans la fermeture de la socket UDP ");
	   exit(1);
	}

	printf("Client : Fermeture de la socket UDP réussie\n");

	while (1) {
		printf("Client - Père : Attente de la connexion du serveur TCP...\n");

		// Attente de la connexion du serveur TCP

		struct sockaddr_in adresseSocketContacteeTCP;
		socklen_t adressLengthTCP = sizeof(adresseSocketContacteeTCP);

		int socketContact = accept(socketEcoute, (struct sockaddr*)&adresseSocketContacteeTCP, &adressLengthTCP);

		if (socketContact == -1) {
			perror("Client : Problème dans l'acceptation de la connexion du serveur TCP ");
			exit(1);
		}

		printf("Client : Connexion du serveur TCP réussie\n");

		int resultFork = fork();

		if (resultFork == -1) {
			perror("Client : Problème dans le fork ");
			exit(1);
		}

		if (resultFork == 0) {
			printf("Client - Fils : Processus fils créé\n");

			// Réception de la taille du message

			int tailleMessage;

			resultReceiving =
				recv(socketContact, &tailleMessage, sizeof(tailleMessage), 0);

			if (resultReceiving == -1) {
				perror("Client - Fils : Problème dans la réception de la taille du message ");
				exit(1);
			}

			printf("Client - Fils : Taille du message reçue avec succès\n");

			// Réception du message

			char message[tailleMessage];

			resultReceiving =
				recv(socketContact, &message, sizeof(message), 0);

			if (resultReceiving == -1) {
				perror("Client - Fils : Problème dans la réception du message ");
				exit(1);
			}

			printf("Client - Fils : Message reçu avec succès\n");
			printf("Client - Fils : Message reçu : %s\n", message);

			// Fin étape 2

			// Etape 3 : recevoir un tableau

			printf("Client - Fils : Attente de la réception du tableau...\n");

			int tableau[320000];

			int receivedBytes = 0;
			int bytesToReceive = sizeof(tableau);
			while (receivedBytes < bytesToReceive) {
				resultReceiving =
					recv(socketContact, tableau + (receivedBytes / sizeof(int)), bytesToReceive - receivedBytes, 0);

				if (resultReceiving == -1) {
					perror("Client - Fils : Problème dans la réception du tableau ");
					exit(1);
				}

				receivedBytes += resultReceiving;
			}

			printf("Client - Fils : Tableau reçu avec succès\n");

			// Renvoi du tableau

			printf("Client - Fils : Envoi du tableau au serveur TCP...\n");

			int sentBytes = 0;

			while (sentBytes < bytesToReceive) {
				resultSending =
					send(socketContact, tableau + (sentBytes / sizeof(int)), bytesToReceive - sentBytes, 0);

				if (resultSending == -1) {
					perror("Client - Fils : Problème dans l'envoi du tableau ");
					exit(1);
				}

				sentBytes += resultSending;
			}

			printf("Client - Fils : Tableau envoyé avec succès\n");

			// Réception de la taille d'un message

			int tailleMessage2;

			resultReceiving =
				recv(socketContact, &tailleMessage2, sizeof(tailleMessage2), 0);

			if (resultReceiving == -1) {
				perror("Client - Fils : Problème dans la réception de la taille du message ");
				exit(1);
			}

			printf("Client - Fils : Taille du message reçue avec succès\n");

			// Réception du message

			char message2[tailleMessage2];

			resultReceiving =
				recv(socketContact, &message2, sizeof(message2), 0);

			if (resultReceiving == -1) {
				perror("Client - Fils : Problème dans la réception du message ");
				exit(1);
			}

			printf("Client - Fils : Message reçu avec succès\n");
			printf("Client - Fils : Message reçu : %s\n", message2);

			// Fin étape 3

			printf("Client - Fils : Fermeture de la socket TCP...\n");

			resClose = close(socketContact);

			if (resClose == -1) {
				perror("Client - Fils : Problème dans la fermeture de la socket TCP ");
				exit(1);
			}

			printf("Client - Fils : Fermeture de la socket TCP réussie\n");

			printf("Client - Fils : Fin du processus fils\n");

			break;
		} else {
			printf("Client - Père : Processus père\n");
		}
	}

	printf("Client - Fils : Fermeture de la socket d'écoute TCP...\n");

	resClose = close(socketEcoute);

	if (resClose == -1) {
		perror("Client - Fils : Problème dans la fermeture de la socket d'écoute TCP ");
		exit(1);
	}

	printf("Client - Fils : Fermeture de la socket d'écoute TCP réussie\n");

	printf("Client - Fils : Fin du programme\n");

	return 0;
}

