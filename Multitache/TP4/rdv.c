#include "calcul.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

struct predicatRdv {
	// Regroupe les données partagées entres les threads participants au RdV
	int waitingThread;
	pthread_mutex_t *lock;
	pthread_cond_t *cond;
};

struct params {
	int idThread;
	int num;
	struct predicatRdv *sharedVar;
};

// Fonction associée à chaque thread participant au RdV
void *participant(void *p) {
	struct params *args = (struct params *)p;
	struct predicatRdv *predicat = args->sharedVar;

	printf("[Thread %i] Attente du verrou.\n", args->idThread);
	pthread_mutex_lock(predicat->lock);
	printf("[Thread %i] Verrouillage du verrou.\n", args->idThread);

	int wait = 1;

	printf("[Thread %i] Début du calcul, durée : %is\n", args->idThread, wait);
	calcul(wait);
	printf("[Thread %i] Fin calcul.\n", args->idThread);

	if (++(predicat->waitingThread) == args->num) {
		printf("[Thread %i] Dernier thread : je réveille tout le monde.\n",
			   args->idThread);
		pthread_cond_broadcast(predicat->cond);
	}

	// RdV
	while (predicat->waitingThread !=
		   args->num) { // Attention : le dernier arrivé ne doit pas attendre.
						// Il doit réveiller tous les autres.
		printf("[Thread %i] Attente, libération du verrou.\n", args->idThread);
		pthread_cond_wait(predicat->cond, predicat->lock);
		printf("[Thread %i] Réveil, verouillage du verrou.\n", args->idThread);
	}

	wait = 2;
	pthread_mutex_unlock(predicat->lock);
	printf("[Thread %i] Libération du verrou.\n", args->idThread);

	// Reprise et poursuite de l'execution.
	printf("[Thread %i] Début du calcul final, durée : %is\n", args->idThread,
		   wait);
	calcul(wait);
	printf("[Thread %i] Fin du calcul final\n", args->idThread);

	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("Argument requis.\n");
		printf("%s nombre_threads\n", argv[0]);
		exit(1);
	}

	int nbThreads = atoi(argv[1]);

	// Initialisations
	pthread_t threads[nbThreads];
	struct params tabParams[nbThreads];

	srand(nbThreads); // Initialisation de rand pour la simulation de longs
					  // calculs

	struct predicatRdv predicat;
	predicat.waitingThread = 0;

	pthread_mutex_t lock;
	pthread_cond_t cond;
	pthread_mutex_init(&lock, NULL);
	pthread_cond_init(&cond, NULL);

	// Création des threards
	for (int i = 0; i < nbThreads; i++) {
		tabParams[i].idThread = i + 1;
		tabParams[i].sharedVar = &predicat;
		tabParams[i].num = nbThreads;
		predicat.lock = &lock;
		predicat.cond = &cond;
	}
	for (int i = 0; i < nbThreads; i++) {
		tabParams->idThread = i;
		if (pthread_create(&threads[i], NULL, participant, tabParams) != 0) {
			perror("Erreur Création Thread.");
			exit(1);
		}
	}

	// Attente de la fin des threads. Partie obligatoire
	for (int i = 0; i < nbThreads; i++) {
		pthread_join(threads[i], NULL);
	}

	printf("[Thread principal] Fin de tous les threads secondaires.\n");

	// Destruction des mutex et des conditions
	pthread_mutex_destroy(&lock);
	pthread_cond_destroy(&cond);
}