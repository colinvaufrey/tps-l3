#include "calcul.h"
#include <iostream>
#include <pthread.h>
#include <stdio.h> //perror
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

struct paramsFonctionThread {
	int idThread;
	int waitTimeMultiplier;
	void *data; // Données partagées
};

void *fonctionThread(void *params) {
	struct paramsFonctionThread *args = (struct paramsFonctionThread *)params;

	pthread_t threadId = pthread_self();

	calcul(args->waitTimeMultiplier);

	cout << "Thread " << args->idThread << " (id " << threadId << ") : "
		 << "Fin du calcul" << endl;

	if (args->idThread == 10) {
		// Faire appel à exit() dans un thread termine le
		// programme
		cout << "Thread " << args->idThread << " (id " << threadId << ") : "
			 << "Je quitte !" << endl;
		exit(1);
	}

	cout << "Thread " << args->idThread << " (id " << threadId << ") : "
		 << "Données partagées : " << (char *)args->data << endl;
	// Édition de la donnée partagée avec l'id du thread
	sprintf((char *)args->data, "Données partagées modifiées par le thread %d",
			args->idThread);
	cout << "Thread " << args->idThread << " (id " << threadId << ") : "
		 << "Données partagées : " << (char *)args->data << endl;

	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		cout << "Utilisation : " << argv[0] << " nombre_threads" << endl;
		return 1;
	}

	int nbThreads = atoi(argv[1]);

	pthread_t threads[nbThreads];

	char *data = new char[100];
	strcpy(data, "Données partagées");

	// Création des threads
	for (int i = 0; i < nbThreads; i++) {
		struct paramsFonctionThread *params = new paramsFonctionThread;
		params->idThread = i;
		params->waitTimeMultiplier = i + 1;
		params->data = data;

		// Écris l'adresse de la structure params
		cout << "Thread " << i << " : " << params << endl;

		if (pthread_create(&threads[i], NULL, fonctionThread, params) != 0) {
			perror("Erreur Création Thread");
			exit(1);
		}
	}

	// Attente de la fin des threads
	for (int i = 0; i < nbThreads; i++) {
		if (pthread_join(threads[i], NULL) != 0) {
			perror("Erreur Attente Thread");
			exit(1);
		}
	}

	return 0;
}
