#include "calcul.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_WAIT_TIME 5

struct varPartagees {
	int nbZones;
	int *di; // Ce tableau contient le numéro de la zone traitée par chaque
			 // activité
};

// Structure qui regroupe les paramètres d'un thread
struct params {
	int idThread;
	struct varPartagees *vPartage;
	int randomWaitValue;
};

// Fonction associée à chaque thread secondaire à créer.
void *traitement(void *p) {
	struct params *args = (struct params *)p;
	struct varPartagees *vPartage = args->vPartage;

	printf("[Thread %i] Début de l'activité.\n", args->idThread);

	for (int i = 0; i <= vPartage->nbZones; i++) {
		printf("[Thread %i] Activation de la zone %i\n", args->idThread, i);

		if (args->idThread != 0) { // Le premier traitement n'attend personne
			while (vPartage->di[args->idThread - 1] <= i) {
				// Attendre que la zone i soit traitée
			}

			printf("[Thread %i] La zone %i est libre.\n", args->idThread, i);
		}

		printf("[Thread %i] Zone %i en cours de traitement. Temps de "
			   "traitement : %is\n",
			   args->idThread, i, args->randomWaitValue);

		calcul(args->randomWaitValue); // Simuler un long calcul

		// On indique que la zone i est traitée
		vPartage->di[args->idThread] = i + 1;
		printf("[Thread %i] Zone %i traitée.\n", args->idThread, i);
	}

	printf("[Thread %i] Fin de l'activité.\n", args->idThread);
	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {

	if (argc != 4) {
		printf("Argument requis.\n");
		printf("%s nombre_traitements nombre_zones graine\n", argv[0]);
		exit(1);
	}

	int nombreTraitements = atoi(argv[1]);
	int graine = atoi(argv[3]);

	// Initialisations
	pthread_t threads[nombreTraitements];
	struct params tabParams[nombreTraitements];

	struct varPartagees vPartage;

	vPartage.nbZones = atoi(argv[2]);
	vPartage.di = (int *)malloc(sizeof(int) * nombreTraitements);

	// Initialisation du tableau di
	for (int i = 0; i < nombreTraitements; i++) {
		vPartage.di[i] = 0;
	}

	srand(graine); // Initialisation de rand pour la simulation
	// de longs calculs

	// Création des threards
	for (int i = 0; i < nombreTraitements; i++) {
		tabParams[i].idThread = i;
		tabParams[i].vPartage = &vPartage;
		tabParams[i].randomWaitValue = rand() % MAX_WAIT_TIME + 1;
		if (pthread_create(&threads[i], NULL, traitement, &tabParams[i]) != 0) {
			perror("Erreur Création Thread.");
			exit(1);
		}
	}

	// Attente de la fin des threads. Partie obligatoire.
	for (int i = 0; i < nombreTraitements; i++) {
		if (pthread_join(threads[i], NULL) != 0) {
			perror("Erreur Join Thread.");
			exit(1);
		}
	}

	printf("[Thread principal] Fin de tous les threads secondaires.\n");

	// Libération de la mémoire
	free(vPartage.di);
}
