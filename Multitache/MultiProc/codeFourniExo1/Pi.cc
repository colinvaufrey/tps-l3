#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

#include "shared.h"

int main(int argc, char *argv[]) {
	long ownPid = getpid();

	printf("Starting the process %ld and getting key\n", ownPid);

	key_t key = ftok(filePath, 1);
	checkError(key, "An error occured while getting the key");

	printf("Got key\n");

	int msgid = msgget(key, 0666);
	checkError(msgid, "An error occured while getting the message queue");

	printf("Sending an access request\n");

	struct message request;

	request.msgType = WAITING_FOR_ACCESS;
	request.pid = ownPid;

	checkError(msgsnd(msgid, &request, messageSize, 0),
			   "An error occured while sending an access request");

	printf("Sent access request\n");

	struct message response;

	do {
		checkError(
			msgrcv(msgid, &response, messageSize, (long)ACCESS_GRANTED, 0),
			"An error occured while receiving an access granted message");
	} while (response.pid != ownPid);

	printf("Received access granted message\n");

	printf("Accessing the resource\n");

	sleep(5);

	printf("Releasing the resource\n");

	struct message release;

	release.msgType = RESOURCE_RELEASED;
	release.pid = ownPid;

	checkError(msgsnd(msgid, &release, messageSize, 0),
			   "An error occured while sending a resource released message");

	printf("Sent resource released message\n");

	return 0;
}