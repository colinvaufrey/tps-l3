#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

#include "shared.h"

int main(int argc, char *argv[]) {
	printf("Starting the resource manager\n");

	key_t key = ftok(filePath, 1);

	printf("Created the key\n");

	int msgid = msgget(key, 0666 | IPC_CREAT);

	printf("Created the message queue\n");

	while (1) {
		printf("Waiting for a request\n");

		struct message receivedRequest;

		checkError(msgrcv(msgid, &receivedRequest, messageSize,
						  (long)WAITING_FOR_ACCESS, 0),
				   "An error occured while receiving a request");

		long receivedPid = receivedRequest.pid;

		printf("Received request from process %ld to access the resource\n",
			   receivedRequest.pid);

		struct message response;
		response.msgType = ACCESS_GRANTED;
		response.pid = receivedPid;

		checkError(msgsnd(msgid, &response, messageSize, 0),
				   "An error occured while sending an access granted message");

		printf("Sent access granted message to process %ld\n", receivedPid);

		do {
			checkError(msgrcv(msgid, &receivedRequest, messageSize,
							  (long)RESOURCE_RELEASED, 0),
					   "An error occured while receiving a resource released "
					   "message");
		} while (response.pid != receivedPid);

		printf("Received resource released message from process %ld\n",
			   receivedRequest.pid);
	}

	return 0;
}