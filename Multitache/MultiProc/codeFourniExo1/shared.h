#define WAITING_FOR_ACCESS 1
#define ACCESS_GRANTED 2
#define RESOURCE_RELEASED 3

#define ERROR -1

const char *filePath = "./pourCle.txt";

void checkError(int value, const char *message) {
	if (value == ERROR) {
		perror(message);
		exit(1);
	}
}

struct message {
	long msgType;
	long pid;
};

size_t messageSize = sizeof(struct message);