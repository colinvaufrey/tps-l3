#include <stdio.h>//perror
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>//close
#include <stdlib.h>
#include <string.h>


#define MAX_BUFFER_SIZE 146980


int main(int argc, char *argv[]) {
	/* etape 0 : gestion des paramètres si vous souhaitez en passer */

	if (argc != 2) {
		printf("Utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}

	/* etape 1 : creer une socket d'écoute des demandes de connexions*/

	printf("Serveur : Création de la socket en cours...\n");

	int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);

	// Vérification de la création de la socket

	if (socketDescriptor == -1) {
		perror("Serveur : Problème dans la création de la socket ");
		exit(1);
	}

	printf("Serveur : Création de la socket réussie\n");

	/* etape 2 : nommage de la socket */

	printf("Serveur : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketHote;
	adresseSocketHote.sin_family = AF_INET;
	adresseSocketHote.sin_addr.s_addr = INADDR_ANY;
	// adresseSocketHote.sin_port = 0; // Zero : le système choisit un port
	// libre
	adresseSocketHote.sin_port = htons((short)atoi(argv[1]));
	int resultSocketNaming =
		bind(socketDescriptor, (struct sockaddr *)&adresseSocketHote,
			 sizeof(adresseSocketHote));

	if (resultSocketNaming == -1) {
		perror("Serveur : Problème dans le nommage socket (bind) ");
		exit(1);
	}

	/* etape 3 : mise en ecoute des demandes de connexions */

	printf("Serveur : Mise en écoute de la socket en cours...\n");

	int resultListen = listen(socketDescriptor, 5);

	if (resultListen == -1) {
		perror("Serveur : Problème dans la mise en écoute de la socket ");
		exit(1);
	}

	printf("Serveur : Mise en écoute de la socket réussie\n");

	/* etape 4 : plus qu'a attendre la demadne d'un client */

	while (1) {

		printf("Serveur : Attente d'une demande de connexion d'un client...\n");

		struct sockaddr_in adresseSocketClient;
		socklen_t adresseSocketClientLength = sizeof(adresseSocketClient);

		int socketDescriptorClient = accept(socketDescriptor,
											(struct sockaddr *)&adresseSocketClient,
											&adresseSocketClientLength);

		if (socketDescriptorClient == -1) {
			perror("Serveur : Problème dans l'acceptation de la connexion ");
			exit(1);
		}

		printf("Serveur : Connexion acceptée d'un client\n");

		int pid = fork();

		if (pid == -1) {
			perror("Serveur : Problème dans la création du processus fils ");
			exit(1);
		}

		if (pid == 0) {
			printf("Serveur : Processus fils créé\n");

			printf("Serveur : Attente de la taille du nom du fichier...\n");

			int fileNameSize;

			int resultReceiveFileNameSize = recv(
				socketDescriptorClient, &fileNameSize, sizeof(fileNameSize), 0);

			if (resultReceiveFileNameSize == -1) {
				perror("Serveur : Problème dans la réception de la taille du "
					   "nom du fichier ");
				exit(1);
			}

			printf("Serveur : Taille du nom du fichier reçue\n");

			printf("Serveur : Attente du nom du fichier...\n");

			char fileName[fileNameSize];

			int resultReceiveFileName =
				recv(socketDescriptorClient, fileName, fileNameSize, 0);

			if (resultReceiveFileName == -1) {
				perror(
					"Serveur : Problème dans la réception du nom du fichier ");
				exit(1);
			}

			printf("Serveur : Nom du fichier reçu : %s\n", fileName);

			printf("Serveur : Attente de la taille du fichier...\n");

			int fileSize;

			int resultReceiveFileSize =
				recv(socketDescriptorClient, &fileSize, sizeof(fileSize), 0);

			if (resultReceiveFileSize == -1) {
				perror("Serveur : Problème dans la réception de la taille du "
					   "fichier ");
				exit(1);
			}

			printf("Serveur : Taille du fichier reçue\n");

			int totalRecv =
				0; // un compteur du nombre total d'octets recus d'un client

			char filepath[300];
			filepath[0] = '\0';
			strcat(filepath, "reception/");
			strcat(filepath, fileName);

			// On ouvre le fichier dans lequel on va écrire
			FILE *file = fopen(filepath, "wb");
			if (file == NULL) {
				perror("Serveur : erreur ouverture fichier: \n");
				exit(1);
			}

			int countRecvCalls = 0;

			char buffer[MAX_BUFFER_SIZE];

			while (totalRecv < fileSize) {
				int bytesLeft = fileSize - totalRecv;
				int bytesToReceive =
					bytesLeft < MAX_BUFFER_SIZE ? bytesLeft : MAX_BUFFER_SIZE;

				// On lit le contenu du fichier envoyé par le client
				int resultReceiveBlock =
					recv(socketDescriptorClient, buffer, bytesToReceive, 0);
				countRecvCalls++;

				if (resultReceiveBlock == -1) {
					perror("Serveur : Problème dans la réception d'un bloc de "
						   "données ");
					exit(1);
				}

				if (resultReceiveBlock == 0) {
					printf("Serveur : Fin de la réception du fichier\n");
					break;
				}

				// On écrit le contenu du fichier dans le fichier de réception

				int resultWriteBlock =
					fwrite(buffer, 1, resultReceiveBlock, file);

				if (resultWriteBlock != resultReceiveBlock) {
					perror("Serveur : Problème dans l'écriture d'un bloc de "
						   "données ");
					exit(1);
				}

				totalRecv += resultReceiveBlock;
			}

			printf("Serveur : Ecriture dans fichier réussie. Vous pouvez "
				   "vérifier la création du fichier et son contenu.\n");
			printf("Serveur : Nombre total d'octets reçus : %d\n", totalRecv);
			printf("Serveur : Nombre total d'appels à recv : %d\n",
				   countRecvCalls);
			// fermeture du fichier
			fclose(file);

			break;
		} else {
			printf("Serveur : Processus père\n");
		}

		// fermeture de la socket de communication avec le client
		close(socketDescriptorClient);
	}

	printf("Serveur : Fini\n");
}








