#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>

#define MAX_BUFFER_SIZE 146980

int main(int argc, char *argv[]) {

	if (argc != 4){
		printf("utilisation : client ip_serveur port_serveur nom_fichier\n Remaque : le parametre nom_fichier correspond au nom d'un fichier existant dans le répertoire emission\n");
		exit(0);
	}

	/* etape 1 : créer une socket */

	printf("Client : Création de la socket\n");

	int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
	if (socketDescriptor == -1){
		perror("Client : Erreur création socket");
		exit(1);
	}
	printf("Client : Socket créée\n");

	/* etape 2 : designer la socket du serveur */

	printf("Client : Description de la socket du serveur...\n");

	int adressLength = sizeof(struct sockaddr_in);

	struct sockaddr_in adresseSocketDistante;
	adresseSocketDistante.sin_family = PF_INET;
	adresseSocketDistante.sin_addr.s_addr = inet_addr(argv[1]);
	adresseSocketDistante.sin_port = htons((short)atoi(argv[2]));

	printf("Client : Description de la socket du serveur terminée\n");

	/* etape 3 : demander une connexion */

	printf("Client : Connexion au serveur...\n");

	int resultConnection = connect(socketDescriptor, (struct sockaddr *)&adresseSocketDistante, adressLength);

	if (resultConnection == -1) {
		perror("Client : Problème dans la connexion au serveur ");
		exit(1);
	}

	printf("Client : Connexion au serveur réussie\n");

	/* etape 4 : envoi de fichier : attention la question est générale. Il faut creuser pour définir un protocole d'échange entre le client et le serveur pour envoyer correctement un fichier */

	printf("Client : Envoi de la taille du nom du fichier...\n");

	int fileNameLength = strlen(argv[3]) + 1;

	int resultFileNameLength = send(socketDescriptor, &fileNameLength, sizeof(int), 0);

	if (resultFileNameLength == -1) {
		perror("Client : Problème dans l'envoi du message ");
		exit(1);
	}

	printf("Client : Envoi de la taille du nom du fichier réussi\n");

	printf("Client : Envoi du nom fichier...\n");

	int resultFileName = send(socketDescriptor, argv[3], fileNameLength, 0);

	if (resultFileName == -1) {
		perror("Client : Problème dans l'envoi du message ");
		exit(1);
	}

	printf("Client : Envoi du nom du fichier réussi\n");

	int totalSent = 0; // variable pour compter le nombre total d'octet effectivement envoyés au serveur du début à la fin des échanges.

	/* le bout de code suivant est une lecture de contenu d'un fichier dont le nom est passé en paramètre.
	- pour lire un fichier, il faut l'ouvrir en mode lecture
	- la lecture se fait par blocs d'octets jusqu'à la fin du fichier.
	- la taille d'un bloc est définie par la constante MAX_BUFFER_SIZE que vous pouvez modifier.

	Le code est à compléter pour mettre en place l'envoi d'un fichier.
	*/

	// construction du nom du chemin vers le fichier
	char* filepath = malloc(strlen(argv[3]) + 16); // ./emission/+nom fichier +\0
	filepath[0] = '\0';
	strcat(filepath, "./emission/");
	strcat(filepath, argv[3]);

	printf("Client: Je vais envoyer %s\n", filepath);

	// obtenir la taille du fichier
	struct stat attributes;
	if (stat(filepath, &attributes) == -1) {
		perror("Client : Erreur stat");
		free(filepath);
		exit(1);
	}

	int file_size = attributes.st_size; // cette copie est uniquement informée d'où obtenir la taille du fichier.

	printf("Client : Envoi de la taille du fichier...\n");

	int resultFileSize = send(socketDescriptor, &file_size, sizeof(int), 0);

	if (resultFileSize == -1) {
		perror("Client : Problème dans l'envoi du message ");
		free(filepath);
		exit(1);
	}

	printf("Client : Envoi de la taille du fichier réussi\n");

	printf("Client : Taille du fichier : %d octets\n", file_size);

	// lecture du contenu d'un fichier
	FILE* file = fopen(filepath, "rb");
	if (file == NULL) {
		perror("Client : Erreur ouverture fichier \n");
		free(filepath);
		exit(1);   
	}
	free(filepath);

	int countSendCalls = 0;

	int total_lu = 0;
	char buffer[MAX_BUFFER_SIZE];
	while (total_lu < file_size) {
		int bytesLeft = file_size - totalSent;
		int bytesToSend = bytesLeft < MAX_BUFFER_SIZE ? bytesLeft : MAX_BUFFER_SIZE;

		size_t read = fread(buffer, sizeof(char), bytesToSend, file);
		if (read == 0) {
			if (ferror(file) != 0) {
				perror("Client : Erreur lors de la lecture du fichier \n");
				fclose(file);
				exit(1);
			} else {
				printf("Client : Arrivé à la fin du fichier\n");
				break;
			}
		} else {
			printf("Client : J'ai lu %ld octets du fichier \n", read);

			// envoi du bloc lu

			int resultSendingBlock = send(socketDescriptor, buffer, read, 0);
			countSendCalls++;

			if (resultSendingBlock == -1) {
				perror("Client : Problème dans l'envoi du bloc ");
				fclose(file);
				exit(1);
			}
			
			totalSent += resultSendingBlock;

			printf("Client : Envoi du bloc réussi\n");

		}
		printf("Client : J'ai lu un bloc du fichier \n");  
		total_lu += read;
	}

	// fermeture du fichier
	fclose(file); 

	printf("Client : J'ai lu au total : %d octets \n", total_lu);
	printf("Client : J'ai envoyé au total : %d octets \n", totalSent);
	printf("Client : J'ai appelé send au total : %d fois \n", countSendCalls);

	printf("Client : Fini\n");
}
