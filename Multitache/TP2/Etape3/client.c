#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// Programme client

socklen_t adressLength = sizeof(struct sockaddr_in);

int main(int argc, char *argv[]) {
	if (argc != 5) {
		printf("Utilisation : %s ip_serveur port_serveur port_client nombre_iterations\n",
			   argv[0]);
		exit(1);
	}

	int numberOfIterations = atoi(argv[4]);

	printf("Client : Création de la socket en cours...\n");

	int socketDescription = socket(PF_INET, SOCK_STREAM, 0);

	if (socketDescription == -1) {
		perror("Client : Problème création socket ");
		exit(1);
	}

	printf("Client : Création de la socket réussie\n");

	// Nommage de la socket (optionnel)

	printf("Client : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketClient;
	adresseSocketClient.sin_family = AF_INET;
	adresseSocketClient.sin_addr.s_addr = INADDR_ANY;
	adresseSocketClient.sin_port = htons((short) atoi(argv[3]));
	int res = bind(socketDescription, (struct sockaddr*)
	&adresseSocketClient, sizeof(adresseSocketClient));

	if (res == -1){
	   perror("Client : Problème dans le lien de la socket (bind) ");
	   exit(1);
	}

	printf("Client : Nommage de la socket réussie\n");

	printf("Client : Description de la socket du serveur...\n");

	struct sockaddr_in adresseSocketDistante;
	adresseSocketDistante.sin_family = AF_INET;
	adresseSocketDistante.sin_addr.s_addr = inet_addr(argv[1]);
	adresseSocketDistante.sin_port = htons((short)atoi(argv[2]));

	printf("Client : Description de la socket du serveur terminée\n");

	printf("Client : Connexion au serveur...\n");

	int resultConnection =
		connect(socketDescription, (struct sockaddr *)&adresseSocketDistante,
				adressLength);

	if (resultConnection == -1) {
		perror("Client : Problème dans la connexion au serveur ");
		exit(1);
	}

	printf("Client : Connexion au serveur réussie\n");

	printf("Client : Envoi d'un message au serveur...\n");

	char message[200];
	printf("Entrez un message pour le serveur : ");
	scanf("%[^\n]", message);

	int messageLength = strlen(message) + 1;

	int numberOfBytesSupposedToBeSent = messageLength * numberOfIterations;
	int numberOfBytesSent = 0;
	int numberOfCallsToSendFunction = 0;

	int resultSending;

	for (int i = 0; i < numberOfIterations; i++) {
		printf("\nClient : Itération %d / %d\n", i + 1, numberOfIterations);

		resultSending = send(socketDescription, message, messageLength, 0);
		numberOfCallsToSendFunction++;

		if (resultSending == -1) {
			perror("Client : Problème dans l'envoi du message ");
			exit(1);
		}

		printf("Client : Message envoyé avec succès\n");
		numberOfBytesSent += resultSending;

		printf("Client : Réponse du serveur reçue avec succès\n");
	}

	printf("\nClient : Itérations terminées\n");

	printf("Client : Nombre d'appels à la fonction send : %d\n", numberOfCallsToSendFunction);
	printf("Client : Nombre d'octets envoyés : %d\n", numberOfBytesSent);
	printf("Client : Nombre d'octets supposés être envoyés : %d\n", numberOfBytesSupposedToBeSent);

	printf("Client : Fermeture de la socket...\n");

	int resultClosure = close(socketDescription);

	if (resultClosure == -1) {
		perror("Client : Problème dans la fermeture de la socket ");
		exit(1);
	}

	printf("Client : Terminé\n");
	return 0;
}