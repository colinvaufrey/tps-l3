#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// Programme serveur

socklen_t adressLength = sizeof(struct sockaddr_in);

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("Utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}

	printf("Serveur : Création de la socket en cours...\n");

	int socketDescription = socket(PF_INET, SOCK_STREAM, 0);

	// Vérification de la création de la socket
	if (socketDescription == -1) {
		perror("Serveur : Problème dans la création de la socket ");
		exit(1);
	}

	printf("Serveur : Création de la socket réussie\n");

	printf("Serveur : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketHote;
	adresseSocketHote.sin_family = AF_INET;
	adresseSocketHote.sin_addr.s_addr = INADDR_ANY;
	adresseSocketHote.sin_port = htons((short)atoi(argv[1]));
	int resultSocketNaming =
		bind(socketDescription, (struct sockaddr *)&adresseSocketHote,
			 sizeof(adresseSocketHote));

	if (resultSocketNaming == -1) {
		perror("Serveur : Problème dans le nommage socket (bind) ");
		exit(1);
	}

	printf("Serveur : Nommage de la socket réussie\n");

	printf("Serveur : Mise en écoute de la socket en cours...\n");

	int resultSocketListening = listen(socketDescription, 10);

	if (resultSocketListening == -1) {
		perror(
			"Serveur : Problème dans la mise en écoute de la socket (listen) ");
		exit(1);
	}

	printf("Serveur : Mise en écoute de la socket réussie\n");

	printf("Serveur : Attente d'une connexion...\n");

	int socketDescriptionClient =
		accept(socketDescription, (struct sockaddr *)&adresseSocketHote,
			   &adressLength);

	if (socketDescriptionClient == -1) {
		perror("Serveur : Problème dans l'acceptation de la connexion ");
		exit(1);
	}

	printf("Serveur : Connexion acceptée\n");
	
	char message[200];
	ssize_t receivedMessageLength;

	int totalBytesReceived = 0;
	int numberOfCallsToRecvFunc = 0;

	while(1) {
		printf("\nServeur : En attente d'un message...\n");

		receivedMessageLength =
			recv(socketDescriptionClient, message, sizeof(message), 0);
		numberOfCallsToRecvFunc++;

		if (receivedMessageLength == -1) {
			perror("Serveur : Problème dans la réception du message ");
			exit(1);
		} else if (receivedMessageLength == 0) {
			printf("Serveur : Connexion terminée par le client\n");
			break;
		}

		printf("Message reçu : %s\n", message);
		printf("Taille du message reçu : %ld\n", receivedMessageLength);
		totalBytesReceived += receivedMessageLength;
		printf("Nombre total de bytes reçus : %d\n", totalBytesReceived);
		printf("Nombre total d'appels à recv : %d\n", numberOfCallsToRecvFunc);
	}

	printf("\nServeur : Fermeture de la connexion en cours...\n");

	int resultConnectionClosure = close(socketDescriptionClient);

	if (resultConnectionClosure == -1) {
		perror("Serveur : Problème dans la fermeture de la connexion ");
		exit(1);
	}

	printf("Serveur : Fermeture de la connexion réussie\n");

	printf("Serveur : Fermeture de la socket en cours...\n");

	int resultClosure = close(socketDescription);

	if (resultClosure == -1) {
		perror("Serveur : Problème dans la fermeture de la socket ");
		exit(1);
	}

	printf("Serveur : Fermeture de la socket réussie\n");

	return 0;
}
