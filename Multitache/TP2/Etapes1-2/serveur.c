#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int receiveTCP(int sock, char *msg, int sizeMsg) {
	int received = 0;
	int result = 0;
	while (received < sizeMsg) {
		result = recv(sock, msg + received, sizeMsg - received, 0);
		received += result;

		switch (result) {
		case -1:
			return -1;
		case 0:
			return 0;
		}
	}
	return 1;
}

// Programme serveur

socklen_t adressLength = sizeof(struct sockaddr_in);

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("Utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}

	printf("Serveur : Création de la socket en cours...\n");

	int socketDescription = socket(PF_INET, SOCK_STREAM, 0);

	// Vérification de la création de la socket
	if (socketDescription == -1) {
		perror("Serveur : Problème dans la création de la socket ");
		exit(1);
	}

	printf("Serveur : Création de la socket réussie\n");

	printf("Serveur : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketHote;
	adresseSocketHote.sin_family = AF_INET;
	adresseSocketHote.sin_addr.s_addr = INADDR_ANY;
	// adresseSocketHote.sin_port = 0; // Zero : le système choisit un port
	// libre
	adresseSocketHote.sin_port = htons((short)atoi(argv[1]));
	int resultSocketNaming =
		bind(socketDescription, (struct sockaddr *)&adresseSocketHote,
			 sizeof(adresseSocketHote));

	if (resultSocketNaming == -1) {
		perror("Serveur : Problème dans le nommage socket (bind) ");
		exit(1);
	}

	// Récupérer le port de la socket si le système a choisi un port libre

	// int resultGetSocketName = getsockname(socketDescription, (struct
	// sockaddr*) &adresseSocketHote, &adressLength);

	// if (resultGetSocketName == -1) {
	// 	perror("Serveur : Problème getsockname");
	// 	exit(1);
	// }

	// printf("Serveur : Port de la socket : %d\n", (int)
	// ntohs(adresseSocketHote.sin_port));

	printf("Serveur : Nommage de la socket réussie\n");

	printf("Serveur : Mise en écoute de la socket en cours...\n");

	int resultSocketListening = listen(socketDescription, 10);

	if (resultSocketListening == -1) {
		perror(
			"Serveur : Problème dans la mise en écoute de la socket (listen) ");
		exit(1);
	}

	printf("Serveur : Mise en écoute de la socket réussie\n");

	// printf("Serveur : Suspension de la connexion...\n");
	// char suspens[200];
	// printf("Entrée pour continuer...\n");
	// scanf("%[^\n]", suspens);

	printf("Serveur : Attente d'une connexion...\n");

	int socketDescriptionClient =
		accept(socketDescription, (struct sockaddr *)&adresseSocketHote,
			   &adressLength);

	if (socketDescriptionClient == -1) {
		perror("Serveur : Problème dans l'acceptation de la connexion ");
		exit(1);
	}

	printf("Serveur : Connexion acceptée\n");

	// printf("Serveur : Suspension de la connexion...\n");
	// char suspens[200];
	// printf("Entrée pour continuer...\n");
	// scanf("%[^\n]", suspens);

	int lengthMessageToReceive = 0;

	printf("Serveur : Attente de la taille du message...\n");

	int resultMessageLengthReceiving =
		recv(socketDescriptionClient, &lengthMessageToReceive,
			 sizeof(lengthMessageToReceive), 0);
	
	if (resultMessageLengthReceiving == -1) {
		perror("Serveur : Problème dans la réception de la taille du message ");
		exit(1);
	}

	printf("Serveur : Taille du message reçue : %d\n",
		   lengthMessageToReceive);

	char message[lengthMessageToReceive];

	printf("Serveur : En attente d'un message...\n");

	ssize_t receivedMessageLength =
		recv(socketDescriptionClient, message, sizeof(message), 0);

	if (receivedMessageLength == -1) {
		perror("Serveur : Problème dans la réception du message ");
		exit(1);
	}

	// printf("Serveur : Ajout de \\O à la fin du message\n");
	// message[receivedMessageLength] = '\0';

	printf("Message reçu : %s\n", message);
	printf("Taille du message reçu : %ld\n", receivedMessageLength);

	printf("Serveur : Envoi du message au client...\n");

	int resultMessageSending =
		send(socketDescriptionClient, &receivedMessageLength,
			 sizeof(receivedMessageLength), 0);

	if (resultMessageSending == -1) {
		perror("Serveur : Problème dans l'envoi du message ");
		exit(1);
	}

	printf("Serveur : Message envoyé avec succès\n");

	printf("Serveur : Fermeture de la connexion en cours...\n");

	int resultConnectionClosure = close(socketDescriptionClient);

	if (resultConnectionClosure == -1) {
		perror("Serveur : Problème dans la fermeture de la connexion ");
		exit(1);
	}

	printf("Serveur : Fermeture de la connexion réussie\n");

	printf("Serveur : Fermeture de la socket en cours...\n");

	int resultClosure = close(socketDescription);

	if (resultClosure == -1) {
		perror("Serveur : Problème dans la fermeture de la socket ");
		exit(1);
	}

	printf("Serveur : Fermeture de la socket réussie\n");

	return 0;
}
