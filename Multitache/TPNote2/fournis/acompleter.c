/*
 Programme d'un vol aller-retour d'un avion avec un thread avion et des threads passagers.
  
 Les parties à compléter sont indiquées en commentaires.
 
 Les traces fournies sont suffisantes.
 
*/

#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "simulation.h"
#include <string.h>


struct varPartagees {
 // regroupe toute les variables partagées entre threads. Ces variables ne sont pas forcément toutes utilisées par un thread particulier.
 
 // vous pouvez choisir de faire autrement, à partir du moment où il n'y a pas de variables globales ! 

  //... à compléter
  int nbPassagersEmbarques;
  char * destCourante;

  pthread_mutex_t mutexAvion;

  pthread_cond_t condDestinationMiseAJour;
  pthread_cond_t condArriveeADestination;

  pthread_cond_t condEmbarquement;
  pthread_cond_t condDebarquement;
};


struct paramsAvion {
  // structure pour regrouper les paramètres du thread avion. 
  char * destAller;
  char * destRetour;
  int nbPassagersAller; // nombre de passagers à l'aller
  int nbPassagersRetour; // nombre de passagers au retour
  
  // ... à compléter

  struct varPartagees * varPartagees;
};

struct paramsPassager {
  // structure qui regroupe les paramètres d'un thread passager. 
  int idPassager; // un identifiant de passager 1, 2, 3, etc.)
  char * destination; // la destination du passager (aller ou retour) donnée en paramètre du programme.
  char * couleurTrace; // permet de gérer les couleurs des traces.
  
  // ... à compléter. Remarque : en aucun cas un passager n'a besoin de connaitre le nombre total de passagers.
  struct varPartagees * varPartagees;
};


void * avion (void * p) {

  struct paramsAvion * args = (struct paramsAvion *) p;
  
  char * couleurTrace = "\x1b[38;5;39m";
  
  // implémenter le  vol aller 
  afficher('a', "embarquement immediat !", 0, args -> destAller, couleurTrace);
   
   
   /* ... à compléter pour :
    
    1) annoncer le début de l'embarquement aux passagers de l'aller
    2) attendre que tous les passagers de l'aller aient embarqué. Cette partie doit inclure la ligne suivante au bon moment :
    
    afficher('a', "attente fin embarquement", 0, args -> destAller, couleurTrace);
    
    */
  
  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  args->varPartagees->destCourante = args->destAller;

  pthread_cond_broadcast(&args->varPartagees->condDestinationMiseAJour);

  while(args->varPartagees->nbPassagersEmbarques < args->nbPassagersAller){
    afficher('a', "attente fin embarquement", 0, args -> destAller, couleurTrace);
    pthread_cond_wait(&args->varPartagees->condEmbarquement, &args->varPartagees->mutexAvion);
  }

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);


  

 afficher('a',"décollage imminent! Durée du vol estimée à quelques secondes", 0, args -> destAller, couleurTrace); 
 
  vol(4); // simulation d'une durée de vol (vous pouvez tester avec différentes valeurs. Le code rendu doit toutefois remettre vol(4); 
  
  afficher('a',"arrivé à destination", 0, args -> destAller, couleurTrace);
      
  /* ... à compléter pour :
  
  1) annoncer l'arrivée à destination et donc le début du débarquement
  2) attendre que tous les passagers soient descendus de l'avion. Cette partie doit inclure la ligne suivante au bon moment :
  
 */

  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  pthread_cond_broadcast(&args->varPartagees->condArriveeADestination);

  while(args->varPartagees->nbPassagersEmbarques > 0){
    afficher('a', "attente que tout le monde soit descendu", 0, args -> destAller, couleurTrace);
    pthread_cond_wait(&args->varPartagees->condDebarquement, &args->varPartagees->mutexAvion);
  }

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);

  
  afficher('a', "vol aller terminé", 0, args -> destAller, couleurTrace);
  
  // impélmenter le vol retour : 
  
   afficher('a', "embarquement immédiat !", 0, args -> destRetour, couleurTrace);
   
   /* ... à compléter


   
   Remarques :
   - ne faire cette partie qu'une fois l'implémentation du vol aller terminée et testée !
   - les threads passager du vol retour ne doivent pas être réveillés inutilement tout au long de l'étape implémentant l'aller.
   
   */

  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  args->varPartagees->destCourante = args->destRetour;

  pthread_cond_broadcast(&args->varPartagees->condDestinationMiseAJour);

  while(args->varPartagees->nbPassagersEmbarques < args->nbPassagersRetour){
    afficher('a', "attente fin embarquement", 0, args -> destRetour, couleurTrace);
    pthread_cond_wait(&args->varPartagees->condEmbarquement, &args->varPartagees->mutexAvion);
  }

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);

  afficher('a',"décollage imminent! Durée du vol estimée à quelques secondes", 0, args -> destRetour, couleurTrace);

  vol(4); // simulation d'une durée de vol (vous pouvez tester avec différentes valeurs. Le code rendu doit toutefois remettre vol(4);

  afficher('a',"arrivé à destination", 0, args -> destRetour, couleurTrace);

  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  pthread_cond_broadcast(&args->varPartagees->condArriveeADestination);

  while(args->varPartagees->nbPassagersEmbarques > 0){
    afficher('a', "attente que tout le monde soit descendu", 0, args -> destRetour, couleurTrace);
    pthread_cond_wait(&args->varPartagees->condDebarquement, &args->varPartagees->mutexAvion);
  }

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);

  afficher('a', "vol retour terminé", 0, args -> destRetour, couleurTrace);
  
  pthread_exit(NULL); 
}


void * passager (void * p) {

  struct paramsPassager * args = (struct paramsPassager *) p;
    
  afficher('p', "Yoopi, je vais prendre l'avion !", args -> idPassager, args -> destination, args ->couleurTrace);

  /* ... à compléter pour attendre l'annonce de l'embarquement. Cette partie doit inclure la ligne suivante au bon moment :
  
  afficher('p', "je patiente en salle d'embarquement", args -> idPassager, args -> destination, args ->couleurTrace);
  
  */

  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  do {
    afficher('p', "je patiente en salle d'embarquement", args -> idPassager, args -> destination, args ->couleurTrace);
    pthread_cond_wait(&args->varPartagees->condDestinationMiseAJour, &args->varPartagees->mutexAvion);
  } while (strcmp(args->varPartagees->destCourante, args->destination) != 0);

  afficher('p', "j'embarque ...", args -> idPassager, args -> destination, args ->couleurTrace);
  
  args->varPartagees->nbPassagersEmbarques++;

  pthread_cond_signal(&args->varPartagees->condEmbarquement);

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);

   // ... ici, en fonction de votre solution, il est possible d'ajouter du code



  action(); // simulation de l'action embarquer.
  
   // ... ici, en fonction de votre solution, il est possible d'ajouter du code
   
  // ici se produit la simulation du vol (géré par l'avion). il n'y a rien à faire
   
  /* ... à compléter pour attendre l'arrivée à destination et donc attendre de pouvoir débarquer. Cette partie doit inclure la ligne suivante au bon moment :
  
    afficher('p', "maintenant, je patiente jusqu'à destination", args -> idThread, args -> destination, args ->couleurTrace);
    
    */
  
  pthread_mutex_lock(&args->varPartagees->mutexAvion);

  afficher('p', "maintenant, je patiente jusqu'à destination", args -> idPassager, args -> destination, args ->couleurTrace);
  pthread_cond_wait(&args->varPartagees->condArriveeADestination, &args->varPartagees->mutexAvion);
  

  afficher('p', "je descends de l'avion", args -> idPassager, args -> destination, args ->couleurTrace);
  
   // ... ici, en fonction de votre solution, il est possible d'ajouter du code
    
  action(); // simulation de l'action débarquer.
  
   // ... ici, en fonction de votre solution, il est possible d'ajouter du code
   
  args->varPartagees->nbPassagersEmbarques--;

  pthread_cond_signal(&args->varPartagees->condDebarquement);

  pthread_mutex_unlock(&args->varPartagees->mutexAvion);


  afficher('p', "je quitte l'aéroport !", args -> idPassager, args -> destination, args ->couleurTrace); 
  pthread_exit(NULL); 
}


int main(int argc, char * argv[]){
  
  if (argc!=5) {
    printf(" argument requis \n %s destination_aller nombre_passager_aller destination_retour nombre_passager_retour\n", argv[0]);
    exit(1);
  }

  initDefault(atoi(argv[2])); // ne pas modifier cet appel ni le déplacer.
 
 
 
  // initialisations 
  pthread_t threads[1+atoi(argv[2])+atoi(argv[4])]; // pour garder les identifiants de tous les threads dans le même tableau. Vous pouvez faire autrement
  
  // le code fourni dans le reste de ce programme est pour vous aider. Vous pouvez le modifier si vous le souhaitez, à condition que tous les threads passagers (aller et retour) soient créés avant l'avion. 
  
  struct varPartagees varP;

  varP.nbPassagersEmbarques = 0;
  pthread_mutex_init(&varP.mutexAvion, NULL);

  pthread_cond_init(&varP.condDestinationMiseAJour, NULL);
  pthread_cond_init(&varP.condArriveeADestination, NULL);

  pthread_cond_init(&varP.condEmbarquement, NULL);
  pthread_cond_init(&varP.condDebarquement, NULL);
  
  struct paramsAvion paramAvion; 
   
  // initialisations pour l'avion
  paramAvion.destAller = argv[1];
  paramAvion.destRetour = argv[3];
  paramAvion.nbPassagersAller = atoi(argv[2]);
  paramAvion.nbPassagersRetour = atoi(argv[4]);
  paramAvion.varPartagees = &varP;
  
  // initialisations et création des threads pour les passagers 
  
  struct paramsPassager tabParamsAller[atoi(argv[2])];
   
  struct paramsPassager tabParamsRetour[atoi(argv[4])]; 
 
  // création des threads

  // Passagers aller 
  char * couleurTraceA = "\x1b[38;5;226m";
  
  for (int i = 0; i < atoi(argv[2]); i++){
    tabParamsAller[i].idPassager = i+1;
    tabParamsAller[i].destination = argv[1];
    tabParamsAller[i].couleurTrace = couleurTraceA;
    tabParamsAller[i].varPartagees = &varP;
    
    if (pthread_create(&threads[1+i], NULL, passager, &(tabParamsAller[i])) != 0){
      perror("erreur creation thread passager aller");
      exit(1);
    }
  }
  
  
  // Passagers retour
  char * couleurTraceR = "\x1b[38;5;46m";
  for (int i = 0; i < atoi(argv[4]); i++){
    tabParamsRetour[i].idPassager = i+1;
    tabParamsRetour[i].destination = argv[3];
    tabParamsRetour[i].couleurTrace = couleurTraceR;
    tabParamsRetour[i].varPartagees = &varP;
    
    if (pthread_create(&threads[1+atoi(argv[2])+i], NULL, passager, &(tabParamsRetour[i])) != 0){
      perror("erreur creation thread passager aller");
      exit(1);
    }
  }
  
  // Avion
  if (pthread_create(&(threads[0]), NULL, avion, &paramAvion) != 0){
    perror("erreur creation thread avion");
    exit(1);
   } 
    
    // attente de la fin des  threads. 
    for (int i = 0; i < 1+atoi(argv[2])+atoi(argv[4]); i++){
      pthread_join(threads[i], NULL);
    }
    
    // ... à compléter pour une terminaison "propre" (destruction des variables de type mutex, variables conditionnelles, espaces alloués dynamiquement...

    pthread_mutex_destroy(&varP.mutexAvion);
    pthread_cond_destroy(&varP.condDestinationMiseAJour);
    pthread_cond_destroy(&varP.condArriveeADestination);
    pthread_cond_destroy(&varP.condEmbarquement);
    pthread_cond_destroy(&varP.condDebarquement);
    
    return 0;
}
 
