#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>


// Programme serveur

socklen_t adressLength = sizeof(struct sockaddr_in);

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("Utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}

	printf("Serveur : Création de la socket en cours...\n");

	int socketDescription = socket(PF_INET, SOCK_DGRAM, 0);

	// Vérification de la création de la socket
	if (socketDescription == -1) {
		perror("Serveur : Problème dans la création de la socket ");
		exit(1);
	}

	printf("Serveur : Création de la socket réussie\n");

	printf("Serveur : Nommage de la socket en cours...\n");

	struct sockaddr_in adresseSocketHote;
	adresseSocketHote.sin_family = AF_INET;
	adresseSocketHote.sin_addr.s_addr = INADDR_ANY;
	// adresseSocketHote.sin_port = 0; // Zero : le système choisit un port
	// libre
	adresseSocketHote.sin_port = htons((short)atoi(argv[1]));
	int resultSocketNaming =
		bind(socketDescription, (struct sockaddr *)&adresseSocketHote,
			 sizeof(adresseSocketHote));

	if (resultSocketNaming == -1) {
		perror("Serveur : Problème dans le nommage socket (bind) ");
		exit(1);
	}

	// Récupérer le port de la socket si le système a choisi un port libre

	// int resultGetSocketName = getsockname(socketDescription, (struct
	// sockaddr*) &adresseSocketHote, &adressLength);

	// if (resultGetSocketName == -1) {
	// 	perror("Serveur : Problème getsockname");
	// 	exit(1);
	// }

	// printf("Serveur : Port de la socket : %d\n", (int)
	// ntohs(adresseSocketHote.sin_port));

	printf("Serveur : Nommage de la socket réussie\n");

	char message[1000];
	struct sockaddr_in adresseSocketEnvoyeur;

	while (1) {

		printf("Serveur : En attente d'un message...\n");

		ssize_t receivedMessageLength =
			recvfrom(socketDescription, &message, sizeof(message), 0,
					 (struct sockaddr *)&adresseSocketEnvoyeur, &adressLength);

		if (receivedMessageLength == -1) {
			perror("Serveur : Problème dans la réception du message ");
			exit(1);
		}

		printf("Message reçu du client d'adresse %s et de port %d : ",
			   inet_ntoa(adresseSocketEnvoyeur.sin_addr),
			   (int)ntohs(adresseSocketEnvoyeur.sin_port));
		printf("%s\n", message);

		printf("Serveur : Envoi du message au client...\n");

		int resultMessageSending =
			sendto(socketDescription, &receivedMessageLength, sizeof(ssize_t),
				   0, (struct sockaddr *)&adresseSocketEnvoyeur, adressLength);

		if (resultMessageSending == -1) {
			perror("Serveur : Problème dans l'envoi du message ");
			exit(1);
		}

		printf("Serveur : Message envoyé avec succès\n");
	}

	printf("Serveur : Fermeture de la socket en cours...\n");

	int resultClosure = close(socketDescription);

	if (resultClosure == -1) {
		perror("Serveur : Problème dans la fermeture de la socket ");
		exit(1);
	}

	printf("Serveur : Fermeture de la socket réussie\n");

	return 0;
}
