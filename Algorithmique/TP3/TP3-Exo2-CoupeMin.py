from random import *


def GenereGraph(n: int, p: float) -> tuple[list, list]:
    E = []
    Contract = [i for i in range(n)]
    for i in range(n):
        for j in range(i + 1, n):
            if i != j:
                if random() < p:
                    E.append((i, j))
    return (E, Contract)


def ContractArete(E: list, Contract: list) -> None:
    if len(E) == 0:
        return
    random_index = randint(0, len(E) - 1)
    arete = E[random_index]  # arete = [u, v]
    debug_log("\nArête contractée : " + str(arete))
    debug_log(" Liste d'arêtes avant contraction : " + str(E))
    # On remplace les u par v dans E
    for i in range(len(E)):
        if E[i][0] == arete[0]:
            E[i] = (arete[1], E[i][1])
        if E[i][1] == arete[0]:
            E[i] = (E[i][0], arete[1])
    # On supprime les boucles [v, v]
    for i in range(len(E), 0, -1):
        if E[i - 1][0] == E[i - 1][1]:
            del E[i - 1]
    # On remplace les u par v dans Contract
    for i in range(len(Contract)):
        if Contract[i] == arete[0]:
            Contract[i] = arete[1]
    debug_log(" Liste d'arêtes après contraction : " + str(E))
    debug_log(" Tableau des sommets contractés courant : " + str(Contract))


def CoupeMin(E: list, Contract: list) -> tuple[list, int]:
    debug_log("\n#################################################\n")
    debug_log("Nombre de sommets : " + str(len(Contract)))
    debug_log(" Liste d'arêtes initiale : " + str(E))
    debug_log(" Tableau des sommets contractés : " + str(Contract))
    n = len(Contract)
    for i in range(n - 2):
        debug_log("\nItération " + str(i + 1) + "/" + str(n - 2) + ":")
        ContractArete(E, Contract)
    return (list(dict.fromkeys(Contract)), len(E))


def LemmeRepetition(n: int) -> int:
    E, Contract = GenereGraph(n, 0.5)
    nb_aretes_min = -1
    for i in range(2 * (n**2)):
        nb_aretes = CoupeMin(E, Contract)[1]
        if nb_aretes_min == -1 or nb_aretes < nb_aretes_min:
            nb_aretes_min = nb_aretes
    return nb_aretes_min


def debug_log(s: str) -> None:
    print(s)


print("Génération du graphe")
E, Contract = GenereGraph(8, 0.3)
print("Graphe généré")
partition, nb_aretes = CoupeMin(E, Contract)
print("\n")
print("Partition : " + str(partition))
print("Nombre d'arêtes : " + str(nb_aretes))
print("\n")
