import matplotlib.pyplot as plt
from random import *
import time


def TableauAuHasard(n: int) -> list:
    TabHasard = []
    for i in range(n):
        TabHasard.append(randint(1, n * n))
    return TabHasard


def TriFusion(n: int, T: list) -> None:
    T[:] = tri_fusion_worker(n, T)


def tri_fusion_worker(n: int, T: list) -> list:
    if n <= 1:
        return T
    else:
        m = n // 2
        A = T[:m]
        B = T[m:]
        A = tri_fusion_worker(m, A)
        B = tri_fusion_worker(n - m, B)
        return fusion(A, B)


def fusion(A: list, B: list) -> list:
    if len(A) == 0:
        return B
    if len(B) == 0:
        return A
    if A[0] <= B[0]:
        return [A[0]] + fusion(A[1:], B)
    else:
        return [B[0]] + fusion(A, B[1:])


def TriBulles(n: int, T: list) -> None:
    for i in range(n - 1, 0, -1):
        for j in range(0, i):
            if T[j + 1] < T[j]:
                T[j + 1], T[j] = T[j], T[j + 1]


def Quicksort(n: int, T: list) -> None:
    if n > 1:
        p = T[randint(0, n - 1)]
        np = 0
        t0 = []
        t1 = []
        for i in range(0, n):
            if T[i] < p:
                t0.append(T[i])
            elif T[i] > p:
                t1.append(T[i])
            else:
                np += 1
        Quicksort(len(t0), t0)
        Quicksort(len(t1), t1)
        T[:] = t0 + [p] * np + t1


####### Programme Principal ########

choix = int(
    input(
        "Taper 1 pour un test sur un exemple simple, 2 pour un comparatif TriFusion/TriBulles/Quicksort/Sorted : "
    )
)
if choix == 1:
    n = 6
    T = TableauAuHasard(n)
    print("Tableau de départ :", T)

    Tbulles = T.copy()
    TriBulles(n, Tbulles)
    print("Après tri bulles :", Tbulles)

    Tfusion = T.copy()
    print("Tableau de départ :", Tfusion)
    TriFusion(n, Tfusion)
    print("Après tri fusion :", Tfusion)

    Tquick = T.copy()
    print("Tableau de départ :", Tquick)
    Quicksort(n, Tquick)
    print("Après QuickSort :", Tquick)

    Tsorted = T.copy()
    print("Tableau de départ :", Tsorted)
    Tsorted = sorted(Tsorted)
    print("Après sorted :", Tsorted)

else:
    # Valeurs de n choisies
    abscisses = [n for n in range(1, 1000, 10)]
    # Temps de calcul
    tps1 = []
    tps2 = []
    tps3 = []
    tps4 = []
    for n in range(1, 1000, 10):
        T = TableauAuHasard(n)
        T1 = T.copy()
        t = time.time()
        TriBulles(n, T1)
        tps1.append(time.time() - t)
        T2 = T.copy()
        t = time.time()
        TriFusion(n, T2)
        tps2.append(time.time() - t)
        T3 = T.copy()
        t = time.time()
        Quicksort(n, T3)
        tps3.append(time.time() - t)
        T4 = T.copy()
        t = time.time()
        sorted(T4)
        tps4.append(time.time() - t)

    # Tracé
    plt.plot(abscisses, tps1, label="Tri bulles")
    plt.plot(abscisses, tps2, label="Tri Fusion")
    plt.plot(abscisses, tps3, label="Quicksort")
    plt.plot(abscisses, tps4, label="Sorted")
    plt.legend(loc="upper left")
    plt.show()
