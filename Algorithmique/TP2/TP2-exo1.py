from time import *


def clause(s):
    L = s.split()
    return [int(v) for v in L[:-1]]


def parseur(nom):
    with open(nom) as f:
        F = []
        n = 0
        for ligne in f:
            if ligne[0] == "c":
                continue
            if ligne[0] == "p":
                L = ligne.split()
                n = int(L[-2])
            else:
                F.append(clause(ligne))
    return F, n


def affiche(F):
    s = ""
    for j in range(0, len(F)):
        C = F[j]
        s = s + "("
        for i in range(0, len(C)):
            if C[i] < 0:
                s = s + "¬"
            s = s + "x" + str(abs(C[i]))
            if i == len(C) - 1:
                s = s + ")"
            else:
                s = s + " ∨ "
        if j != len(F) - 1:
            s = s + " ∧ "
    return s


################################################################


def valide(F, A):
    taille_formule = len(F)
    clause_invalide_trouvee = False
    i = 0
    while i < taille_formule and not clause_invalide_trouvee:
        clause = F[i]
        taille_clause = len(clause)
        bon_litteral_trouve = False
        j = 0
        while j < taille_clause and not bon_litteral_trouve:
            variable = abs(clause[j])
            if variable > len(A):
                raise Exception("A n'a pas assez d'éléments !")
            valeur_inversion = clause[j] / variable
            if A[variable - 1] == valeur_inversion:
                bon_litteral_trouve = True
            j += 1
        if not bon_litteral_trouve:
            clause_invalide_trouvee = True
        i += 1

    return not clause_invalide_trouvee


################################################################


def aff_suivante(A):
    n = len(A)
    i = 0
    while i < n and A[i] == 1:
        A[i] = -1
        i += 1
        if i == n:
            return None
    A[i] = 1
    return A


def test_aff_suivante(n):
    A = [-1] * n
    while A is not None:
        print(A)
        A = aff_suivante(A)
    return None


#########################################################################


def sat_exhau(F, n):
    A = [-1] * n
    while not valide(F, A):
        A = aff_suivante(A)
        if A is None:
            return None
    return A


def elimination(F, n, b):
    psi = []
    for c in F:
        c_prime = []
        sat = False
        for l in c:
            if abs(l) == n and l * b > 0:
                sat = True
            elif abs(l) != n:
                c_prime.append(l)
        if not sat:
            psi.append(c_prime)
    return psi


def sat_backtrack(F, n):
    if len(F) == 0:
        return [1] * n
    for c in F:
        if len(c) == 0:
            return None
    for b in [1, -1]:
        psi = elimination(F, n, b)
        A = sat_backtrack(psi, n - 1)
        if A is not None:
            A.append(b)
            return A
    return None


###############################################################################


print("-------------------------------------------------------")
Fichier = "./cnf/random-41-unsat.cnf"
print("Formule du fichier: " + Fichier)
F, n = parseur(Fichier)
print("Récupérée sous forme de tableau: ", F)
print("Nombre de variables: ", n)
print("Formule SAT: ", affiche(F))
print("-------------------------------------------------------")
# print(valide(F, [1, 1, -1]))
# print(valide(F, [-1, -1, 1]))
# print("-------------------------------------------------------")
# print(test_aff_suivante(4))
print("-------------------------------------------------------")
# start_time = time()
# print(sat_exhau(F, n))
# print("Trouvé (exh) en", time() - start_time, "secondes")
start_time = time()
print(sat_backtrack(F, n))
print("Trouvé (btr) en", time() - start_time, "secondes")
