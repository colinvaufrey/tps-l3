from dessins import *
from math import sqrt
import random
import time


def distance(A: tuple[float, float], B: tuple[float, float]) -> float:
    return sqrt((B[0] - A[0]) ** 2 + (B[1] - A[1]) ** 2)


def aretes(P: list) -> list:
    n = len(P)
    result_list = []
    for i in range(n):
        for j in range(i + 1, n):
            result_list.append((i, j, distance(P[i], P[j])))
    return result_list


def pointsAleatoires(n: int, xmax: float, ymax: float) -> list:
    points = []
    for i in range(n):
        x = random.uniform(0, xmax)
        y = random.uniform(0, ymax)
        points.append((x, y))
    return points


def listesAdjacence(n: int, A: list) -> dict:
    adjacence = {}
    for i in range(n):
        sommets_adjacents = []
        for arete in A:
            if arete[0] == i and arete[1] not in sommets_adjacents:
                sommets_adjacents.append(arete[1])
            elif arete[1] == i and arete[0] not in sommets_adjacents:
                sommets_adjacents.append(arete[0])
        adjacence[i] = sommets_adjacents
    return adjacence


def arbreCouvrant(n: int, A: list) -> list:
    aretes_triees = sorted(A, key=lambda a: a[2])
    T = []

    comp = {}
    i = 1
    for sommet_index in range(n):
        comp[sommet_index] = i
        i += 1

    for arete in aretes_triees:
        u, v, _ = arete
        if comp[u] != comp[v]:
            T.append((u, v))
            aux = comp[u]
            for w in range(n):
                if comp[w] == aux:
                    comp[w] = comp[v]
    return T


def vdc2Approximation(listes_adjacence: dict) -> list:
    P = [0]
    C = []
    while P:
        s = P.pop()
        C.append(s)
        for v in listes_adjacence[s]:
            if v not in C:
                P.append(v)
    return C


def VdC(P: list) -> tuple[list, float]:
    A = aretes(P)
    arbre = arbreCouvrant(n, A)
    listes_a = listesAdjacence(n, arbre)
    parc = vdc2Approximation(listes_a)

    longueur = 0
    for i in range(len(parc)):
        index1, index2 = parc[i], parc[(i + 1) % len(parc)]
        A, B = P[index1], P[index2]
        longueur += distance(A, B)
    return (parc, longueur)


# # Tests distance
# A, B, C = (121, 77), (48, 70), (12, 72)
# print(distance(A, B), distance(A, C), distance(B, C))

# # Tests aretes
# P = [(6, 20), (67, 18), (96, 4), (32, 45)]
# print(aretes(P))

# # Tests pointsAleatoires
# random_points = pointsAleatoires(3, 10, 20)
# print(random_points)
# dessinPoints(random_points)

# # Tests listesAdjacence
# A = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)]
# points_al = pointsAleatoires(4, 10, 10)
# liste_adj = listesAdjacence(4, A)
# print(liste_adj)
# dessinGraphe(points_al, liste_adj)

# # Tests arbreCouvrant
# end_time = 0
# n = 4500
# P = []
# arbre = []
# A = []
# while end_time < 10:
#     xmax = 100
#     ymax = 100
#     P = pointsAleatoires(n, xmax, ymax)
#     A = aretes(P)
#     start_time = time.time()
#     arbre = arbreCouvrant(n, A)
#     end_time = time.time() - start_time
#     print("%i sommets" % n)
#     # print("Arbre :", arbre)
#     print("%f secondes de calcul" % end_time)
#     n += 100
# print("%i sommets" % n - 100)
# # dessinGraphe(P, listesAdjacence(n, A))
# dessinArbre(P, listesAdjacence(n, arbre), listesAdjacence(n, A))

# Tests deux_approximation
# P = [
#     (6, 60),
#     (67, 62),
#     (96, 76),
#     (32, 35),
#     (70, 39),
#     (98, 24),
#     (129, 30),
#     (121, 3),
#     (48, 10),
#     (12, 8),
# ]
# n = len(P)
# A = aretes(P)
# arbre = arbreCouvrant(n, A)
# listes_a = listesAdjacence(n, arbre)
# parc, longueur = VdC(P)
# print("Parcours :", parc)
# print("Longueur : %f" % longueur)
# dessinParcours(P, parc, listes_a)

end_time = 0
n = 3500
xmax = 100
ymax = 100
P = []
listes_a = []
longueur = 0.0
while end_time < 10:
    print("%i sommets" % n)
    P = pointsAleatoires(n, xmax, ymax)
    A = aretes(P)
    arbre = arbreCouvrant(n, A)
    start_time = time.time()
    parc, longueur = VdC(P)
    end_time = time.time() - start_time
    print("%f secondes de calcul" % end_time)
    n += 100
print("Longueur du parcours : %f" % longueur)
dessinParcours(P, parc, listes_a)