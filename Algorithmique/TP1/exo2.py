from random import *
from time import *
from matplotlib import pyplot as plt


def eltMajDet(T):
    found = False
    i = 0
    while not found and i < len(T):
        j = 0
        count = 1
        while not found and j < len(T):
            if T[i] == T[j]:
                count += 1
                if count >= len(T) / 2:
                    found = True
            j += 1
        i += 1
    return T[i]


def eltMajProba(T):
    found = False
    while not found:
        elt = choice(T)
        i = 0
        count = 0
        while not found and i < len(T):
            if elt == T[i]:
                count += 1
                if count >= len(T) / 2:
                    found = True
            i += 1
    return elt


def tabAlea(n, a, b, k):
    m = randint(a, b)
    tab = []
    while len(tab) < n - k:
        randomElt = randint(a, b)
        while randomElt == m:
            randomElt = randint(a, b)
        tab.append(randomElt)
    while len(tab) < n:
        tab.append(m)
    shuffle(tab)
    return tab


def tabDeb(n, a, b, k):
    m = randint(a, b)
    tab = []
    while len(tab) < k:
        tab.append(m)
    while len(tab) < n:
        randomElt = randint(a, b)
        while randomElt == m:
            randomElt = randint(a, b)
        tab.append(randomElt)
    return tab


def tabFin(n, a, b, k):
    m = randint(a, b)
    tab = []
    while len(tab) < n - k:
        randomElt = randint(a, b)
        while randomElt == m:
            randomElt = randint(a, b)
        tab.append(randomElt)
    while len(tab) < n:
        tab.append(m)
    return tab


# debut = time()
# print(eltMajDet(tabAlea(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajDet et tabAlea :",
#       time() - debut, "seconde(s)")

# debut = time()
# print(eltMajDet(tabDeb(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajDet et tabDeb :",
#       time() - debut, "seconde(s)")

# debut = time()
# print(eltMajDet(tabFin(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajDet et tabFin :",
#       time() - debut, "seconde(s)")

# debut = time()
# print(eltMajProba(tabAlea(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajProba et tabAlea :",
#       time() - debut, "seconde(s)")

# debut = time()
# print(eltMajProba(tabDeb(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajProba et tabDeb :",
#       time() - debut, "seconde(s)")

# debut = time()
# print(eltMajProba(tabFin(10000, 1, 10, randint(5000, 10000))))
# print("Temps écoulé pour les fonctions eltMajProba et tabFin :",
#       time() - debut, "seconde(s)")

# Résultats comparables pour les tableaux mélangés ou avec l'élément majoritaire au début,
# résultat catastrophiques pour l'aglorithme déterministe sur le tableau avec l'élément majoritaire à la fin.
# L'algorithme probabiliste est à privilégier dans ces situations.


def contientEltMaj(T, m):
    found = False
    testedElements = 0
    while not found and testedElements < m:
        elt = choice(T)
        i = 0
        count = 0
        while not found and i < len(T):
            if elt == T[i]:
                count += 1
                if count >= len(T) / 2:
                    found = True
            i += 1
        testedElements += 1
    return found


def testContient(n, a, b, k, m, N):
    successCount = 0
    for i in range(N):
        tab = tabAlea(n, a, b, k)
        if contientEltMaj(tab, m):
            successCount += 1
    return successCount / N


# kValues = []
# results = []
# for k in range(500, 1001, 50):
#     kValues.append(k)
#     results.append(testContient(1000, 1, 10, k, 1, 1000))
# print(results)
# plt.plot(kValues, results)
# plt.axis([500, 1000, 0, 1])
# plt.show()

# J'en conclus que les chances de tomber sur le bon résultat sont proportionnelles à la valeur de k / N

# mValues = []
# results = []
# for m in range(1, 11):
#     mValues.append(m)
#     results.append(testContient(1000, 1, 10, 500, m, 1000))
# print(results)
# plt.plot(mValues, results)
# plt.axis([1, 10, 0, 1])
# plt.show()

# Je conclus qu'augmenter le nombre d'éléments tirés permet d'augmenter rapidement l'efficacité de l'algorithme probabiliste
