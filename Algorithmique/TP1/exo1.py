from random import *
from matplotlib import pyplot as plt
from math import *


def entiersAleatoires(n, a, b) -> list:
    return [randint(a, b) for i in range(n)]


def entiersAleatoires2(n, a, b):
    return [randrange(a, b) for i in range(n)]


a = 1
b = 10

le1 = entiersAleatoires(1000, a, b)
le2 = entiersAleatoires2(1000, a, b)

# plt.hist(le1, bins=100)
# plt.hist(le2, bins=100)
# plt.show()


def flottantsAleatoires(n):
    return [random() for i in range(n)]


def flottantsAleatoires2(n, a, b):
    return [uniform(a, b) for i in range(n)]


a2 = -3
b2 = 10

lf1 = flottantsAleatoires(1000)
lf2 = flottantsAleatoires2(1000, a2, b2)

# plt.plot(lf1)
# plt.plot(lf2)
# plt.show()


def pointsDisque(n):
    points = []
    while len(points) < n:
        x = uniform(-1, 1)
        y = uniform(-1, 1)
        if x**2 + y**2 <= 1:
            points.append([x, y])
    return points


def pointsDisque2(n):
    points = []
    while len(points) < n:
        x = uniform(-1, 1)
        y = uniform(-1, 1)
        while x**2 + y**2 > 1:
            y = uniform(-1, 1)
        points.append([x, y])
    return points


def pointsDisque3(n):
    points = []
    while len(points) < n:
        theta = random() * 2 * pi
        r = random()
        points.append([r * cos(theta), r * sin(theta)])
    return points


def affichagePoints(L):
    X = [x for x, y in L]  # on recupere les abscisses ...
    Y = [y for x, y in L]  # ... et les ordonnees
    plt.scatter(X, Y, s=1)  # taille des points minimale
    plt.axis("square")  # meme echelle en abscisse et ordonnee
    plt.show()


lp1 = pointsDisque(10000)
lp2 = pointsDisque2(10000)
lp3 = pointsDisque3(10000)

# Uniforme
# affichagePoints(lp1)

# Non uniforme : les points sont concentrés les extrémités de x (quand les x tirés sont proches de -1 ou 1, les y tirés doivents obligatoirement être proches de 0)
# affichagePoints(lp2)

# Non uniforme : les points sont concentrés autour de (0, 0) (les points sont tirés uniformément sur le rayon or le cercle a une plus grande
# aire pour les rayons plus élevés, résulte donc une plus forte densité au centre)
# affichagePoints(lp3)


def aleatoireModulo(N):
    return getrandbits(N.bit_length()) % N


def aleatoireRejet(N):
    x = getrandbits(N.bit_length())
    while x >= N:
        x = getrandbits(N.bit_length())
    return x


# Non uniforme (concentration sur les nombres plus faibles)
leb1 = [aleatoireModulo(100) for i in range(10000)]

# Uniforme
leb2 = [aleatoireRejet(100) for i in range(10000)]

# plt.hist(leb1, bins=100)
# plt.hist(leb2, bins=100)
# plt.show()
