from matplotlib import pyplot as plt


def suivant(xn, a, c, m):
    return ((a * xn) + c) % m


def valeurs(x0, a, c, m, N):
    values = []
    if N > 0:
        values.append(x0)
        for i in range(N - 1):
            values.append(suivant(values[i], a, c, m))
    return values


# print(valeurs(0, 3, 5, 136, 20))
# Les termes suivant seront une reproduction des 16 premiers.

# print(valeurs(0, 75, 74, 2**16 + 1, 2**20))

# lCGen1 = valeurs(0, 16807, 0, 2**31 - 1, 1000)
# lCGen2 = valeurs(15235, 16807, 0, 2**31 - 1, 1000)
# lCGen3 = valeurs(185, 16807, 0, 2**31 - 1, 1000)

# plt.plot(lCGen1, "r.") # Ne fonctionne pas des masses avec 0 comme graîne...
# plt.plot(lCGen2, "r.")
# plt.plot(lCGen3, "r.")
# plt.show()


# list1 = valeurs(123489, 16807, 0, 2**31 - 1, 10000)
# list2 = valeurs(25743, 16807, 0, 2**31 - 1, 10000)

# plt.scatter(list1, list2, s=.2)
# plt.show()

# list1 = valeurs(123488, 16807, 0, 2**31 - 1, 10000)
# list2 = valeurs(123489, 16807, 0, 2**31 - 1, 10000)

# plt.scatter(list1, list2, s=.2)
# plt.show()

# list1 = valeurs(5, 16807, 0, 2**31 - 1, 10000)
# list2 = valeurs(6, 16807, 0, 2**31 - 1, 10000)

# plt.scatter(list1, list2, s=.2)
# plt.show()

# Je conclus qu'il faut des graînes de valeur élevée pour avoir des résultats décorrelés.


class Generateur:
    def __init__(self, a: int, c: int, m: int) -> None:
        self.__a: int = a
        self.__c: int = c
        self.__m: int = m
        self.__x: int = None

    def graine(self, g: int) -> None:
        self.__x = g

    def aleatoire(self) -> int:
        if not self.__x:
            raise ValueError(
                "Graîne non définie. Utilisez la méthode graine(graine: int) pour définir une nouvelle graîne."
            )
        self.__x = ((self.__a * self.__x) + self.__c) % self.__m
        return self.__x


g = Generateur(3, 5, 17)
g.graine(5)
print(g.aleatoire())
