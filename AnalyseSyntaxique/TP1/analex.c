/**
 * @file analex.c
 * @author Michel Meynard
 * @brief Prog principal appelant analex()
 */
#include "analex.h" /* Définition de la fon : int analex() */
#include "afd.h"	/* Définition de l'AFD et des JETONS */
#include <stdio.h>
#include <string.h>


int main() {
	/* Construction de l'AFD */
	int j; /* jeton retourné par analex() */
	char *invite = "Saisissez un texte suivi de EOF (CTRL-D) SVP : ";
	creerAfd();				 /* Construction de l'AFD à jeton */
	printf("%s", invite);	 /* prompt */
	while ((j = analex())) { /* analyser tq pas jeton 0 */
		printf("\nRésultat : Jeton = %d ; Lexeme = %s\n", j, lexeme);
	}
	return 0;
}
