/**
 * @file afd.h
 * @brief Définition d'un AFD du langage C
 * @author Michel Meynard
 */

#define EINIT 0
#define EI 1
#define EIF 2
#define EID 3
#define ESEP 4
#define ECHIFFRE 5
#define EFLOAT 6
#define EP 7
#define ESLASH 8
#define ESLASHET 9
#define ESLASHDET 10
#define ECOM2 11
#define EDSLASH 12
#define ECOM 13

#define NBETAT 14

int TRANS[NBETAT][256]; /* table de transition : état suivant */
int JETON[NBETAT];		/* jeton (1-32000) ou non final (0) ? */

/** construit un ensemble de transitions de ed à ef pour un intervale de char
 *@param ed l'état de départ
 *@param ef l'état final
 *@param cd char de début
 *@param cf char de fin
 */
void classe(int ed, int cd, int cf, int ef) {
	for (int i = cd; i <= cf; i++) {
		TRANS[ed][i] = ef;
	}
}

void creerAfd() { /* Construction de l'AFD */
	int i;
	int j; /* variables locales */
	for (i = 0; i < NBETAT; i++) {
		for (j = 0; j < 256; j++)
			TRANS[i][j] = -1; /* init vide */
		JETON[i] = 0;		  /* init tous états non finaux */
	}

	/* Transitions de l'AFD */
	classe(EIF, 'a', 'z', EID);
	classe(EI, 'a', 'z', EID);
	TRANS[EI]['f'] = EIF;
	classe(EINIT, 'a', 'z', EID);
	classe(EID, 'a', 'z', EID);
	TRANS[EINIT]['i'] = EI;

	TRANS[EINIT]['\n'] = ESEP;
	TRANS[EINIT]['\t'] = ESEP;
	TRANS[EINIT][' '] = ESEP;
	TRANS[ESEP]['\n'] = ESEP;
	TRANS[ESEP]['\t'] = ESEP;
	TRANS[ESEP][' '] = ESEP;

	classe(EINIT, '0', '9', ECHIFFRE);
	classe(ECHIFFRE, '0', '9', ECHIFFRE);
	TRANS[ECHIFFRE]['.'] = EFLOAT;
	TRANS[EINIT]['.'] = EP;
	classe(EP, '0', '9', EFLOAT);
	classe(EFLOAT, '0', '9', EFLOAT);

	TRANS[EINIT]['/'] = ESLASH;
	TRANS[ESLASH]['*'] = ESLASHET;
	classe(ESLASHET, 0, 255, ESLASHET);
	TRANS[ESLASHET]['*'] = ESLASHDET;
	classe(ESLASHDET, 0, 255, ESLASHET);
	TRANS[ESLASHDET]['*'] = ESLASHDET;
	TRANS[ESLASHDET]['/'] = ECOM2;
	TRANS[ESLASH]['/'] = EDSLASH;
	classe(EDSLASH, 0, 255, EDSLASH);
	TRANS[EDSLASH]['\n'] = ECOM;

	JETON[EI] = 300;
	JETON[EIF] = 301;
	JETON[EID] = 300;
	JETON[ESEP] = -300;
	JETON[ECHIFFRE] = 302;
	JETON[EFLOAT] = 303;
	JETON[ECOM2] = -300;
	JETON[ECOM] = -300;
}
