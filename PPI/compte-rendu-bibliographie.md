# Compte rendu bibliographie ‑ EHR

## Informations de Santé Protégées (PHI)

- Statut de santé
- Aide apportée
- Paiments

## Dossier Mécical Électronique (EMR)

- Ancien nom de EHR
- Maintenant fonctions de EHR

## Dossier Patient Informatisé (EHR ‑ Electronic Health Record)

- Rapport électonique
- Modifié par les médecins
- Pour une organisation de santé
- Accès par logiciels tiers

### Fonctionnalités

- Rapport de santé
- Démographie
- Listes de problèmes
- Listes de médicaments
- Historique des patients
- Documents cliniques
- Récupérer des données externes
- Présenter des plans de soins
- Gérer les protocols et les règles
- Infos de contact
- Allergies
- Infos assurance
- Histoire familiale
- Immunisations
- Opérations
- Accès aux infos par les patients ?

### Avantages

- Partage facile des informations
- Stockage et accès facile
- Standardisation
- Généralisation des informations
- Moins de redondance
- Aide à la décision
- Coût moindre pour les organisations de santé
- Potentiels avantage écologique (moins de papier)
- Lisibilité (les médecins écrivent très mal :D)

### Inconvénients

- Temps de formation
- Sécurité (piratages, etc.)
- Suivant la taille de l'organisation, le coût peut être élevé

### Types

- Locaux
- Cloud (devient standard)

### Existants

- OpenEMR (2018 : 20 failles de sécurité)
- Philips Tasy EMR (2019 : 2 failles de sécurité)