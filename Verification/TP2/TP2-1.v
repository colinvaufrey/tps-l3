(* TP2 : exo3 *)

Open Scope type_scope.

Section Iso_axioms.

Variables x y z : Set.

Axiom Com : x * y = y * x.
Axiom Ass : x * (y * z) = x * y * z.
Axiom Cur : (x * y -> z) = (x -> y -> z).
Axiom Dis : (x -> y * z) = (x -> y) * (x -> z).
Axiom P_unit : x * unit = x.
Axiom AR_unit : (x -> unit) = unit.
Axiom AL_unit : (unit -> x) = x.

End Iso_axioms.

Lemma isos_ex1 : forall A B : Set, A * (B -> unit) = A.
Proof.
  intros.
  rewrite AR_unit.
  rewrite P_unit.
  reflexivity.
Qed.

Lemma isos_ex2 : forall A B : Set, A * unit * B = B * (unit * A).
Proof.
  intros.
  rewrite (Com (A * unit) B).
  rewrite (Com A unit).
  reflexivity.
Qed.

Lemma isos_ex3 : forall A B C : Set,
  (A * unit -> B * (C * unit)) =
  (A * unit -> (C -> unit) * C) * (unit -> A -> B).
Proof.

(*
Ltac simplifie :=
  intros;
  repeat
    (rewrite P_unit || rewrite P_unit_com || rewrite AR_unit || rewrite AL_unit ||
      rewrite Dis);
  try reflexivity.

repeat (rewrite P_unit_com || rewrite P_unit).
Répéter commande, peut utiliser alternatives si ne fonctionne pas
*)