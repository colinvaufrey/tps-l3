Parameter A: Set.
Open Scope list_scope.
Require Export List.
Import ListNotations.
Require Import Arith.
Require Import Lia.

Inductive is_perm : list nat -> list nat -> Prop :=
|is_perm_refl : forall l : list nat, is_perm l l
|is_perm_av_ar : forall (e : nat) (l : list nat), is_perm (e::l) (l++e :: nil)
|is_perm_cons : forall (a:nat) (l1 l2 : list nat), is_perm l1 l2 -> is_perm (a:: l1) (a::l2)
|is_perm_sym : forall l1 l2 : list nat, is_perm l1 l2 -> is_perm l2 l1
|is_perm_trans : forall l1 l2 l3 : list nat, is_perm l1 l2 -> is_perm l2 l3 -> is_perm l1 l3.


Goal is_perm [1;2;3;4] [3;4;1;2].
Proof.
  apply (is_perm_trans [1;2;3;4] [2;3;4;1] [3;4;1;2]).
  apply is_perm_av_ar.
  apply is_perm_av_ar.
Qed.

Goal is_perm [1;2;3;4] [3;2;1;4].
Proof.
 apply (is_perm_trans [1;2;3;4] [2;3;4;1] [3;2;1;4]).
   apply is_perm_av_ar.
 apply (is_perm_trans [2;3;4;1] [3;4;1;2] [3;2;1;4]).
   apply is_perm_av_ar.
   apply is_perm_cons.
   apply (is_perm_trans [4;1;2] [2;4;1] [2;1;4]).
     apply is_perm_sym.
     apply is_perm_av_ar.
     apply is_perm_cons.
    apply is_perm_av_ar.
Qed.

(*
Goal is_perm [1;2;3] [3;2;1].
Proof.
  apply (is_perm_trans 
*)


(*
Is_sorted de facon inductive.
base : 
  is_sorted nil
  is_sorted [e]
induction :
  is_sorted e1<=e2 et que is_sorted e2::l alors is_sorted e1::e2::l
*)

Inductive is_sorted : list nat -> Prop :=
| is_sorted_nil : is_sorted []
| is_sorted_sing : forall e: nat, is_sorted [e]
| is_sorted_cons : forall (e1 e2 : nat) (l : list nat),
  e1 <= e2 -> is_sorted (e2::l) -> is_sorted (e1::e2::l).

Goal is_sorted [1;2;3;4].
Proof.
  apply is_sorted_cons.
    auto.

    apply is_sorted_cons.
      auto.

      apply is_sorted_cons.
        auto.
        apply is_sorted_sing.
Qed.

Check le_dec.
Print sumbool.

(*
Comprendre cet import pour la suite et le_dec est de type sumbool.
Donc le_dec n 10 est une 'macro' pour 
|left : n<=10 -> {n<=10} + {~ n <=10}
|right : ~ n<=10 -> {n<=10} + {~ n <=10}
*)

Definition le_10 (n : nat) : bool :=
match (le_dec n 10) with
|left _ => true
|right _ => false
end.
(*left _ => tout ce qui commence par left*)
(*right _ => tout ce qui commence par right*)
Eval compute in (le_10 3).
Eval compute in (le_10 12).

Fixpoint insert (e:nat) (l : list nat) {struct l} : list nat :=
match (l) with
| nil => [e]
| h::q => match (le_dec e h) with
    |left _ => e::l
    |right _ => h::(insert e q)
    end
end.

(*_ signifie 'tous les autres cas'*)

Eval compute in (insert 2 [1;3]).

Fixpoint isort (l: list nat) {struct l} : list nat :=
match (l) with
| nil => nil
| h::q => insert h (isort q)
end.

Eval compute in (isort [5;4;3;2;1]).

Lemma head_is_perm : forall (x1 x2 : nat) (l : list nat),
is_perm (x1::x2::l) (x2::x1::l).
Proof.
  intro.
  intro.
  intro.
  apply (is_perm_trans (x1::x2::l) ((x2 :: l)++[x1]) (x2::x1::l)).
    apply is_perm_av_ar.
    simpl.
    apply is_perm_cons.
    apply is_perm_sym.
    apply is_perm_av_ar.
Qed.

Lemma insert_is_perm : forall (x : nat) (l: list nat),
is_perm (x::l) (insert x l).
Proof.
  intro.
  induction l.
    simpl.
    apply is_perm_refl.
    simpl.
    elim (le_dec x a).
      intro.
      apply is_perm_refl.

      intro.
      apply (is_perm_trans (x::a::l) (a:: x ::l) (a::insert x l)).
        apply head_is_perm.
        apply is_perm_cons.
        apply IHl.

Qed.

Lemma insert_is_sorted : forall (x : nat) (l : list nat),
is_sorted l -> is_sorted (insert x l).
Proof.
intros.
elim H.
  simpl.
  apply is_sorted_sing.

  simpl.
  intros.
  elim (le_dec x e).
    intro.
    apply is_sorted_cons.
      apply a.

      apply is_sorted_sing.

        intro.
        apply is_sorted_cons.
          lia.
          apply is_sorted_sing.

intro.
intro.
intro.
intro.
intro.

simpl.
elim (le_dec x e2).
  elim (le_dec x e1).
  intros.
  apply is_sorted_cons.
  lia.
  apply is_sorted_cons.
  lia.

  apply H1.

  intros.
  apply is_sorted_cons.
  lia.
  apply is_sorted_cons.
  lia.
  
  apply H1.

  elim (le_dec x e1).
  
  intros.
  apply is_sorted_cons.
  lia.
  apply is_sorted_cons.
  lia.

  apply H1.

  intros.
  apply is_sorted_cons.
  lia.

  apply H2.
Qed.

Lemma isort_correct : forall (l l' : list nat),
  l' = isort l -> is_perm l l' /\ is_sorted l'.
Proof.
  intro.
  induction l.
    simpl.
    intros.
    split.
      rewrite H.
      apply is_perm_refl.
      
      rewrite H.
      apply is_sorted_nil.
     
    intro.
    intro.
    elim (IHl (isort l)).
    
    intro.
    intro.
    split.
    rewrite H.
    apply (is_perm_trans (a::l) (a::(isort l)) (isort (a::l))).
    apply is_perm_cons.
    apply H0.

    simpl.
    apply insert_is_perm.
    
    rewrite H.
    simpl.
    apply insert_is_sorted.
    assumption.
    reflexivity.
Qed.