Fixpoint mult (n m : nat) {struct n} : nat :=
  match n with
  | 0 => 0
  | S p => plus (mult p m) m
  end.

Lemma aux0 : forall n, (plus n 2) = S (S n).
Proof.
  intro.
  elim n.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite H.
  reflexivity.
Qed.

Lemma aux1 : forall n m, (plus n (S m)) = S (plus n m).
Proof.
  intro.
  elim n.
  intro.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite (H m).
  reflexivity.
Qed.

Lemma proof1 : forall n : nat, (mult 2 n) = (plus n n).
Proof.
  intro.
  simpl.
  reflexivity.
Qed.

Lemma proof2 : forall n : nat, (mult n 2) = (plus n n).
Proof.
  apply nat_ind.
  (*intro.
  elim n.*)
    simpl.
    reflexivity.

    intro.
    intro.
    simpl.
    (*rewrite H*)
    rewrite aux0.
    rewrite aux1.
    rewrite H.
    reflexivity.
Qed.

Open Scope list.

Parameter A : Type.

Require Export List.
Import ListNotations.

Check ([]).
Check ([1;2]).

Fixpoint reverse (l : list A) {struct l} : list A :=
  match l with
  | nil => nil
  | t::q => app (reverse q) (t::nil)
  end.

Lemma proof2_2 : forall l : list A, forall a : A, (reverse (app l [a])) = a::reverse(l).
  intro.
  intro.
  elim l.
    simpl.
    reflexivity.

    intros.
    simpl.
    rewrite H.
    reflexivity.
Qed.

Axiom complus : forall k n : nat, plus k n = plus n k.

(*
Lemma complus2 : forall k n : nat, plus k n = plus n k.
  intros.
Qed.
*)

Lemma proof1_3 : forall n : nat, mult n 2 = plus n n.
  apply nat_ind.
    simpl.
    reflexivity.
    
    intros.
    simpl.
    rewrite (complus n (S n)).
    rewrite (complus (mult n 2) 2).
    rewrite H.
    simpl.
    reflexivity.
Qed.

(*
Lemma proof2_3 : forall l : list A, (reverse (reverse l)) = l.
  intros.
  elim l.
    simpl.
    reflexivity.

    
Qed.*)