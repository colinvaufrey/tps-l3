Require Export List.
Import ListNotations.

Open Scope list.

Inductive is_perm : list nat -> list nat -> Prop :=
| is_perm_cons : forall i : nat, forall l1 l2 : list nat,
    (is_perm l1 l2) -> (is_perm (i::l1) (i::l2))
| is_perm_nil : is_perm nil nil
| is_perm_last : forall i : nat, forall l1 l2 : list nat,
    (is_perm l1 l2) -> (is_perm (l1++[i]) (l2++[i]))
| is_perm_cons_last : forall i : nat, forall l1 l2 : list nat,
    (is_perm l1 l2) -> (is_perm (i::l1) (l2++[i])).

Goal is_perm [1;2;3] [3;2;1].
Proof.