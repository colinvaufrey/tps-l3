(*
    CC Colin VAUFREY 22/03/2023
*)

(*
    Exercice 1
*)

Inductive bin : Set :=
| C0 : bin
| C1 : bin
| C0S : bin -> bin
| C1S : bin -> bin.

Inductive is_even : bin -> Prop :=
| is_even_C0 : is_even C0
| is_even_C0S : forall b : bin, is_even (C0S b).

Goal is_even (C0S (C1S C1)).
Proof.
	apply is_even_C0S.
Qed.

(*
	Exercice 2
*)

Fixpoint head_0 (b : bin) : bin :=
	match b with
	| C0 => C0S C0
	| C1 => C1S C0
	| C0S b => C0S (head_0 b)
	| C1S b => C1S (head_0 b) 
	end.

Compute (head_0 (C0S (C1S C1))).

(*
	Exercice 3
*)

Inductive is_equal : bin -> bin -> Prop :=
| is_equal_base : forall b : bin, (is_equal b b)
| is_equal_C0_C0S : (is_equal C0 (C0S C0))
| is_equal_C0_C1S : (is_equal C1 (C1S C0))
| is_equal_ref : forall b c : bin, (is_equal b c) -> (is_equal c b)
| is_equal_trans_C0 : forall b c : bin, (is_equal b c) -> (is_equal (C0S b) (C0S c))
| is_equal_trans_C1 : forall b c : bin, (is_equal b c) -> (is_equal (C1S b) (C1S c)).

(*
Goal (is_equal (C0S (C1S C1)) (C0S (C1S (C1S (C0S C0))))).
Proof.
	
Qed.
*)

(*
	Exercice 4
*)

Definition mult_2 (b : bin) :=
	match b with
	| b => (C0S b)
	end.

Compute (mult_2 (C0S (C1S C1))).

(*
	Exercice 5
*)

Goal forall b: bin, is_even b -> exists c : bin, (is_equal b (mult_2 c)).
Proof.
	intro.
	intro.
Qed.