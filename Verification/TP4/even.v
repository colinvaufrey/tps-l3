Inductive is_even : nat -> Prop :=
| is_even_O : is_even 0
| is_even_S : forall n : nat, is_even n -> is_even (S (S n)).

Fixpoint even (n : nat) : Prop :=
match n with
| 0 => True
| 1 => False
| (S (S n)) => even n
end.