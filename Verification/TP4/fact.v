Inductive is_fact : nat -> nat -> Prop :=
| is_fact_O : (is_fact 0 1)
| is_fact_S : forall m fm : nat, (is_fact m fm) -> (is_fact (S m) (mult (S m) fm)).

Fixpoint fact (n : nat) {struct n} : nat :=
match n with
| 0 => 1
| (S n) => (mult (S n) (fact n))
end.

Print is_fact_ind. (* Afficher schéma induction *)

Goal fact 3 = 6.
Proof.
	simpl.
	reflexivity.
Qed.

Lemma fact_sound : forall (n f : nat), fact n = f -> is_fact n f.
Proof.
	induction n.
		intro.
		intro.
		rewrite <- H.
		apply is_fact_O.

		intro.
		intro.
		rewrite <- H.
		simpl.
		apply is_fact_S.
		apply IHn.
		reflexivity.
Qed.

Require Import FunInd.

Functional Scheme fact_ind := Induction for fact Sort Prop.

Print fact_ind.

Lemma fact_sound_bis : forall (n f : nat), fact n = f -> is_fact n f.
Proof.
	intro.
	functional induction (fact n).
		intro.
		intro.
		rewrite <- H.
		apply is_fact_O.

		intro.
		intro.
		rewrite <- H.
		simpl.
		apply is_fact_S.
		apply IHn0.
		reflexivity.
Qed.

Lemma fact_complete : forall (n f : nat), is_fact n f -> fact n = f.
Proof.
	intro.
	intro.
	intro.
	elim H.
		reflexivity.

		intros.
		simpl.
		rewrite <- H1.
		reflexivity.
Qed.
