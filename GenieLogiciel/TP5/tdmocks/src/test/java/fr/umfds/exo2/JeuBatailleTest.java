package fr.umfds.exo2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JeuBatailleTest {

    JeuBataille jeuBataille;
    @Mock
    JeuDeCartes jeuDeCartes;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void round1card() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> cartesJ1 = new ArrayDeque<>();
        cartesJ1.add(new Carte(1, Couleur.trefle));
        cartesJ1.add(new Carte(2, Couleur.trefle));

        ArrayDeque<Carte> cartesJ2 = new ArrayDeque<>();
        cartesJ2.add(new Carte(10, Couleur.carreau));
        cartesJ2.add(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> cartes = new ArrayList<>();
        cartes.add(cartesJ1);
        cartes.add(cartesJ2);

        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(cartes);

        jeuBataille = new JeuBataille(jeuDeCartes); // ne pas instancier avant : jeuDeCartes n'est pas instancié !

        assertEquals(2, jeuBataille.tailleJeuJoueur1());
        assertEquals(2, jeuBataille.tailleJeuJoueur2());

        boolean jeuFini = jeuBataille.round();

        assertEquals(1, jeuBataille.tailleJeuJoueur1());
        assertEquals(3, jeuBataille.tailleJeuJoueur2());

        assertEquals(false, jeuFini);
    }

    @Test
    void round2cards() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> cartesJ1 = new ArrayDeque<>();
        cartesJ1.add(new Carte(1, Couleur.trefle));
        cartesJ1.add(new Carte(12, Couleur.trefle));
        cartesJ1.add(new Carte(3, Couleur.trefle));

        ArrayDeque<Carte> cartesJ2 = new ArrayDeque<>();
        cartesJ2.add(new Carte(1, Couleur.carreau));
        cartesJ2.add(new Carte(3, Couleur.carreau));
        cartesJ2.add(new Carte(8, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> cartes = new ArrayList<>();
        cartes.add(cartesJ1);
        cartes.add(cartesJ2);

        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(cartes);

        jeuBataille = new JeuBataille(jeuDeCartes); // ne pas instancier avant : jeuDeCartes n'est pas instancié !

        assertEquals(3, jeuBataille.tailleJeuJoueur1());
        assertEquals(3, jeuBataille.tailleJeuJoueur2());

        boolean jeuFini = jeuBataille.round();

        assertEquals(5, jeuBataille.tailleJeuJoueur1());
        assertEquals(1, jeuBataille.tailleJeuJoueur2());

        assertEquals(false, jeuFini);
    }

    @Test
    void round3cards() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> cartesJ1 = new ArrayDeque<>();
        cartesJ1.add(new Carte(1, Couleur.trefle));
        cartesJ1.add(new Carte(2, Couleur.trefle));
        cartesJ1.add(new Carte(3, Couleur.trefle));
        cartesJ1.add(new Carte(6, Couleur.trefle));

        ArrayDeque<Carte> cartesJ2 = new ArrayDeque<>();
        cartesJ2.add(new Carte(1, Couleur.carreau));
        cartesJ2.add(new Carte(2, Couleur.carreau));
        cartesJ2.add(new Carte(7, Couleur.carreau));
        cartesJ2.add(new Carte(8, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> cartes = new ArrayList<>();
        cartes.add(cartesJ1);
        cartes.add(cartesJ2);

        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(cartes);

        jeuBataille = new JeuBataille(jeuDeCartes); // ne pas instancier avant : jeuDeCartes n'est pas instancié !

        assertEquals(4, jeuBataille.tailleJeuJoueur1());
        assertEquals(4, jeuBataille.tailleJeuJoueur2());

        boolean jeuFini = jeuBataille.round();

        assertEquals(1, jeuBataille.tailleJeuJoueur1());
        assertEquals(7, jeuBataille.tailleJeuJoueur2());

        assertEquals(false, jeuFini);
    }

    @Test
    void winner() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> cartesJ1 = new ArrayDeque<>();
        cartesJ1.add(new Carte(10, Couleur.trefle));
        cartesJ1.add(new Carte(12, Couleur.trefle));

        ArrayDeque<Carte> cartesJ2 = new ArrayDeque<>();
        cartesJ2.add(new Carte(1, Couleur.carreau));
        cartesJ2.add(new Carte(2, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> cartes = new ArrayList<>();
        cartes.add(cartesJ1);
        cartes.add(cartesJ2);

        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(cartes);

        jeuBataille = new JeuBataille(jeuDeCartes); // ne pas instancier avant : jeuDeCartes n'est pas instancié !

        while (jeuBataille.round())
            ;

        boolean jeuFini = jeuBataille.round();

        assertEquals(true, jeuFini);
        assertEquals(1, jeuBataille.numJoueurGagnant());
    }

    @Test
    void winnerDuringRound() throws CarteIncorrecteException, Exception {
        ArrayDeque<Carte> cartesJ1 = new ArrayDeque<>();
        cartesJ1.add(new Carte(2, Couleur.trefle));
        cartesJ1.add(new Carte(1, Couleur.trefle));
        cartesJ1.add(new Carte(3, Couleur.trefle));

        ArrayDeque<Carte> cartesJ2 = new ArrayDeque<>();
        cartesJ2.add(new Carte(13, Couleur.carreau));
        cartesJ2.add(new Carte(1, Couleur.carreau));
        cartesJ2.add(new Carte(3, Couleur.carreau));

        ArrayList<ArrayDeque<Carte>> cartes = new ArrayList<>();
        cartes.add(cartesJ1);
        cartes.add(cartesJ2);

        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(cartes);

        jeuBataille = new JeuBataille(jeuDeCartes); // ne pas instancier avant : jeuDeCartes n'est pas instancié !

        jeuBataille.round();

        assertEquals(2, jeuBataille.tailleJeuJoueur1());
        assertEquals(4, jeuBataille.tailleJeuJoueur2());

        boolean jeuFini = jeuBataille.round();

        assertEquals(true, jeuFini);
        assertEquals(2, jeuBataille.numJoueurGagnant());
    }
}