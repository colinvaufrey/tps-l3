package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JeuDeCartesTest {

    @InjectMocks
    JeuDeCartes jeu;
    @Mock
    Random random;

    @Test
    void piocheDerniereCarte() {
        when(random.nextInt(anyInt())).thenReturn(51);
        Carte cartePiochee = jeu.pioche();
        assertEquals(13, cartePiochee.getHauteur());
        assertEquals(Couleur.trefle, cartePiochee.getCouleur());
    }

    @Test
    void piochePremiereCarte() {
        when(random.nextInt(anyInt())).thenReturn(0);
        Carte cartePiochee = jeu.pioche();
        assertEquals(1, cartePiochee.getHauteur());
        assertEquals(Couleur.pique, cartePiochee.getCouleur());
    }

    @Test
    void distribution() throws Exception {
        when(random.nextInt(52)).thenReturn(0);
        when(random.nextInt(51)).thenReturn(0);
        when(random.nextInt(50)).thenReturn(0);
        when(random.nextInt(49)).thenReturn(48);
        when(random.nextInt(48)).thenReturn(47);
        when(random.nextInt(47)).thenReturn(46);

        ArrayList<ArrayDeque<Carte>> paquets = jeu.distribution(3, 2);

        ArrayDeque<Carte> paquetJ1 = paquets.get(0);
        ArrayDeque<Carte> paquetJ2 = paquets.get(1);

        Carte carte1 = paquetJ1.pop();
        assertEquals(1, carte1.getHauteur());
        assertEquals(Couleur.pique, carte1.getCouleur());

        Carte carte2 = paquetJ1.pop();
        assertEquals(2, carte2.getHauteur());
        assertEquals(Couleur.pique, carte2.getCouleur());

        Carte carte3 = paquetJ1.pop();
        assertEquals(3, carte3.getHauteur());
        assertEquals(Couleur.pique, carte3.getCouleur());

        Carte carte4 = paquetJ2.pop();
        assertEquals(13, carte4.getHauteur());
        assertEquals(Couleur.trefle, carte4.getCouleur());

        Carte carte5 = paquetJ2.pop();
        assertEquals(12, carte5.getHauteur());
        assertEquals(Couleur.trefle, carte5.getCouleur());

        Carte carte6 = paquetJ2.pop();
        assertEquals(11, carte6.getHauteur());
        assertEquals(Couleur.trefle, carte6.getCouleur());
    }
}