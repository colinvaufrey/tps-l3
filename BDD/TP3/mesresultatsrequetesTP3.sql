/*
USAGE :
Penser à remplir les champs Numéro carte, nom, prénom, date.

Utiliser ce fichier pour répondre aux différentes questions. 
Pour chaque requete vous avez le résultat que vous devez obtenir avec les données de la base BIBLIOTHEQUE.

Les requêtes sont affichées

Si vous ne voulez pas afficher à chaque fois vos requêtes vous pouvez les mettre en commentaire. 
Attention sous ORACLE pour les marques des commentaires (le slash et l'étoile) doivent être seuls sur une ligne.

 */
/*
Numéro de carte étudiant : 
Nom : 
Prénom : 
Date : 
 */
/*
Mise en page - Ne pas toucher
 */
CLEAR SCREEN
SET
    PAGESIZE 30 COLUMN COLUMN_NAME FORMAT A30
SET
    LINESIZE 300;

prompt --- Q3 : modification de la relation mot_clefs
ALTER TABLE
    Mot_Clef
ADD
    parent VARCHAR(16) REFERENCES Mot_Clef (mot);

/*
Table modifiee.
 */
prompt --- Q4 : ajout de la date de naissance des abonnés, du type parmi  {‘ADULTE’, ‘ENFANT}) et de leur catégorie dont les valeurs admissibles sont : {‘REGULIER’, ‘OCCASIONNEL’, ‘A PROBLEME’, ‘EXCLU’}.
ALTER TABLE
    Abonne
ADD
    (
        date_nai DATE,
        type_ab VARCHAR(16) CONSTRAINT CONST_TYPE_AB CHECK (type_ab IN ('ADULTE', 'ENFANT')),
        cat_ab VARCHAR(16) CONSTRAINT CONST_CAT_AB CHECK (
            cat_ab IN ('REGULIER', 'OCCASIONNEL', 'A PROBLEME', 'EXCLU')
        )
    );

/*
Table modifiee.
 */
prompt --- Q4 : describe
DESCRIBE Abonne;

/*
Exemple de résultat obtenu en fonction des types retenuss
Nom		NULL ?		Type
--------------------------------------------------
NUM_AB 	NOT NULL 	NUMBER(6)
NOM		NOT NULL 	VARCHAR2(15)
PRENOM 			VARCHAR2(20)
VILLE				VARCHAR2(13)
AGE				NUMBER(3)
TARIF				NUMBER(3)
REDUC				NUMBER(3)
DATE_NAI			DATE
TYPE_AB			VARCHAR2(6)
CAT_AB 			VARCHAR2(12)
 */
prompt -- Q4 : mises à jour qui violent la contrainte
UPDATE
    Abonne
SET
    type_ab = 'ADO'
WHERE
    num_ab = 902043;

UPDATE
    Abonne
SET
    cat_ab = 'THE_BEST'
WHERE
    num_ab = 902043;

/*
Exemples d'erreurs obtenues :
ERREUR a la ligne 1 :
ORA-02290: violation de contraintes (P00000008868.DOM_CAT_AB) de verification

ORA-12899: valeur trop grande pour la colonne "P00000008868"."ABONNE"."TYPE_AB" (reelle : 8, maximum
: 6)
 */
prompt --- Q5 - Augmentation de la taille des noms
ALTER TABLE
    Abonne
MODIFY
    (nom VARCHAR(20));

/*
Table modifiee.
 */
prompt --- Q5 - Verification que la taille des noms a bien été prise en compte
DESC Abonne;

/*
Par rapport à la version précédente, le type de NOM a bien été modifié
Nom			NULL ?		Type
-----------------------------------------------------
NUM_AB 		NOT NULL 	NUMBER(6)
NOM			NOT NULL 	VARCHAR2(20)
PRENOM 				VARCHAR2(20)
VILLE					VARCHAR2(13)
AGE					NUMBER(3)
TARIF					NUMBER(3)
REDUC					NUMBER(3)
DATE_NAI				DATE
TYPE_AB				VARCHAR2(6)
CAT_AB 				VARCHAR2(12)
 */
prompt --- Q6 - Ajout des Auteurs dans la base
CREATE TABLE
    Auteur (
        num_Auteur NUMERIC(6, 0) PRIMARY KEY,
        nom VARCHAR(20),
        prenom VARCHAR(20),
        nationalite VARCHAR(20)
    );

CREATE TABLE
    AuteurLivre (
        num_Auteur NUMERIC(6, 0) REFERENCES Auteur (num_Auteur),
        isbn VARCHAR(15) REFERENCES Livre (isbn),
        PRIMARY KEY (num_Auteur, isbn)
    );

/*
Table creee.


Table creee.
 */
prompt --- Q7 - Ajout de données dans le thésaurus
INSERT INTO
    Mot_Clef
VALUES
    ('INDEX', NULL);

UPDATE
    Mot_Clef
SET
    parent = 'INDEX'
WHERE
    mot = 'LITTERATURE';

UPDATE
    Mot_Clef
SET
    parent = 'INDEX'
WHERE
    mot = 'SCIENCES';

UPDATE
    Mot_Clef
SET
    parent = 'INDEX'
WHERE
    mot = 'HISTOIRE';

UPDATE
    Mot_Clef
SET
    parent = 'LITTERATURE'
WHERE
    mot = 'ROMAN';

UPDATE
    Mot_Clef
SET
    parent = 'LITTERATURE'
WHERE
    mot = 'POESIE';

UPDATE
    Mot_Clef
SET
    parent = 'LITTERATURE'
WHERE
    mot = 'ESSAI';

UPDATE
    Mot_Clef
SET
    parent = 'LITTERATURE'
WHERE
    mot = 'NOUVELLE';

UPDATE
    Mot_Clef
SET
    parent = 'SCIENCES'
WHERE
    mot = 'MEDECINE';

UPDATE
    Mot_Clef
SET
    parent = 'SCIENCES'
WHERE
    mot = 'INFORMATIQUE';

UPDATE
    Mot_Clef
SET
    parent = 'INFORMATIQUE'
WHERE
    mot = 'BASES DE DONNEES';

UPDATE
    Mot_Clef
SET
    parent = 'BASES DE DONNEES'
WHERE
    mot = 'SQL';

/* 
Il faut au préalable insérer un tuple avec
INSERT INTO MOT_CLEF VALUES ('INDEX', NULL);

1 ligne modifiée.
 */
prompt --- Q8 - Creation des dates de naissances et du type d'abonné
UPDATE
    Abonne
SET
    date_nai = (
        SELECT
            SYSDATE
        FROM
            DUAL
    ) - (age * 365)
WHERE
    age IS NOT NULL;

UPDATE
    Abonne
SET
    type_ab = (
        CASE
            WHEN age >= 18 THEN 'ADULTE'
            ELSE 'ENFANT'
        END
    )
WHERE
    age IS NOT NULL;

/*
11 lignes mises a jour.


7 lignes mises a jour.
 */
prompt --- Q9 - Insertion d'Auteurs 
INSERT INTO
    Auteur
VALUES
    (101, 'DUMAS', 'ALEXANDRE', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (102, 'SARTRE', 'JEAN-PAUL', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (103, 'GENEY', 'JEAN', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (104, 'VALLES', 'JULES', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (105, 'VILLON', 'FRANCOIS', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (106, 'ECO', 'UMBERTO', 'ITALIENNE');

INSERT INTO
    Auteur
VALUES
    (107, 'GARY', 'ROMAIN', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (108, 'ROCHEFORT', 'CHRISTIANE', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (109, 'STEINBECK', 'JOHN', 'AMERICAIN');

INSERT INTO
    Auteur
VALUES
    (110, 'HOFSTADTER', 'DOUGLAS', 'ALLEMAND');

INSERT INTO
    Auteur
VALUES
    (111, 'BOUZEGHOUB', 'MOKRANE', 'TUNISIENNE');

INSERT INTO
    Auteur
VALUES
    (112, 'GARDARIN', 'GEORGES', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (113, 'VALDURIEZ', 'PATRICK', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (114, 'ULLMAN', 'JEFFREY', 'AMERICAINE');

INSERT INTO
    Auteur
VALUES
    (115, 'DELOBEL', 'CLAUDE', 'FRANCAISE');

INSERT INTO
    Auteur
VALUES
    (116, 'DATE', 'JC', 'AMERICAINE');

INSERT INTO
    Auteur
VALUES
    (117, 'GELENBE', 'EROL', 'INDIENNE');

INSERT INTO
    Auteur
VALUES
    (118, 'FLORY', 'ANDRE', 'FRANCAISE');

INSERT INTO
    AuteurLivre
VALUES
    (102, '1_104_1050_2');

INSERT INTO
    AuteurLivre
VALUES
    (103, '0_15_270500_3');

INSERT INTO
    AuteurLivre
VALUES
    (104, '0_85_4107_3');

INSERT INTO
    AuteurLivre
VALUES
    (105, '0_112_3785_5');

INSERT INTO
    AuteurLivre
VALUES
    (116, '0_201_14439_5');

INSERT INTO
    AuteurLivre
VALUES
    (112, '0_12_27550_2');

INSERT INTO
    AuteurLivre
VALUES
    (117, '0_12_27550_2');

INSERT INTO
    AuteurLivre
VALUES
    (111, '0_8_7707_2');

INSERT INTO
    AuteurLivre
VALUES
    (118, '0_8_7707_2');

INSERT INTO
    AuteurLivre
VALUES
    (106, '1_22_1721_3');

INSERT INTO
    AuteurLivre
VALUES
    (107, '3_505_13700_5');

INSERT INTO
    AuteurLivre
VALUES
    (108, '0_18_47892_2');

INSERT INTO
    AuteurLivre
VALUES
    (109, '9_782070_36');

INSERT INTO
    AuteurLivre
VALUES
    (110, '2_7296_0040');

INSERT INTO
    AuteurLivre
VALUES
    (111, '0_26_28079_6');

INSERT INTO
    AuteurLivre
VALUES
    (112, '0_26_28079_6');

INSERT INTO
    AuteurLivre
VALUES
    (113, '0_26_28079_6');

/*
1 ligne créee.

autant de fois qu'il y a d'insertion
 */
prompt --- Q10 - Quels sont les différents propriétaires des tables ? 
DESC All_Tables;

DESC User_Tables;

DESC DBA_Tables;

DESC User_Constraints;

DESC User_Tab_Columns;

DESC User_Indexes;

SELECT DISTINCT
    owner
FROM
    All_Tables;

/*
La dernière valeur correspond à votre identifiant UNIX.
----------------------------------------------------------------------------------------------------
----------------------------
SYSTEM
AUDSYS
XDB
SYS
P00000008868
 */
prompt --- Q11 - Quels sont les noms de tous les attributs de la relation ABONNE ?
SELECT
    column_name
FROM
    User_Tab_Columns
WHERE
    table_name = 'ABONNE';

/*
COLUMN_NAME
------------------------------
NUM_AB
NOM
PRENOM
VILLE
AGE
TARIF
REDUC
DATE_NAI
TYPE_AB
CAT_AB

10 lignes selectionnees.
 */
prompt --- Q12 - Quel est le nombre d'attributs définis dans la BD BIBLIO ?
SELECT
    count(DISTINCT column_name)
FROM
    User_Tab_Columns
WHERE
    table_name IN (
        SELECT
            table_name
        FROM
            All_Tables
        WHERE
            owner = 'E20220006046'
    );

prompt --- Q13 - Quelles sont toutes les contraintes d'intégrité définies sur les relations de données de la base BIBLIO ?
SELECT DISTINCT
    constraint_name
FROM
    User_Constraints
WHERE
    owner = 'E20220006046'
    AND constraint_name LIKE 'PK_%';

prompt --- Q14 - Création de la relation TESTINDEX
CREATE TABLE
    TestIndex (nom VARCHAR(10));

/*
Table créée.
 */
prompt --- Q14 - Création de l'index
CREATE UNIQUE INDEX Cle_TestIndex ON TestIndex (nom);

/*
Index créé.
 */
prompt --- Q14 - Essai de deux insertions
INSERT INTO
    TestIndex
VALUES
    ('toto');

INSERT INTO
    TestIndex
VALUES
    ('toto');

/*
Les valeurs peuvent changer :
1 ligne creee.

.......

 *
ERREUR a la ligne 1 :
ORA-00001: violation de contrainte unique (P00000008868.CLE_TESTINDEX)
 */
prompt --- Q14 - Affichage de l'index dans les tables systèmes
SELECT
    *
FROM
    User_Indexes
WHERE
    index_name = 'CLE_TESTINDEX';

/*
INDEX_NAME		INDEX_TYPE
-----------------------------------
CLE_TESTINDEX		NORMAL
 */
prompt --- Q15 - Existe-t-il, dans la base BIBLIO, une relation pour laquelle aucun index n’a été spécifié ?
SELECT
    table_name
FROM
    All_Tables
WHERE
    owner = 'E20220006046'
    AND table_name NOT IN (
        SELECT
            table_name
        FROM
            User_Indexes
    );

/*
aucune ligne selectionnee
 */
prompt --- Q16 - Création vue ABONNE_MONTP
CREATE VIEW
    Abonne_Montp AS
SELECT
    num_ab,
    nom,
    prenom
FROM
    Abonne
WHERE
    ville = 'Montpellier';

/*
Vue creee.
 */
prompt --- Q16 - Insertion d'un tuple dans la vue
INSERT INTO
    Abonne_Montp
VALUES
    (1, 'TOTO', 'TITI');

/*
1 ligne creee.
 */
prompt --- Q16 - Visualisation d'abonne
SELECT
    *
FROM
    Abonne;

/*
NUM_AB NOM			PRENOM		     VILLE		  AGE
---------- -------------------- -------------------- ------------- ----------
TARIF	REDUC DATE_NAI	 TYPE_A CAT_AB
---------- ---------- ---------- ------ ------------
.....
950001	  FAURE

12 lignes sélectionnées.
 */
prompt --- Q17 : Vérifier que la vue a bien été créée dans les tables systèmes.
SELECT DISTINCT
    view_name
FROM
    User_Views
WHERE
    view_name = 'ABONNE_MONTP';

/*
VIEW_NAME
--------------------------------------------------------------------------------
ABONNE_MONTP
 */
prompt --- Q18 - vue ETAT_ETAT EXEMPLAIRE
CREATE VIEW
    Etat_Exemplaire AS
SELECT
    *
FROM
    Exemplaire
WHERE
    etat IN ('BON', 'PERDU', 'ABIME', 'EN_REPARATION')
WITH
    CHECK OPTION;

/*
Vue créée.
 */
prompt --- Q18 - tentative d'insertion dans la vue d'un tuple erroné
INSERT INTO
    Etat_Exemplaire
VALUES
    (
        1,
        '10-04-2022',
        55,
        'EMPRUNTABLE',
        'PAS OUF',
        '0_18_47892_2'
    );

/*
INSERT INTO ...
 *
ERREUR a la ligne 1 :
ORA-01402: vue WITH CHECK OPTION - violation de clause WHERE
 */
prompt --- Q19 - Creation de la vue EMP
CREATE VIEW
    Emp AS
SELECT
    *
FROM
    Emprunt
WHERE
    num_ab IN (
        SELECT
            num_ab
        FROM
            Abonne
    )
    AND num_ex IN (
        SELECT
            num_ex
        FROM
            Exemplaire
    )
    AND nb_relance IN (1, 2, 3)
    AND (num_ab, num_ex, d_emprunt) NOT IN (
        SELECT
            num_ab,
            num_ex,
            d_emprunt
        FROM
            Emprunt
    )
WITH
    CHECK OPTION;

/*
Vue créée.
 */
prompt --- Q20 - Test d'une inserion invalide
INSERT INTO
    Emp
VALUES
    (
        911007,
        5004,
        '20-07-2020',
        '10-08-2020',
        '10-08-2020',
        NULL
    );

/*
INSERT INTO ...
 *
ERREUR a la ligne 1 :
ORA-01402: vue WITH CHECK OPTION - violation de clause WHERE
 */