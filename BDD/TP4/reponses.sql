-- Question 1
DESC All_Tables;

SELECT
    table_name
FROM
    All_Tables;

SELECT
    *
FROM
    All_Tables;

-- Question 2
SELECT
    count(*)
FROM
    User_Tab_Columns
WHERE
    table_name = 'ABONNE';

-- Question 3
DESC User_Constraints;

SELECT
    constraint_name,
    table_name,
    status
FROM
    User_Constraints
WHERE
    owner = 'E20220006046'
    AND generated = 'USER NAME'
    AND constraint_name NOT LIKE 'BIN$%';

-- Question 4
SELECT
    *
FROM
    User_Constraints
WHERE
    owner = 'E20220006046'
    AND generated = 'USER NAME'
    AND constraint_name LIKE 'PK_%';

-- Question 5
DESC User_Tab_Columns;

SELECT
    column_name,
    data_type
FROM
    User_Tab_Columns
WHERE
    table_name = '&table_name';

-- ###########################################
-- Partie 2
-- ###########################################
-- Question 2.1
-- Colin
GRANT
SELECT
    ON ABONNE TO E20200004725;

GRANT ALTER ON LIVRE TO E20200004725;

DESC User_Tab_Privs_Made;

SELECT
    grantee,
    table_name,
    privilege
FROM
    User_Tab_Privs_Made;

-- Alexis 
SELECT
    *
FROM
    E20220006046.ABONNE;

ALTER TABLE
    E20220006046.LIVRE
MODIFY
    CATEGORIE VARCHAR(32);

CREATE VIEW
    Beziers_Clients AS
SELECT
    NUM_AB,
    NOM
FROM
    E20220006046.ABONNE
WHERE
    VILLE = 'BEZIERS';

-- Question 2.2
-- Colin
REVOKE
SELECT
    ON ABONNE
FROM
    E20200004725;

REVOKE ALTER ON LIVRE
FROM
    E20200004725;

SELECT
    grantee,
    table_name,
    privilege
FROM
    User_Tab_Privs_Made;

-- ###########################################
-- Partie 3
-- ###########################################
GRANT ALL ON ABONNE TO E20200004725;

GRANT ALL ON LIVRE TO E20200004725;

SELECT
    nom,
    prenom
FROM
    Abonne;

UPDATE
    Abonne
SET
    nom = 'DUPOND'
WHERE
    nom = 'DUPONT';

UPDATE
    Abonne
SET
    prenom = 'POPOL'
WHERE
    nom = 'LUCAS'
    AND prenom = 'PAUL';

UPDATE
    Livre
SET
    siecle = 20
WHERE
    titre = 'POESIES COMPLETES';