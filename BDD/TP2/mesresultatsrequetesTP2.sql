/*
USAGE :
Penser à remplir les champs Numéro carte, nom, prénom, date.

Utiliser ce fichier pour répondre aux différentes questions. 
Pour chaque requete vous avez le résultat que vous devez obtenir avec les données de la base BIBLIOTHEQUE.

Les requêtes sont affichées

Si vous ne voulez pas afficher à chaque fois vos requêtes vous pouvez les mettre en commentaire. 
Attention sous ORACLE pour les marques des commentaires (le slash et l'étoile) doivent être seuls sur une ligne.

 */
/*
Numéro de carte étudiant : 
Nom : 
Prénom : 
Date : 
 */
/*
Mise en page - Ne pas toucher
 */
CLEAR SCREEN
SET
    PAGESIZE 30 COLUMN COLUMN_NAME FORMAT A30
SET
    LINESIZE 300 prompt --- Q1 : Quels sont les noms et prénoms des abonnés domiciliés à Montpellier ?
SELECT
    nom,
    prenom
FROM
    Abonne
WHERE
    ville = 'MONTPELLIER';

/*
NOM		PRENOM
--------------- --------------------
LEVEQUE 	PIERRE
DUPONT		MARIE
RENARD		ALBERT
DUPONT		ANTOINE
DUPONT		SYLVIE
DUPONT		JEAN
MEUNIER 	LUC
LUCAS		PAUL
REVEST		ANNIE
ANTON		JEANNE

10 lignes selectionnees.
 */
prompt ---- Q2 : Donner toutes les informations sur les exemplaires dont le code de prêt est : « EMPRUNTABLE ».
SELECT
    *
FROM
    Exemplaire
WHERE
    code_pret = 'EMPRUNTABLE';

/*
NUMERO DATE_ACHAT	    PRIX CODE_PRET	      ETAT	      ISBN
---------- ---------- ---------- -------------------- --------------- ---------------
1010 10-04-2015	      55 EMPRUNTABLE	      BON	      0_18_47892_2
1011 10-04-2015	      55 EMPRUNTABLE	      BON	      0_18_47892_2
1012 20-05-2015	     112 EMPRUNTABLE	      BON	      3_505_13700_5
2909 30-03-2017	      35 EMPRUNTABLE	      BON	      3_505_13700_5
2673 15-03-2018	      42 EMPRUNTABLE	      ABIME	      3_505_13700_5
2711 20-06-2014	      27 EMPRUNTABLE	      BON	      0_8_7707_2
3016 15-09-2017	      42 EMPRUNTABLE	      BON	      0_201_14439_5
3702 20-02-2019	      21 EMPRUNTABLE	      BON	      1_22_1721_3
4111 03-01-2021	      48 EMPRUNTABLE	      BON	      1_22_1721_3
4203 29-11-2019	      35 EMPRUNTABLE	      BON	      1_104_1050_2
4204 29-11-2019	      35 EMPRUNTABLE	      ABIME	      1_104_1050_2
5003 10-06-2020	      39 EMPRUNTABLE	      BON	      1_104_1050_2
5004 10-06-2020	      41 EMPRUNTABLE	      BON	      0_15_270500_3
5005 10-06-2020	      41 EMPRUNTABLE	      BON	      0_15_270500_3
5104 20-12-2017	      47 EMPRUNTABLE	      BON	      0_12_27550_2
6006 15-02-2021	      33 EMPRUNTABLE	      BON	      0_85_4107_3
6007 15-02-2021	      33 EMPRUNTABLE	      BON	      0_85_4107_3
5202 18-10-2020	      40 EMPRUNTABLE	      BON	      0_18_47892_2
7001 01-09-2016	      42 EMPRUNTABLE	      BON	      2_7296_0040
1109 30-05-2016	      17 EMPRUNTABLE	      BON	      9_782070_36

20 lignes selectionnees.
 */
prompt ---- Q3 : Donner la liste des ouvrages (ISBN, TITRE, CATEGORIE), dont le titre inclut le mot ‘ROSE’, triée par ordre décroissant de numéro.
SELECT
    isbn,
    titre,
    categorie
FROM
    Livre
WHERE
    titre LIKE '%ROSE%'
ORDER BY
    isbn DESC;

/*
ISBN		TITRE						   CATEGORIE
--------------- -------------------------------------------------- ----------------
1_22_1721_3	LE NOM DE LA ROSE				   ROMAN
0_18_47892_2	UNE ROSE POUR MORRISSON 			   ROMAN
0_15_270500_3	LE MIRACLE DE LA ROSE				   ROMAN
 */
prompt ---- Q4 : Donner la liste des livres (leur titre et catégorie) de toutes les catégories sauf Médecine, Sciences et Loisirs. Cette liste sera donnée triée par ordre alphabétique selon la catégorie.
SELECT
    titre,
    categorie
FROM
    Livre
WHERE
    categorie NOT IN ('MEDECINE', 'SCIENCES', 'LOISIRS')
ORDER BY
    categorie;

/*
TITRE						   CATEGORIE
-------------------------------------------------- ----------------
GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE	   HISTOIRE
LE MUR						   NOUVELLE
POESIES COMPLETES				   POEME
UNE ROSE POUR MORRISSON 			   ROMAN
LA PERLE					   ROMAN
LE GRAND VESTIAIRE				   ROMAN
L ENFANT					   ROMAN
LE MIRACLE DE LA ROSE				   ROMAN
LE NOM DE LA ROSE				   ROMAN

9 lignes selectionnees.
 */
prompt ---- Q5 : Donner toutes les informations sur les emprunts pour lesquels la date de retour effective (attribut D_RET_REEL) n'est pas renseignée dans la base de données.
SELECT
    *
FROM
    Emprunt
WHERE
    d_ret_reel IS NULL;

/*
NUM_AB     NUM_EX D_EMPRUNT  D_RETOUR   D_RET_REEL NB_RELANCE
---------- ---------- ---------- ---------- ---------- ----------
911007	 4204 19-01-2021 10-02-2021			1
902075	 2673 15-02-2021 28-02-2021
902075	 1010 05-01-2021 25-01-2021			1
921102	 6006 20-12-2020 10-01-2021			2

 */
prompt ---- Q6 : Donner, pour l'abonné JEAN DUPONT, la liste des exemplaires empruntés (leur numéro et la date d'emprunt), par ordre croissant de date d'emprunt.
SELECT
    e.num_ex,
    e.d_emprunt
FROM
    Emprunt e
    JOIN Abonne a ON e.num_ab = a.num_ab
WHERE
    a.nom = 'DUPONT'
    AND a.prenom = 'JEAN'
ORDER BY
    d_emprunt;

/*
NUM_EX D_EMPRUNT
---------- ----------
5003 10-03-2020
5005 30-03-2020
1012 30-03-2020
5103 17-06-2020
1109 15-09-2020
6007 22-12-2020

6 lignes selectionnees.
 */
prompt ------ Q7 : Donner la liste des exemplaires empruntés (leur numéro, code prêt et état) du livre de titre « LE MUR».
SELECT
    ex.numero,
    ex.code_pret,
    ex.etat
FROM
    Exemplaire ex
    JOIN Emprunt em ON ex.numero = em.num_ex
    JOIN Livre l ON ex.isbn = l.isbn
WHERE
    em.d_ret_reel IS NULL
    AND l.titre = 'LE MUR';

/*
NUMERO CODE_PRET		ETAT
---------- -------------------- ---------------
5003 EMPRUNTABLE		BON
4203 EMPRUNTABLE		BON
4204 EMPRUNTABLE		ABIME
 */
prompt ---- Q8 : Quels sont les exemplaires (numéro) reproduisant le même livre que l'exemplaire de numéro 4112 et dont l'état est : « BON » ?
SELECT
    numero
FROM
    Exemplaire e
    JOIN Livre l ON e.isbn = l.isbn
WHERE
    etat = 'BON'
    AND e.numero != 4112
    AND l.isbn = (
        SELECT
            Livre.isbn
        FROM
            Livre
            JOIN Exemplaire ON Livre.isbn = Exemplaire.isbn
        WHERE
            Exemplaire.numero = 4112
    );

/*
NUMERO
----------
3702
3703
4111
 */
prompt ---- Q9 : Existe-t-il une catégorie pour laquelle aucun livre n'a été emprunté ?
SELECT
    categorie
FROM
    (
        SELECT
            L.categorie,
            count(Em.num_ex) AS nb_empruntes
        FROM
            Livre L
            LEFT JOIN Exemplaire Ex ON L.isbn = Ex.isbn
            LEFT JOIN Emprunt Em ON Ex.numero = Em.num_ex
        GROUP BY
            L.categorie
    )
WHERE
    nb_empruntes = 0;

/*
CATEGORIE
----------
POEME
 */
prompt ---- Q9 : Existe-t-il une catégorie pour laquelle aucun livre n'a été emprunté ? (seconde version)
/*
CATEGORIE
----------
POEME
 */
prompt ---- Q10 : Quel est le nombre d'emprunt en cours de l'abonné Renard Albert ? 
SELECT
    count(*) AS emprunts_albert_renard
FROM
    Emprunt Em
    JOIN Abonne A ON Em.num_ab = A.num_ab
WHERE
    A.nom = 'RENARD'
    AND A.prenom = 'ALBERT'
    AND Em.d_ret_reel IS NULL;

/*
EMPRUNTS D ALBERT RENARD
------------------------
2
 */
prompt ---- Q11 : Quelle est la catégorie de livres pour laquelle l'exemplaire le plus cher a été acheté ? 
SELECT
    L.categorie
FROM
    Livre L
    JOIN Exemplaire Ex ON L.isbn = Ex.isbn
WHERE
    Ex.prix = (
        SELECT
            max(prix)
        FROM
            Exemplaire
    );

/*
CATEGORIE
----------------
ROMAN
 */
prompt ---- Q12 : Existe-t-il des exemplaires dans l'état « Abimé » et qui sont actuellement empruntés ? Si oui, donner leur numéro.
SELECT
    Ex.numero
FROM
    Exemplaire Ex
    JOIN Emprunt Em ON Ex.numero = Em.num_ex
WHERE
    etat = 'ABIME'
    AND Em.d_ret_reel IS NULL;

/*
NUMERO
----------
2673
4204
 */
prompt -- Q13 : Existe-t-il des mots clefs ne caractérisant aucun livre ?
SELECT
    mot
FROM
    Mot_Clef
WHERE
    mot NOT IN (
        SELECT
            mot
        FROM
            Caracterise
    );

/*
MOT
--------------------
MEDECINE
NOUVELLE
 */
prompt --- Q14 : Donner le nombre d'emprunts effectués par chacun des abonnés (numéro, nom) pour chacune des catégories de livres proposées.
SELECT
    A.num_ab,
    A.nom,
    L.categorie,
    count(*)
FROM
    Abonne A
    JOIN Emprunt Em ON A.num_ab = Em.num_ab
    JOIN Exemplaire Ex ON Em.num_ex = Ex.numero
    JOIN Livre L ON Ex.isbn = L.isbn
GROUP BY
    A.num_ab,
    A.nom,
    L.categorie;

/*
NUM_AB NOM		   CATEGORIE	      COUNT(*)
---------- --------------- ---------------- ----------
911023 DUPONT	   NOUVELLE		     1
921102 LUCAS	   NOUVELLE		     2
902043 DUPONT	   HISTOIRE		     2
902075 RENARD	   NOUVELLE		     1
922016 MEUNIER	   NOUVELLE		     1
932010 ANTON	   NOUVELLE		     1
901001 LEVEQUE	   NOUVELLE		     2
911007 MARTIN	   ROMAN		     3
911023 DUPONT	   SCIENCES		     1
921102 LUCAS	   ROMAN		     2
922143 REVEST	   NOUVELLE		     1
901001 LEVEQUE	   HISTOIRE		     2
911007 MARTIN	   NOUVELLE		     3
911021 DUPONT	   ROMAN		     1
911021 DUPONT	   NOUVELLE		     1
911023 DUPONT	   ROMAN		     4
902043 DUPONT	   NOUVELLE		     1
901001 LEVEQUE	   ROMAN		     1
902043 DUPONT	   SCIENCES		     2
902075 RENARD	   ROMAN		     2
911022 DUPONT	   NOUVELLE		     1

21 lignes selectionnees.
 */
prompt --- Q15 : Donner, pour chaque livre (numéro ISBN) ayant plus de deux exemplaires, le prix moyen d'achat des exemplaires.
SELECT
    L.isbn,
    L.titre,
    avg(Ex.prix) AS prix_moyen
FROM
    Livre L
    JOIN Exemplaire Ex ON L.isbn = Ex.isbn
GROUP BY
    L.isbn,
    L.titre
HAVING
    count(Ex.isbn) > 2;

/*
ISBN		TITRE						   PRIX MOYEN
--------------- -------------------------------------------------- ----------
0_18_47892_2	UNE ROSE POUR MORRISSON 				   50
9_782070_36	LA PERLE					   60,6666667
3_505_13700_5	LE GRAND VESTIAIRE					   63
1_104_1050_2	LE MUR						   36,3333333
1_22_1721_3	LE NOM DE LA ROSE					 34,5
2_7296_0040	GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE		   42

6 lignes selectionnees.
 */
prompt --- Q16 : Quel est l’abonné ayant effectué le plus d’emprunts ?
SELECT
    num_ab,
    nom
FROM
    Abonne
WHERE
    num_ab IN (
        SELECT
            num_ab
        FROM
            Emprunt
        GROUP BY
            num_ab
        HAVING
            count(*) = (
                SELECT
                    max(count(*))
                FROM
                    Emprunt
                GROUP BY
                    num_ab
            )
    );

/*
NUM_AB NOM
---------- ---------------
911023 DUPONT
911007 MARTIN
 */
prompt --- Q17 : Donnez, pour chaque livre (numéro ISBN et titre) emprunté au moins deux fois, son nombre d’exemplaires en catégorie "Exclu". 
SELECT
    L.isbn,
    L.titre,
    count(*) AS nombre_exemplaires
FROM
    LIVRE L
    JOIN Exemplaire Ex ON L.isbn = Ex.isbn
WHERE
    L.isbn IN (
        SELECT
            Ex.isbn
        FROM
            Exemplaire Ex
            JOIN Emprunt Em ON Ex.numero = Em.num_ex
        GROUP BY
            Ex.isbn
        HAVING
            count(*) >= 2
    )
    AND Ex.code_pret = 'EXCLU'
GROUP BY
    L.isbn,
    L.titre;

/*
ISBN		TITRE						   Nombre exemplaires
--------------- -------------------------------------------------- ------------------
2_7296_0040	GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE			    1

 */
prompt -- Q18 : Existe t'il des homonymes (NUM_AB, NOM) parmi les abonnés ? Attention dans le résultat il ne faut ne pas dupliquer l'information. Par exemple si deux abonnés s'appellent DUPOND le résultat à obtenir est : 
prompt -- 1 DUPOND
prompt -- 2 DUPOND
SELECT
    num_ab,
    nom
FROM
    Abonne
WHERE
    nom IN (
        SELECT
            nom
        FROM
            Abonne
        GROUP BY
            nom
        HAVING
            count(num_ab) >= 2
    );

/*
NUM_AB NOM
---------- ---------------
911021 DUPONT
911022 DUPONT
911023 DUPONT
902043 DUPONT
 */
prompt --- Q19 : Existe-t'il des catégories de livres empruntées par tous les abonnés ?
prompt --- Q19 Premiere possibilité
SELECT
    L.categorie
FROM
    Livre L
    JOIN Exemplaire Ex ON L.isbn = Ex.isbn
    JOIN Emprunt Em ON Ex.numero = Em.num_ex
GROUP BY
    L.categorie
HAVING
    count(distinct Em.num_ab) = (
        SELECT
            count(*)
        FROM
            Abonne
    );

/*
CATEGORIE
----------------
NOUVELLE
 */
prompt --- Q19 Deuxième possibilité
/*
VOTRE REPONSE ICI
 */
/*
CATEGORIE
----------------
NOUVELLE
 */
prompt -- Q20 : Quels sont les livres (numéro ISBN et titre) dont tous les exemplaires valent plus de 30 euros ?
prompt -- Q20 - Première possibilité : 
SELECT
    isbn,
    titre
FROM
    Livre
WHERE
    isbn IN (
        SELECT
            Ex1.isbn
        FROM
            Exemplaire Ex1
        GROUP BY
            Ex1.isbn
        HAVING
            count(*) = (
                SELECT
                    count(*)
                FROM
                    Exemplaire Ex2
                WHERE
                    prix > 30
                    AND Ex2.isbn = Ex1.isbn
                GROUP BY
                    isbn
            )
    )
ORDER BY
    isbn;

/*
ISBN		TITRE
--------------- --------------------------------------------------
0_12_27550_2	NEW APPLICATIONS OF DATABASES
0_15_270500_3	LE MIRACLE DE LA ROSE
0_18_47892_2	UNE ROSE POUR MORRISSON
0_201_14439_5	AN INTRODUCTION TO DATABASE SYSTEMS
0_85_4107_3	L ENFANT
1_104_1050_2	LE MUR
2_7296_0040	GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE
3_505_13700_5	LE GRAND VESTIAIRE

8 lignes selectionnees.
 */
prompt -- Q20 - Deuxième possibilité : 
/*
VOTRE REPONSE ICI
 */
/*
ISBN		TITRE
--------------- --------------------------------------------------
0_12_27550_2	NEW APPLICATIONS OF DATABASES
0_15_270500_3	LE MIRACLE DE LA ROSE
0_18_47892_2	UNE ROSE POUR MORRISSON
0_201_14439_5	AN INTRODUCTION TO DATABASE SYSTEMS
0_85_4107_3	L ENFANT
1_104_1050_2	LE MUR
2_7296_0040	GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE
3_505_13700_5	LE GRAND VESTIAIRE

8 lignes selectionnees.
 */
prompt -- Q21 : Quels sont les livres (numéro ISBN et titre) dont tous les exemplaires valant plus de 30 euros ont été au moins trois fois empruntés ?
SELECT
    isbn,
    titre
FROM
    Livre L
WHERE
    (
        SELECT
            count(*)
        FROM
            Exemplaire Ex
        WHERE
            prix > 30
            AND Ex.isbn = L.isbn
            AND (
                SELECT
                    count(*)
                FROM
                    Emprunt
                WHERE
                    num_ex = Ex.numero
                GROUP BY
                    num_ex
            ) >= 3
    ) = (
        SELECT
            count(*)
        FROM
            Exemplaire Ex
        WHERE
            prix > 30
            AND Ex.isbn = L.isbn
    )
GROUP BY
    isbn,
    titre;

/*
ISBN		TITRE
--------------- --------------------------------------------------
0_112_3785_5	POESIES COMPLETES
0_8_7707_2	BASES DE DONNEES RELATIONNELLES
0_26_28079_6	FUNDAMENTALS OF DATABASE SYSTEMS
 */
prompt --- Q22 Faire 5 requêtes différentes affichant les mots clés des ouvrages pour les numéros ISBN 0_15_270500_3, 0_201_14439_5, 0_12_27550_2, 0_26_28079_6 et 0_8_7707_2.
prompt --- Q22 - requête pour 0_15_270500_3
SELECT
    mot
FROM
    Caracterise
WHERE
    isbn = '0_15_270500_3';

/*
MOT
--------------------
LITTERATURE
ROMAN
ESSAI
 */
prompt --- Q22 - requête pour 0_201_14439_5
SELECT
    mot
FROM
    Caracterise
WHERE
    isbn = '0_201_14439_5';

/*
MOT
--------------------
SCIENCES
INFORMATIQUE
BASES DE DONNEES
 */
prompt --- Q22 - requête pour 0_12_27550_2
SELECT
    mot
FROM
    Caracterise
WHERE
    isbn = '0_12_27550_2';

/*
MOT
--------------------
SCIENCES
SQL
BASES DE DONNEES
 */
prompt --- Q22 - requête pour 0_26_28079_6
SELECT
    mot
FROM
    Caracterise
WHERE
    isbn = '0_26_28079_6';

/*
MOT
--------------------
SCIENCES
INFORMATIQUE
BASES DE DONNEES
SQL
 */
prompt --- Q22 - requête pour 0_8_7707_2
SELECT
    mot
FROM
    Caracterise
WHERE
    isbn = '0_8_7707_2';

/*
MOT
--------------------
SCIENCES
SQL
BASES DE DONNEES
 */
prompt --- Q23 : Quels sont les livres caractérisés par exactement les mêmes mots clefs que l'ouvrage de numéro ISBN 0-8-7707-2 ? Penser à vérifier le résultat par rapport à la requête précédente.
SELECT
    isbn
FROM
    Caracterise Ca1
GROUP BY
    isbn
HAVING
    count(mot) = (
        SELECT
            count(*)
        FROM
            Caracterise Ca2
            JOIN Caracterise Ca3 ON Ca2.mot = Ca3.mot
        WHERE
            Ca2.isbn = '0_8_7707_2'
            AND Ca3.isbn != '0_8_7707_2'
            AND Ca3.isbn = Ca1.isbn
        GROUP BY
            Ca3.isbn
    );

-- Potentiellement : rajouter vérification count(*) = count(mots) pour Ca2.isbn
/*
ISBN
---------------
0_12_27550_2
 */
prompt -- Q24 - Donner en une seule requête le nombre d'abonnés, de livres et d'exemplaires. 
SELECT
    nb_abonnes,
    nb_livres,
    nb_exemplaires
FROM
    (
        SELECT
            count(*) AS nb_abonnes
        FROM
            Abonne
    ),
    (
        SELECT
            count(*) AS nb_livres
        FROM
            Livre
    ),
    (
        SELECT
            count(*) AS nb_exemplaires
        FROM
            Exemplaire
    );

/*
NBAB   NBLIVRES NBEXEMPLAIRES
---------- ---------- -------------
11	   13		 29
 */