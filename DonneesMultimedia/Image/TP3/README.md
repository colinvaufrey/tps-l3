# TP3 Donnée Multimédia

## Partie 1 : Création d'une image

Image choisie : </br>
![Dzongh bhoutanais](https://pix5.agoda.net/geo/country/291/3_291_bhutan_02.jpg?s=1920x)

Image PGM : </br>
![Image PGM](./images/jpg/bhutan256.jpg)

Commande GNUPlot utilisée pour générer les histogrammes : </br>
```bash
plot <file_path> with boxes title 'Occurences par couleur'
```

Histogramme de l'image : </br>
![Histogramme de l'image](./images/histogrammes/base_hist.png)

Commande GNUPlot utilisée pour générer les histogrammes des profils : </br>
```bash
plot <file_path> with boxes title 'Couleur par colonne' ls 2
```

Profil de la ligne 50 : </br>
![Profil de la ligne 50](./images/histogrammes/base_l50.png)

## Partie 2 : Inverse

Image d'entrée : </br>
![Image d'entrée](./images/jpg/bhutan256.jpg)

Image inversée : </br>
![Image inversée](./images/jpg/inverted.jpg)

Profil de la ligne 50 (entrée) : </br>
![Profil de la ligne 50 (entrée)](./images/histogrammes/base_l50.png)

Profil de la ligne 50 (inversée) : </br>
![Profil de la ligne 50 (inversée)](./images/histogrammes/inverse_l50.png)

Observation : On observe que chaque pic de l'histogramme de l'image d'entrée est associé à un creu de l'histogramme de l'image inversée et inversement.

## Partie 3 : Flou (sans diagonale)

Image d'entrée : </br>
![Image d'entrée](./images/jpg/bhutan256.jpg)

Image floutée : </br>
![Image floutée](./images/jpg/blurred1.jpg)

Profil de la ligne 50 (entrée) : </br>
![Profil de la ligne 50 (entrée)](./images/histogrammes/base_l50.png)

Profil de la ligne 50 (floutée) : </br>
![Profil de la ligne 50 (floutée)](./images/histogrammes/blurred1_l50.png)

Observation : On observe que l'image floutée est plus lisse que l'image d'entrée. On peut aussi voir que l'histogramme de l'image floutée est légèrement plus plat que celui de l'image d'entrée.

## Partie 4 : Flou (avec diagonale)

Image d'entrée : </br>
![Image d'entrée](./images/jpg/bhutan256.jpg)

Image floutée (1 fois) : </br>
![Image floutée (1 fois)](./images/jpg/blurred2.jpg)

Image floutée (2 fois) : </br>
![Image floutée (2 fois)](./images/jpg/blurred2-2.jpg)

Image floutée (5 fois) : </br>
![Image floutée (5 fois)](./images/jpg/blurred2-5.jpg)

Profil de la ligne 50 (entrée) : </br>
![Profil de la ligne 50 (entrée)](./images/histogrammes/base_l50.png)

Profil de la ligne 50 (floutée 1 fois) : </br>
![Profil de la ligne 50 (floutée 1 fois)](./images/histogrammes/blurred2_l50.png)

Profil de la ligne 50 (floutée 2 fois) : </br>
![Profil de la ligne 50 (floutée 2 fois)](./images/histogrammes/blurred2-2_l50.png)

Profil de la ligne 50 (floutée 5 fois) : </br>
![Profil de la ligne 50 (floutée 5 fois)](./images/histogrammes/blurred2-5_l50.png)

Histogramme de l'image d'entrée : </br>
![Histogramme de l'image d'entrée](./images/histogrammes/base_hist.png)

Histogramme de l'image floutée (1 fois) : </br>
![Histogramme de l'image floutée (1 fois)](./images/histogrammes/blurred2_hist.png)

Histogramme de l'image floutée (2 fois) : </br>
![Histogramme de l'image floutée (2 fois)](./images/histogrammes/blurred2-2_hist.png)

Histogramme de l'image floutée (5 fois) : </br>
![Histogramme de l'image floutée (5 fois)](./images/histogrammes/blurred2-5_hist.png)

Observation : Même observation que pour la partie 3. On observe une diminution de la variance de l'histogramme de l'image floutée.

## Partie 5 : Flou en couleur

Image d'entrée : </br>
![Image d'entrée](./images/jpg/bhutan256_rgb.jpg)

Image floutée : </br>
![Image floutée](./images/jpg/blurred_rgb.jpg)

Commande GNUPlot utilisée pour générer les histogrammes :
```bash
plot <file_path> using 1:2 with lines title 'Occurences de rouge' lt rgb 'red' lw 2,\
'' using 1:3 with lines title 'Occurences de vert' lt rgb 'green' lw 2,\
'' using 1:4 with lines title 'Occurences de bleu' lt rgb 'blue' lw 2
```

Histogramme de l'image d'entrée : </br>
![Histogramme de l'image d'entrée](./images/histogrammes/base_rgb_hist.png)

Histogramme de l'image floutée : </br>
![Histogramme de l'image floutée](./images/histogrammes/blurred_rgb_hist.png)
