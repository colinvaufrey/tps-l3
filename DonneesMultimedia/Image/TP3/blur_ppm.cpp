#include "image_ppm.h"

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomImgEcrite[250];
	int nH, nW, nTaille, nR, nG, nB;

	if (argc != 3) {
		printf("Usage: ImageIn.ppm ImageOut.ppm");
		exit(1);
	}

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille * 3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille * 3);

    for (int i = 0; i < nH; i++) {
        for (int j = 0; j < nW; j++) {
            // On parcourt le voisinage
            int avgR = ImgIn[3 * (i * nW + j)];
            int avgG = ImgIn[3 * (i * nW + j) + 1];
            int avgB = ImgIn[3 * (i * nW + j) + 2];
            int count = 1;

            for (int k = -1; k <= 1; k++) {
                for (int l = -1; l <= 1; l++) {
                    if (i + k >= 0 && i + k < nH && j + l >= 0 && j + l < nW) {
                        avgR += ImgIn[3 * ((i + k) * nW + j + l)];
                        avgG += ImgIn[3 * ((i + k) * nW + j + l) + 1];
                        avgB += ImgIn[3 * ((i + k) * nW + j + l) + 2];
                        count++;
                    }
                }
            }

            ImgOut[3 * (i * nW + j)] = avgR / count;
            ImgOut[3 * (i * nW + j) + 1] = avgG / count;
            ImgOut[3 * (i * nW + j) + 2] = avgB / count;
        }
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    return 1;
}