#include "pgm_processor.h"
#include <iostream>

using namespace std;

char *imagePathIn, *imagePathOut;

int main(int argc, char *argv[]) {
	if (argc != 3) {
		std::cout << "Usage: " << argv[0] << " <image_in.pgm> <image_out.pgm>"
				  << std::endl;
		return 1;
	}

	imagePathIn = argv[1];
	imagePathOut = argv[2];

	PGMProcessor pgmProcessor(imagePathIn);

	cout << "Image read successfully" << endl;
	cout << "Image format: " << pgmProcessor.getImageWidth() << " × "
		 << pgmProcessor.getImageHeight() << endl;

	pgmProcessor.write(imagePathOut);

	int chosenOption = 0;
	int threshold = 0;

	do {
		cout << "Choose an option:" << endl;
		cout << " 1. Erode image" << endl;
		cout << " 2. Dilate image" << endl;
		cout << endl;
		cout << " 3. Open image" << endl;
		cout << " 4. Close image" << endl;
		cout << endl;
		cout << " 5. Threshold image" << endl;
		cout << " 6. Differentiate image" << endl;
		cout << endl;
		cout << " 7. Invert image" << endl;
		cout << endl;
		cout << " 8. Blur image (no diagonals)" << endl;
		cout << " 9. Blur image (with diagonals)" << endl;
		cout << endl;
		cout << "10. Save image as" << endl;
		cout << "11. Save image histogram as" << endl;
		cout << "12. Save image line or column profile as" << endl;
		cout << endl;
		cout << " 0. Exit" << endl;
		cout << endl;
		cout << "Option: ";

		cin >> chosenOption;

		cout << endl;

		switch (chosenOption) {
		case 1:
			cout << "Eroding image..." << endl;
			pgmProcessor.erode();
			cout << "Image eroded" << endl;
			break;
		case 2:
			cout << "Dilating image..." << endl;
			pgmProcessor.dilate();
			cout << "Image dilated" << endl;
			break;
		case 3:
			cout << "Opening image..." << endl;
			pgmProcessor.open();
			cout << "Image opened" << endl;
			break;
		case 4:
			cout << "Closing image..." << endl;
			pgmProcessor.close();
			cout << "Image closed" << endl;
			break;
		case 5:
			cout << "Threshold value: ";
			cin >> threshold;
			cout << "Thresholding image..." << endl;
			pgmProcessor.threshold(threshold);
			cout << "Image thresholded" << endl;
			break;
		case 6:
			cout << "Threshold value: ";
			cin >> threshold;
			cout << "Differentiating image..." << endl;
			pgmProcessor.differentiate(threshold);
			cout << "Image differentiated" << endl;
			break;
		case 7:
			cout << "Inverting image..." << endl;
			pgmProcessor.invert();
			cout << "Image inverted" << endl;
			break;
		case 8:
			cout << "Blurring image (no diagonals)..." << endl;
			pgmProcessor.blur(false);
			cout << "Image blurred" << endl;
			break;
		case 9:
			cout << "Blurring image (with diagonals)..." << endl;
			pgmProcessor.blur(true);
			cout << "Image blurred" << endl;
			break;
		case 10:
			char imagePathOutTemp[200];
			cout << "Image path: ";
			cin >> imagePathOutTemp;
			cout << "Saving image..." << endl;
			pgmProcessor.write(imagePathOutTemp);
			cout << "Image saved as " << imagePathOutTemp << endl;
			break;
		case 11:
			char histogramPathOutTemp[200];
			cout << "Histogram path: ";
			cin >> histogramPathOutTemp;
			cout << "Saving histogram..." << endl;
			pgmProcessor.saveHistogram(histogramPathOutTemp);
			cout << "Histogram saved as " << histogramPathOutTemp << endl;
			break;
		case 12:
			char profilePathOutTemp[200];
			int lineOrColumn;
			int index;
			cout << "Line or column (0 for line, 1 for column): ";
			cin >> lineOrColumn;
			cout << "Line or column index: ";
			cin >> index;
			cout << "Profile path: ";
			cin >> profilePathOutTemp;
			cout << "Saving profile..." << endl;
			pgmProcessor.saveLineColumnProfile(profilePathOutTemp, index, lineOrColumn);
			cout << "Profile saved as " << profilePathOutTemp << endl;
			break;
		case 0:
			cout << "Exiting..." << endl;
			break;
		default:
			cout << "Invalid option" << endl;
			break;
		}
		pgmProcessor.write(imagePathOut);
	} while (chosenOption != 0);

	cout << "Bye!" << endl;
	return 0;
}