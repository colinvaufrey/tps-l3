#include "image_ppm.h"
#include <cstdlib>

class PGMProcessor {
  private:
	OCTET *image;
	int imageHeight;
	int imageWidth;
	int imageSize;
	bool filterMatrix[3][3] = {{0, 1, 0}, {1, 1, 1}, {0, 1, 0}};
	int gaussianFilterMatrix[3][3] = {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}};

  public:
	PGMProcessor() {
		image = NULL;
		imageHeight = 0;
		imageWidth = 0;
		imageSize = 0;
	}

	PGMProcessor(char *imagePath) { read(imagePath); }

	PGMProcessor(char *imagePath, bool filterMatrix[3][3]) {
		read(imagePath);
		this->setFilterMatrix(filterMatrix);
	}

	PGMProcessor(OCTET *image, int imageHeight, int imageWidth) {
		this->image = image;
		this->imageHeight = imageHeight;
		this->imageWidth = imageWidth;
		this->imageSize = imageHeight * imageWidth;
	}

	~PGMProcessor() {
		if (image != NULL) {
			free(image);
		}
	}

	void read(char *imagePath) {
		// Reads the image
		lire_nb_lignes_colonnes_image_pgm(imagePath, &imageHeight, &imageWidth);
		imageSize = imageHeight * imageWidth;
		allocation_tableau(image, OCTET, imageSize);
		lire_image_pgm(imagePath, image, imageSize);
	}

	static OCTET *erodeImage(OCTET *image, int imageHeight, int imageWidth,
							 bool filterMatrix[3][3]) {
		// Erodes the image
		OCTET *erodedImage;
		allocation_tableau(erodedImage, OCTET, imageHeight * imageWidth);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// On parcourt le voisinage
				int minFound = 255;
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// On vérifie que le voisinage est dans l'image
						if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
							j + l < imageWidth) {
							// On vérifie que le pixel est dans la matrice de
							// convolution
							if (filterMatrix[k + 1][l + 1] == 1) {
								// On vérifie que le pixel est plus clair que le
								// minimum trouvé
								if (image[(i + k) * imageWidth + (j + l)] <
									minFound) {
									minFound =
										image[(i + k) * imageWidth + (j + l)];
								}
							}
						}
					}
				}
				erodedImage[i * imageWidth + j] = minFound;
			}
		}

		return erodedImage;
	}

	void erode() {
		// Erodes the image
		OCTET *erodedImage =
			erodeImage(image, imageHeight, imageWidth, filterMatrix);

		free(image);
		image = erodedImage;
	}

	static OCTET *dilateImage(OCTET *image, int imageHeight, int imageWidth,
							  bool filterMatrix[3][3]) {
		// Dilates the image
		OCTET *dilatedImage;
		allocation_tableau(dilatedImage, OCTET, imageHeight * imageWidth);

		// On crée la matrice de convolution renversée
		bool inverseFilterMatrix[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				inverseFilterMatrix[i][j] = filterMatrix[2 - i][2 - j];
			}
		}

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// On parcourt les voisins
				int maxFound = 0;
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// On vérifie que le voisinage est dans l'image
						if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
							j + l < imageWidth) {
							// On vérifie que le pixel est dans la matrice de
							// convolution renversée
							if (inverseFilterMatrix[k + 1][l + 1] == 1) {
								// On vérifie que le pixel est plus foncé que le
								// maximum trouvé
								if (image[(i + k) * imageWidth + (j + l)] >
									maxFound) {
									maxFound =
										image[(i + k) * imageWidth + (j + l)];
								}
							}
						}
					}
				}
				dilatedImage[i * imageWidth + j] = maxFound;
			}
		}

		return dilatedImage;
	}

	void dilate() {
		// Dilates the image
		OCTET *dilatedImage =
			dilateImage(image, imageHeight, imageWidth, filterMatrix);

		free(image);
		image = dilatedImage;
	}

	void open() {
		// Opens the image
		erode();
		dilate();
	}

	void close() {
		// Closes the image
		dilate();
		erode();
	}

	void setFilterMatrix(bool matrix[3][3]) {
		// Sets the filter matrix
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				filterMatrix[i][j] = matrix[i][j];
			}
		}
	}

	OCTET *getImage() {
		// Returns the image
		return image;
	}

	int getImageHeight() {
		// Returns the image height
		return imageHeight;
	}

	int getImageWidth() {
		// Returns the image width
		return imageWidth;
	}

	int getImageSize() {
		// Returns the image size
		return imageSize;
	}

	static OCTET *thresholdImage(OCTET *image, int imageSize,
								 int thresholdValue) {
		// Thresholds the image
		OCTET *thresholdedImage;
		allocation_tableau(thresholdedImage, OCTET, imageSize);

		for (int i = 0; i < imageSize; i++) {
			thresholdedImage[i] = image[i] < thresholdValue ? 0 : 255;
		}

		return thresholdedImage;
	}

	void threshold(int thresholdValue) {
		// Thresholds the image
		OCTET *thresholdedImage =
			thresholdImage(image, imageSize, thresholdValue);

		free(image);
		image = thresholdedImage;
	}

	static OCTET *differentiateImage(OCTET *image, int imageWidth,
									 int imageHeight, int thresholdValue,
									 bool filterMatrix[3][3], bool inverted) {
		// Draws the edges of the image

		int imageSize = imageHeight * imageWidth;

		OCTET *differentiatedImage;
		allocation_tableau(differentiatedImage, OCTET, imageSize);

		OCTET *imageBW = thresholdImage(image, imageSize, thresholdValue);
		OCTET *dilatedBWImage =
			dilateImage(imageBW, imageHeight, imageWidth, filterMatrix);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// Si les pixels sont différents, on écrit noir
				if (imageBW[i * imageWidth + j] !=
					dilatedBWImage[i * imageWidth + j]) {
					differentiatedImage[i * imageWidth + j] =
						(inverted) ? 255 : 0;
				}
				// Sinon, on écrit blanc
				else {
					differentiatedImage[i * imageWidth + j] =
						(inverted) ? 0 : 255;
				}
			}
		}

		free(imageBW);
		free(dilatedBWImage);

		return differentiatedImage;
	}

	void differentiate(int thresholdValue, bool inverted = false) {
		// Draws the edges of the image
		OCTET *differentiatedImage =
			differentiateImage(image, imageWidth, imageHeight, thresholdValue,
							   filterMatrix, inverted);

		free(image);
		image = differentiatedImage;
	}

	static OCTET *invertImage(OCTET *image, int imageSize) {
		// Inverts the image
		OCTET *invertedImage;
		allocation_tableau(invertedImage, OCTET, imageSize);

		for (int i = 0; i < imageSize; i++) {
			invertedImage[i] = 255 - image[i];
		}

		return invertedImage;
	}

	void invert() {
		// Inverts the image
		OCTET *invertedImage = invertImage(image, imageSize);

		free(image);
		image = invertedImage;
	}

	static OCTET *meanBlurImage(OCTET *image, int imageWidth, int imageHeight,
								bool includeDiagonals) {
		// Blurs the image
		int imageSize = imageHeight * imageWidth;

		OCTET *blurredImage;
		allocation_tableau(blurredImage, OCTET, imageSize);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// Si on est sur un bord, on recopie le pixel
				if (i == 0 || i == imageHeight - 1 || j == 0 ||
					j == imageWidth - 1) {
					blurredImage[i * imageWidth + j] =
						image[i * imageWidth + j];
				} else {
					int sum = 0;
					int count = 0;

					// On parcourt les pixels voisins
					for (int k = -1; k <= 1; k++) {
						for (int l = -1; l <= 1; l++) {
							// On ne prend pas en compte les diagonales
							if (includeDiagonals || k == 0 || l == 0) {
								sum += image[(i + k) * imageWidth + j + l];
								count++;
							}
						}
					}

					blurredImage[i * imageWidth + j] = sum / count;
				}
			}
		}

		return blurredImage;
	}

	void meanBlur(bool includeDiagonals = false) {
		// Blurs the image
		OCTET *blurredImage =
			meanBlurImage(image, imageWidth, imageHeight, includeDiagonals);

		free(image);
		image = blurredImage;
	}

	static OCTET *gradientImage(OCTET *image, int imageWidth, int imageHeight) {
		// Calculates the gradient of the image
		int imageSize = imageHeight * imageWidth;

		OCTET *gradientImage;
		allocation_tableau(gradientImage, OCTET, imageSize);

		// On parcourt l'image

		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				int horizontalGradient = 0;
				int verticalGradient = 0;

				int currentPixel = image[i * imageWidth + j];
				int previousPixelHorizontal =
					image[i * imageWidth + j - 1]; // Pixel à gauche
				int previousPixelVertical =
					image[(i - 1) * imageWidth + j]; // Pixel en haut

				// Si on est sur un bord, on recopie le pixel
				if (i == 0 || i == imageHeight - 1 || j == 0 ||
					j == imageWidth - 1) {
					gradientImage[i * imageWidth + j] =
						image[i * imageWidth + j];
				} else {
					// On calcule le gradient horizontal
					horizontalGradient = currentPixel - previousPixelHorizontal;

					// On calcule le gradient vertical
					verticalGradient = currentPixel - previousPixelVertical;

					// On calcule le gradient
					gradientImage[i * imageWidth + j] = sqrt(
						pow(horizontalGradient, 2) + pow(verticalGradient, 2));
				}
			}
		}

		return gradientImage;
	}

	void gradient() {
		// Calculates the gradient of the image
		OCTET *imageGradient = gradientImage(image, imageWidth, imageHeight);

		free(image);
		image = imageGradient;
	}

	static OCTET *hysteresisThresholdImage(OCTET *image, int imageWidth,
										   int imageHeight, int thresholdMin,
										   int thresholdMax) {
		// Applies the hysteresis threshold to the image
		int imageSize = imageHeight * imageWidth;

		OCTET *thresholdedImage;
		allocation_tableau(thresholdedImage, OCTET, imageSize);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// Si le pixel est supérieur au seuil max, on écrit blanc
				if (image[i * imageWidth + j] > thresholdMax) {
					thresholdedImage[i * imageWidth + j] = 255;
				} else if (image[i * imageWidth + j] < thresholdMin) {
					// Si le pixel est inférieur au seuil min, on écrit noir
					thresholdedImage[i * imageWidth + j] = 0;
				}
			}
		}

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// Si le pixel est supérieur au seuil min et inférieur au seuil
				// max, on regarde ses voisins
				if (image[i * imageWidth + j] > thresholdMin &&
					image[i * imageWidth + j] < thresholdMax) {
					// On parcourt les voisins
					for (int k = -1; k <= 1; k++) {
						for (int l = -1; l <= 1; l++) {
							// Si un voisin est supérieur au seuil max, on écrit
							// blanc
							if (image[(i + k) * imageWidth + j + l] >
								thresholdMax) {
								thresholdedImage[i * imageWidth + j] = 255;
							}
						}
					}
				}
			}
		}

		return thresholdedImage;
	}

	void hysteresisThreshold(int thresholdMin, int thresholdMax) {
		// Applies the hysteresis threshold to the image
		OCTET *thresholdedImage = hysteresisThresholdImage(
			image, imageWidth, imageHeight, thresholdMin, thresholdMax);

		free(image);
		image = thresholdedImage;
	}

	static OCTET *gaussianBlurImage(OCTET *image, int imageWidth,
									int imageHeight,
									int gaussianFilterMatrix[3][3]) {
		// Blurs the image with the defined gaussian filter
		int imageSize = imageHeight * imageWidth;

		OCTET *blurredImage;
		allocation_tableau(blurredImage, OCTET, imageSize);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				int sum = 0;
				int count = 0;

				// On parcourt le filtre
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// Si on est sur un bord, on ne fait rien
						if (i + k < 0 || i + k >= imageHeight || j + l < 0 ||
							j + l >= imageWidth) {
							continue;
						}

						// On applique le filtre
						sum += image[(i + k) * imageWidth + j + l] *
							   gaussianFilterMatrix[k + 1][l + 1];
						count += gaussianFilterMatrix[k + 1][l + 1];
					}
				}

				blurredImage[i * imageWidth + j] = sum / count;
			}
		}

		return blurredImage;
	}

	void gaussianBlur() {
		// Blurs the image with the defined gaussian filter
		OCTET *blurredImage = gaussianBlurImage(image, imageWidth, imageHeight,
												gaussianFilterMatrix);

		free(image);
		image = blurredImage;
	}

	static int *histogramImage(OCTET *image, int imageSize) {
		// Returns the histogram of the image
		static int histogram[256];

		for (int i = 0; i < 256; i++) {
			histogram[i] = 0;
		}

		for (int i = 0; i < imageSize; i++) {
			histogram[image[i]]++;
		}

		return histogram;
	}

	void saveHistogram(char *histogramPath) {
		// Saves the histogram of the image
		int *histogram = histogramImage(image, imageSize);

		FILE *histogramFile = fopen(histogramPath, "w");

		for (int i = 0; i < 256; i++) {
			fprintf(histogramFile, "%d %d\n", i, histogram[i]);
		}

		fclose(histogramFile);
	}

	static int *lineColumnProfile(OCTET *image, int imageWidth, int imageHeight,
								  int index, bool isLine) {
		// Returns the line or column profile of the image
		int profileSize = (isLine) ? imageWidth : imageHeight;
		int *profile = (int *)malloc(profileSize * sizeof(int));

		for (int i = 0; i < profileSize; i++) {
			profile[i] = 0;
		}

		if (isLine) {
			for (int i = 0; i < imageWidth; i++) {
				profile[i] = image[index * imageWidth + i];
			}
		} else {
			for (int i = 0; i < imageHeight; i++) {
				profile[i] = image[i * imageWidth + index];
			}
		}

		return profile;
	}

	void saveLineColumnProfile(char *profilePath, int index, bool isLine) {
		// Saves the line or column profile of the image
		int *profile =
			lineColumnProfile(image, imageWidth, imageHeight, index, isLine);

		FILE *profileFile = fopen(profilePath, "w");

		for (int i = 0; i < ((isLine) ? imageWidth : imageHeight); i++) {
			fprintf(profileFile, "%d %d\n", i, profile[i]);
		}

		fclose(profileFile);

		free(profile);
	}

	void write(char *imagePath) {
		// Writes the image
		ecrire_image_pgm(imagePath, image, imageHeight, imageWidth);
	}
};