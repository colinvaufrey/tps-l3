#include "image_ppm.h"

int main(int argc, char *argv[]) {
    OCTET *ImgInY, *ImgInCb, *ImgInCr, *ImgOut;
    int nH, nW, nTaille;

    if (argc != 5) {
        printf("Usage: ImageInY.pgm ImageInCb.pgm ImageInCr.pgm ImageOut.ppm\n");
        exit(1);
    }
    
    lire_nb_lignes_colonnes_image_pgm(argv[1], &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgInY, OCTET, nTaille);
    lire_image_pgm(argv[1], ImgInY, nTaille);
    allocation_tableau(ImgInCb, OCTET, nTaille);
    lire_image_pgm(argv[2], ImgInCb, nTaille);
    allocation_tableau(ImgInCr, OCTET, nTaille);
    lire_image_pgm(argv[3], ImgInCr, nTaille);

    allocation_tableau(ImgOut, OCTET, nTaille * 3);

    for (int i = 0; i < nTaille; i++) {
        int y = ImgInY[i];
        int cb = ImgInCb[i];
        int cr = ImgInCr[i];
        
        ImgOut[3 * i] = (y + 1.402 * (cr - 128));
        ImgOut[3 * i + 1] = (y - 0.34414 * (cb - 128) - 0.71414 * (cr - 128));
        ImgOut[3 * i + 2] = (y + 1.772 * (cb - 128));
    }

    ecrire_image_ppm(argv[4], ImgOut, nH, nW);
    free(ImgInY);
    free(ImgInCb);
    free(ImgInCr);
    free(ImgOut);
    return 1;
}