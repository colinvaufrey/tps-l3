#include "image_ppm.h"

int main(int argc, char *argv[]) {
    OCTET *ImgIn, *ImgOut;
    int nH, nW, nTaille;

    if (argc != 3) {
        printf("Usage: ImageIn.ppm ImageOut.pgm\n");
        exit(1);
    }

    lire_nb_lignes_colonnes_image_ppm(argv[1], &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille * 3);
    lire_image_ppm(argv[1], ImgIn, nTaille);

    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nTaille; i++) {
        ImgOut[i] = (0.3 * ImgIn[3 * i] + 0.59 * ImgIn[3 * i + 1] +
                     0.11 * ImgIn[3 * i + 2]);
    }

    ecrire_image_pgm(argv[2], ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
    return 1;
}