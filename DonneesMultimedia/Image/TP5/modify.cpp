#include "image_ppm.h"

int main(int argc, char *argv[]) {
    OCTET *ImgIn, *ImgOut;
    int nH, nW, nTaille;

    if (argc != 4) {
        printf("Usage: ImageInY.pgm ImageOutY.pgm k\n");
        exit(1);
    }

    lire_nb_lignes_colonnes_image_pgm(argv[1], &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(argv[1], ImgIn, nTaille);

    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nTaille; i++) {
        int k = atoi(argv[3]);
        int y = ImgIn[i] + k;

        if (y > 255) {
            y = 255;
        } else if (y < 0) {
            y = 0;
        }

        ImgOut[i] = y;
    }

    ecrire_image_pgm(argv[2], ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
    return 1;
}