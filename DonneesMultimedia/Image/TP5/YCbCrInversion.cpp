#include "image_ppm.h"

#define RED 0
#define GREEN 1
#define BLUE 2

void nextColourCombination(int * combination) {
    // goes through all possible combinations of 3 colours

    if (combination[0] == RED && combination[1] == GREEN && combination[2] == BLUE) {
        combination[0] = RED;
        combination[1] = BLUE;
        combination[2] = GREEN;
    } else if (combination[0] == RED && combination[1] == BLUE && combination[2] == GREEN) {
        combination[0] = GREEN;
        combination[1] = RED;
        combination[2] = BLUE;
    } else if (combination[0] == GREEN && combination[1] == RED && combination[2] == BLUE) {
        combination[0] = GREEN;
        combination[1] = BLUE;
        combination[2] = RED;
    } else if (combination[0] == GREEN && combination[1] == BLUE && combination[2] == RED) {
        combination[0] = BLUE;
        combination[1] = RED;
        combination[2] = GREEN;
    } else if (combination[0] == BLUE && combination[1] == RED && combination[2] == GREEN) {
        combination[0] = BLUE;
        combination[1] = GREEN;
        combination[2] = RED;
    } else if (combination[0] == BLUE && combination[1] == GREEN && combination[2] == RED) {
        combination[0] = RED;
        combination[1] = GREEN;
        combination[2] = BLUE;
    }
}

int main(int argc, char *argv[]) {
    OCTET *ImgInY, *ImgInCb, *ImgInCr;
    int nH, nW, nTaille;

    if (argc != 5) {
        printf("Usage: ImageInY.pgm ImageInCb.pgm ImageInCr.pgm ImageOut\n");
        exit(1);
    }
    
    lire_nb_lignes_colonnes_image_pgm(argv[1], &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgInY, OCTET, nTaille);
    lire_image_pgm(argv[1], ImgInY, nTaille);
    allocation_tableau(ImgInCb, OCTET, nTaille);
    lire_image_pgm(argv[2], ImgInCb, nTaille);
    allocation_tableau(ImgInCr, OCTET, nTaille);
    lire_image_pgm(argv[3], ImgInCr, nTaille);

    int colourValues[3][nTaille];

    for (int i = 0; i < nTaille; i++) {
        int y = ImgInY[i];
        int cb = ImgInCb[i];
        int cr = ImgInCr[i];
        
        colourValues[RED][i] = (y + 1.402 * (cr - 128));
		colourValues[GREEN][i] =
			(y - 0.34414 * (cb - 128) - 0.71414 * (cr - 128));
		colourValues[BLUE][i] = (y + 1.772 * (cb - 128));
	}

    int colourCombination[3] = {RED, GREEN, BLUE};
    char colourNames[3] = {'R', 'G', 'B'};

    do {
        OCTET *ImgOut;
        allocation_tableau(ImgOut, OCTET, nTaille * 3);

        for (int i = 0; i < nTaille; i++) {
            ImgOut[3 * i] = colourValues[colourCombination[0]][i];
            ImgOut[3 * i + 1] = colourValues[colourCombination[1]][i];
            ImgOut[3 * i + 2] = colourValues[colourCombination[2]][i];
        }

        char * outputName = (char *) malloc(100 * sizeof(char));
        strcpy(outputName, argv[4]);
        strcat(outputName, "_");
        for (int i = 0; i < 3; i++) {
            char colourName[2] = {colourNames[colourCombination[i]], '\0'};
            strcat(outputName, colourName);
        }
        strcat(outputName, ".ppm");

        printf("Writing %s\n", outputName);

        ecrire_image_ppm(outputName, ImgOut, nH, nW);
        free(ImgOut);
        nextColourCombination(colourCombination);
    } while (colourCombination[0] != RED || colourCombination[1] != GREEN || colourCombination[2] != BLUE);
    

    free(ImgInY);
    free(ImgInCb);
    free(ImgInCr);
    return 1;
}