#include "image_ppm.h"

int main(int argc, char *argv[]) {
	OCTET *ImgIn, *ImgOutY, *ImgOutCb, *ImgOutCr;
	int nH, nW, nTaille;

    if (argc != 3) {
        printf("Usage: ImageIn.ppm ImageOut\n");
        exit(1);
    }

    lire_nb_lignes_colonnes_image_ppm(argv[1], &nH, &nW);

    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille * 3);
    lire_image_ppm(argv[1], ImgIn, nTaille);

    allocation_tableau(ImgOutY, OCTET, nTaille);
    allocation_tableau(ImgOutCb, OCTET, nTaille);
    allocation_tableau(ImgOutCr, OCTET, nTaille);

    for (int i = 0; i < nTaille; i++) {
        int red = ImgIn[3 * i];
        int green = ImgIn[3 * i + 1];
        int blue = ImgIn[3 * i + 2];
        
        ImgOutY[i] = (0.299 * red + 0.587 * green + 0.114 * blue);
        ImgOutCb[i] = (-0.1687 * red - 0.3313 * green + 0.5 * blue + 128);
        ImgOutCr[i] = (0.5 * red - 0.4187 * green - 0.0813 * blue + 128);
    }

    char *filename = argv[2];
    char *filenameY = (char *)malloc(strlen(filename) + 6);
    char *filenameCb = (char *)malloc(strlen(filename) + 7);
    char *filenameCr = (char *)malloc(strlen(filename) + 7);
    strcpy(filenameY, filename);
    strcpy(filenameCb, filename);
    strcpy(filenameCr, filename);
    strcat(filenameY, "_Y.pgm");
    strcat(filenameCb, "_Cb.pgm");
    strcat(filenameCr, "_Cr.pgm");

    ecrire_image_pgm(filenameY, ImgOutY, nH, nW);
    ecrire_image_pgm(filenameCb, ImgOutCb, nH, nW);
    ecrire_image_pgm(filenameCr, ImgOutCr, nH, nW);

    free(ImgIn);
    free(ImgOutY);
    free(ImgOutCb);
    free(ImgOutCr);
    return 1;
}