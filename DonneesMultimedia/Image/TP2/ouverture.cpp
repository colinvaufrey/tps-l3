// ouverture.cpp ‑ Ouverture d'une image binaire en utilisant l'érosion et la dilatation
#include "pgm_processor.h"

int main(int argc, char *argv[]) {
	char readImageName[250], writtenImageName[250];
	int imageHeight, imageWidth, imageSize;

	if (argc != 3) {
		printf("Usage: %s read_image.pgm written_image.pgm\n", argv[0]);
		exit(1);
	}

	sscanf(argv[1], "%s", readImageName);
	sscanf(argv[2], "%s", writtenImageName);

	OCTET *readImage;

	lire_nb_lignes_colonnes_image_pgm(readImageName, &imageHeight, &imageWidth);
	imageSize = imageHeight * imageWidth;

	allocation_tableau(readImage, OCTET, imageSize);
	lire_image_pgm(readImageName, readImage, imageHeight * imageWidth);

	OCTET *erodedImage = erosion_pgm(readImage, imageHeight, imageWidth);
	OCTET *openedImage = dilatation_pgm(erodedImage, imageHeight, imageWidth);

	ecrire_image_pgm(writtenImageName, openedImage, imageHeight, imageWidth);
	free(readImage);
	free(erodedImage);
	free(openedImage);

	return 1;
}
