// difference.cpp ‑ Écriture des contours d'une image binaire à partir de l'image binaire et sa dilatation

#include "pgm_processor.h"

int main(int argc, char *argv[]) {
    char readBaseImageName[250], readDilatedImageName[250], writtenImageName[250];
	int imageHeight, imageWidth, imageSize;

	if (argc != 4) {
		printf("Usage: %s read_base_image.pgm read_dilated_image written_image.pgm\n", argv[0]);
		exit(1);
	}

	sscanf(argv[1], "%s", readBaseImageName);
	sscanf(argv[2], "%s", readDilatedImageName);
	sscanf(argv[3], "%s", writtenImageName);

	OCTET *readBaseImage, *readDilatedImage;

	lire_nb_lignes_colonnes_image_pgm(readBaseImageName, &imageHeight, &imageWidth);

	imageSize = imageHeight * imageWidth;

	allocation_tableau(readBaseImage, OCTET, imageSize);
	allocation_tableau(readDilatedImage, OCTET, imageSize);

	lire_image_pgm(readBaseImageName, readBaseImage, imageSize);
	lire_image_pgm(readDilatedImageName, readDilatedImage, imageSize);

	OCTET *writtenImage = difference_pgm(readBaseImage, readDilatedImage, imageHeight, imageWidth);

	ecrire_image_pgm(writtenImageName, writtenImage, imageHeight, imageWidth);
	free(readBaseImage);
	free(readDilatedImage);
	free(writtenImage);

	return 1;
}
