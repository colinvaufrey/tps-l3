#include "image_ppm.h"
#include <cstdlib>

class PGMProcessor {
  private:
	OCTET *image;
	int imageHeight;
	int imageWidth;
	int imageSize;
	bool filterMatrix[3][3] = {{0, 1, 0}, {1, 1, 1}, {0, 1, 0}};

  public:
	PGMProcessor() {
		image = NULL;
		imageHeight = 0;
		imageWidth = 0;
		imageSize = 0;
	}

	PGMProcessor(char *imagePath) { read(imagePath); }

	PGMProcessor(char *imagePath, bool filterMatrix[3][3]) {
		read(imagePath);
		this->setFilterMatrix(filterMatrix);
	}

	PGMProcessor(OCTET *image, int imageHeight, int imageWidth) {
		this->image = image;
		this->imageHeight = imageHeight;
		this->imageWidth = imageWidth;
		this->imageSize = imageHeight * imageWidth;
	}

	~PGMProcessor() {
		if (image != NULL) {
			free(image);
		}
	}

	void read(char *imagePath) {
		// Reads the image
		lire_nb_lignes_colonnes_image_pgm(imagePath, &imageHeight, &imageWidth);
		imageSize = imageHeight * imageWidth;
		allocation_tableau(image, OCTET, imageSize);
		lire_image_pgm(imagePath, image, imageSize);
	}

	static OCTET *erodeImage(OCTET *image, int imageHeight, int imageWidth,
							 bool filterMatrix[3][3]) {
		// Erodes the image
		OCTET *erodedImage;
		allocation_tableau(erodedImage, OCTET, imageHeight * imageWidth);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// On parcourt le voisinage
				int minFound = 255;
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// On vérifie que le voisinage est dans l'image
						if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
							j + l < imageWidth) {
							// On vérifie que le pixel est dans la matrice de
							// convolution
							if (filterMatrix[k + 1][l + 1] == 1) {
								// On vérifie que le pixel est plus clair que le
								// minimum trouvé
								if (image[(i + k) * imageWidth + (j + l)] <
									minFound) {
									minFound =
										image[(i + k) * imageWidth + (j + l)];
								}
							}
						}
					}
				}
				erodedImage[i * imageWidth + j] = minFound;
			}
		}

		return erodedImage;
	}

	void erode() {
		// Erodes the image
		OCTET *erodedImage = erodeImage(image, imageHeight, imageWidth, filterMatrix);

		free(image);
		image = erodedImage;
	}

	static OCTET *dilateImage(OCTET *image, int imageHeight, int imageWidth,
							  bool filterMatrix[3][3]) {
		// Dilates the image
		OCTET *dilatedImage;
		allocation_tableau(dilatedImage, OCTET, imageHeight * imageWidth);

		// On crée la matrice de convolution renversée
		bool inverseFilterMatrix[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				inverseFilterMatrix[i][j] = filterMatrix[2 - i][2 - j];
			}
		}

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// On parcourt les voisins
				int maxFound = 0;
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// On vérifie que le voisinage est dans l'image
						if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
							j + l < imageWidth) {
							// On vérifie que le pixel est dans la matrice de
							// convolution renversée
							if (inverseFilterMatrix[k + 1][l + 1] == 1) {
								// On vérifie que le pixel est plus foncé que le
								// maximum trouvé
								if (image[(i + k) * imageWidth + (j + l)] >
									maxFound) {
									maxFound =
										image[(i + k) * imageWidth + (j + l)];
								}
							}
						}
					}
				}
				dilatedImage[i * imageWidth + j] = maxFound;
			}
		}

		return dilatedImage;
	}

	void dilate() {
		// Dilates the image
		OCTET *dilatedImage =
			dilateImage(image, imageHeight, imageWidth, filterMatrix);

		free(image);
		image = dilatedImage;
	}

	void open() {
		// Opens the image
		erode();
		dilate();
	}

	void close() {
		// Closes the image
		dilate();
		erode();
	}

	void setFilterMatrix(bool matrix[3][3]) {
		// Sets the filter matrix
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				filterMatrix[i][j] = matrix[i][j];
			}
		}
	}

	OCTET *getImage() {
		// Returns the image
		return image;
	}

	int getImageHeight() {
		// Returns the image height
		return imageHeight;
	}

	int getImageWidth() {
		// Returns the image width
		return imageWidth;
	}

	int getImageSize() {
		// Returns the image size
		return imageSize;
	}

	static OCTET *thresholdImage(OCTET *image, int imageSize,
								 int thresholdValue) {
		// Thresholds the image
		OCTET *thresholdedImage;
		allocation_tableau(thresholdedImage, OCTET, imageSize);

		for (int i = 0; i < imageSize; i++) {
			thresholdedImage[i] = image[i] < thresholdValue ? 0 : 255;
		}

		return thresholdedImage;
	}

	void threshold(int thresholdValue) {
		// Thresholds the image
		OCTET *thresholdedImage =
			thresholdImage(image, imageSize, thresholdValue);

		free(image);
		image = thresholdedImage;
	}

	static OCTET *differentiateImage(OCTET *image, int imageWidth,
									 int imageHeight, int thresholdValue,
									 bool filterMatrix[3][3], bool inverted) {
		// Draws the edges of the image

		int imageSize = imageHeight * imageWidth;

		OCTET *differentiatedImage;
		allocation_tableau(differentiatedImage, OCTET, imageSize);

		OCTET *imageBW = thresholdImage(image, imageSize, thresholdValue);
		OCTET *dilatedBWImage =
			dilateImage(imageBW, imageHeight, imageWidth, filterMatrix);

		// On parcourt l'image
		for (int i = 0; i < imageHeight; i++) {
			for (int j = 0; j < imageWidth; j++) {
				// Si les pixels sont différents, on écrit noir
				if (imageBW[i * imageWidth + j] !=
					dilatedBWImage[i * imageWidth + j]) {
					differentiatedImage[i * imageWidth + j] =
						(inverted) ? 255 : 0;
				}
				// Sinon, on écrit blanc
				else {
					differentiatedImage[i * imageWidth + j] =
						(inverted) ? 0 : 255;
				}
			}
		}

		free(imageBW);
		free(dilatedBWImage);

		return differentiatedImage;
	}

	void differentiate(int thresholdValue, bool inverted = false) {
		// Draws the edges of the image
		OCTET *differentiatedImage =
			differentiateImage(image, imageWidth, imageHeight, thresholdValue,
							   filterMatrix, inverted);

		free(image);
		image = differentiatedImage;
	}

	void write(char *imagePath) {
		// Writes the image
		ecrire_image_pgm(imagePath, image, imageHeight, imageWidth);
	}
};

OCTET *erosion_pgm(OCTET *readImage, int imageHeight, int imageWidth) {

	// Allocation mémoire pour l'image écrite
	OCTET *writtenImage;
	allocation_tableau(writtenImage, OCTET, imageHeight * imageWidth);

	// On parcourt l'image
	for (int i = 0; i < imageHeight; i++) {
		for (int j = 0; j < imageWidth; j++) {
			// On parcourt le voisinage
			for (int k = -1; k <= 1; k++) {
				for (int l = -1; l <= 1; l++) {
					// On vérifie que le voisinage est dans l'image
					if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
						j + l < imageWidth) {
						// Si un pixel du voisinage est blanc, le pixel est
						// blanc
						if (readImage[(i + k) * imageWidth + (j + l)] == 255) {
							writtenImage[i * imageWidth + j] = 255;
							break;
						}
					}
				}
			}
		}
	}

	return writtenImage;
}

OCTET *dilatation_pgm(OCTET *readImage, int imageHeight, int imageWidth) {

	// Allocation mémoire pour l'image écrite
	OCTET *writtenImage;
	allocation_tableau(writtenImage, OCTET, imageHeight * imageWidth);

	int imageSize = imageHeight * imageWidth;

	// On initialise l'image écrite à blanc
	for (int i = 0; i < imageSize; i++) {
		writtenImage[i] = 255;
	}

	// On parcourt l'image
	for (int i = 0; i < imageHeight; i++) {
		for (int j = 0; j < imageWidth; j++) {
			// Si le pixel est noir, on passe tous les pixels du voisinage à
			// noir
			if (readImage[i * imageWidth + j] == 0) {
				// On parcourt le voisinage
				for (int k = -1; k <= 1; k++) {
					for (int l = -1; l <= 1; l++) {
						// On vérifie que le voisinage est dans l'image et qu'il
						// est à une distance de 1 maximum
						if (i + k >= 0 && i + k < imageHeight && j + l >= 0 &&
							j + l < imageWidth && abs(k) + abs(l) <= 1) {
							writtenImage[(i + k) * imageWidth + (j + l)] = 0;
						}
					}
				}
			}
		}
	}

	return writtenImage;
}

OCTET *difference_pgm(OCTET *readBaseImage, OCTET *readDilatedImage,
					  int imageHeight, int imageWidth, bool inverted = true) {

	// Allocation mémoire pour l'image écrite
	OCTET *writtenImage;
	allocation_tableau(writtenImage, OCTET, imageHeight * imageWidth);

	// On parcourt l'image
	for (int i = 0; i < imageHeight; i++) {
		for (int j = 0; j < imageWidth; j++) {
			// Si les pixels sont différents, on écrit noir
			if (readBaseImage[i * imageWidth + j] !=
				readDilatedImage[i * imageWidth + j]) {
				writtenImage[i * imageWidth + j] = (inverted) ? 255 : 0;
			}
			// Sinon, on écrit blanc
			else {
				writtenImage[i * imageWidth + j] = (inverted) ? 0 : 255;
			}
		}
	}

	return writtenImage;
}