#include "image_ppm.h"

int main(int argc, char *argv[]) {
	OCTET *ImgIn, *ImgIn2, *ImgOut;
	int nH, nW, nTaille;

    if (argc != 4) {
        printf("Usage: ImageIn.pgm ImageIn2.pgm ImageOut\n");
        exit(1);
    }

    lire_nb_lignes_colonnes_image_pgm(argv[1], &nH, &nW);

    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(argv[1], ImgIn, nTaille);

    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(argv[2], ImgIn2, nTaille);

    allocation_tableau(ImgOut, OCTET, nTaille);
    
    for (int i = 0; i < nTaille; i++) {
        ImgOut[i] = ImgIn[i] - ImgIn2[i] + 128;
    }

    ecrire_image_pgm(argv[3], ImgOut, nH, nW);

    free(ImgIn);
    free(ImgIn2);
    free(ImgOut);
    return 1;
}