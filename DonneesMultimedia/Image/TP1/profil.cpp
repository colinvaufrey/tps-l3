// test_couleur.cpp : Seuille une image en niveau de gris

#include "image_ppm.h"
#include <fstream>
#include <iostream>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomFichierSortie[250];
	int nH, nW, nTaille;
	char lineOrColumn;
	int lineColumnIndex;

	if (argc != 5) {
		printf("Usage: ImageIn.pgm OutputFile l|c index\n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomFichierSortie);
	sscanf(argv[3], "%c", &lineOrColumn);
	sscanf(argv[4], "%d", &lineColumnIndex);

	OCTET *ImgIn;

	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;

	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

	ofstream outFile;
	outFile.open(cNomFichierSortie);

	if (lineOrColumn == 'l') {
		for (int i = 0; i < nW; i++) {
			cout << i << " " << (int)ImgIn[lineColumnIndex * nW + i] << endl;
			outFile << i << " " << (int)ImgIn[lineColumnIndex * nW + i] << endl;
		}
	} else if (lineOrColumn == 'c') {
		for (int i = 0; i < nH; i++) {
			cout << i << " " << (int)ImgIn[i * nW + lineColumnIndex] << endl;
			outFile << i << " " << (int)ImgIn[i * nW + lineColumnIndex] << endl;
		}
	} else {
		cout << "Wrong argument for line / column" << endl;
	}

	outFile.close();

	free(ImgIn);
	return 1;
}
