// test_couleur.cpp : Seuille une image couleur

#include "image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomImgEcrite[250];
	int nH, nW, nTaille, nR, nG, nB, SR, SV, SB;

	if (argc != 6) {
		printf("Usage: ImageIn.ppm ImageOut.ppm SR SV SB\n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomImgEcrite);
	sscanf(argv[3], "%d", &SR);
	sscanf(argv[4], "%d", &SV);
	sscanf(argv[5], "%d", &SB);

	OCTET *ImgIn, *ImgOut;

	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;

	int nTaille3 = nTaille * 3;
	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille3);

	for (int i = 0; i < nTaille3; i += 3) {
		nR = ImgIn[i];
		nG = ImgIn[i + 1];
		nB = ImgIn[i + 2];

		ImgOut[i] = (nR < SR) ? 0 : 255;
		ImgOut[i + 1] = (nG < SV) ? 0 : 255);
		ImgOut[i + 2] = (nB < SB) ? 0 : 255;
	}

	ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
	free(ImgIn);
	return 1;
}
