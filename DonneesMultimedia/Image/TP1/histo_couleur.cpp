// test_couleur.cpp : Seuille une image en niveau de gris

#include "image_ppm.h"
#include <fstream>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomFichierSortie[250];
	int nH, nW, nTaille;
	int histoR[256];
	int histoV[256];
	int histoB[256];

	for (int i = 0; i < 256; i++) {
		histoR[i] = 0;
		histoV[i] = 0;
		histoB[i] = 0;
	}

	if (argc != 3) {
		printf("Usage: ImageIn.ppm FileOut.txt \n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomFichierSortie);

	OCTET *ImgIn;

	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;
	int nTaille3 = nTaille * 3;

	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

	for (int i = 0; i < nTaille3; i += 3) {
		int nR = ImgIn[i];
		int nG = ImgIn[i + 1];
		int nB = ImgIn[i + 2];
		histoR[nR]++;
		histoV[nG]++;
		histoB[nB]++;
	}

	free(ImgIn);

	ofstream outFile;
	outFile.open(cNomFichierSortie);
	for (int i = 0; i < 256; i++) {
		outFile << i << " " << histoR[i] << " " << histoV[i] << " " << histoB[i]
				<< endl;
	}
	outFile.close();
	return 1;
}
